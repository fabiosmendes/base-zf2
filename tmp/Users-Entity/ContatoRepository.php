<?php

namespace Front\Entity;

use Doctrine\ORM\EntityRepository;

class ContatoRepository extends EntityRepository
{
    public function findByEmailAndTelefone($email, $telefone)
    {
        $contato = $this->findOneBy(array('email' => $email, 'telefone' => $telefone));
        return $contato;
    }
    
    public function getListaContatos()
    {
        $lista = $this->findAll();
        return $lista;
    }
    
    
}
