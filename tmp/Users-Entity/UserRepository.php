<?php

namespace Users\Entity;

use Doctrine\ORM\EntityRepository;

class UserRepository extends EntityRepository
{
    public function findByEmailAndPassword($email, $password)
    {
        $user = $this->findOneBy(array('email' => $email));
        
        if($user instanceof \Users\Entity\User) {
            $hashPassword = $user->encryptPassword($password);
            if($hashPassword == $user->getPassword()) {
                return $user;
            }    
        }

        return false;
    }    
}
