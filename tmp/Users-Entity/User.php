<?php

namespace Users\Entity;

use Doctrine\ORM\Mapping as ORM;

use Zend\Math\Rand,
    Zend\Crypt\Key\Derivation\Pbkdf2;

use Zend\Stdlib\Hydrator;

/**
 * Users
 *
 * @ORM\Table(name="users")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 * @ORM\Entity(repositoryClass="Users\Entity\UserRepository")
 * 
 */
class User
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=60, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=60, nullable=true)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=255, nullable=true)
     */
    private $password;

    /**
     * @var string
     *
     * @ORM\Column(name="salt", type="string", length=255, nullable=true)
     */
    private $salt;

    /**
     * @var integer
     *
     * @ORM\Column(name="points", type="integer", nullable=true)
     */
    private $points;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;

    public function __construct(array $options = array())
    {
        $this->salt = base64_encode(Rand::getBytes(8, true));
        
        (new Hydrator\ClassMethods)->hydrate($options, $this);
        
        $this->createdAt = new \Datetime("now");
        $this->updatedAt = new \Datetime("now");
    }
    
    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        if (!is_int($id)) {
            throw new \InvalidArgumentException('ID accept only integers.');
        }
        
        if ($id <= 0) {
            throw new \InvalidArgumentException('ID accept only number greater than zero.');
        }    
        
        $this->id = $id;
        return $this;
    }

    public function getName()
    {
        return $this->name;
    }
    
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }
    
    public function getEmail()
    {
        return $this->email;
    }
    
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }
    
    public function getPassword()
    {
        return $this->password;
    }
    
    public function setPassword($password)
    {
        $hashPassword = $this->encryptPassword($password);
        $this->password = $hashPassword;
        return $this;
    }
    
    public function getSalt()
    {
        return $this->salt;
    }

    public function setSalt($salt)
    {
        $this->salt = $salt;
        return $this;
    }

    public function getPoints()
    {
        return $this->points;
    }

    public function setPoints($points)
    {
        if (!is_int($points)) {
            throw new \InvalidArgumentException('Attribute Points accept only integers.');
        }
        
        if ($points <= 0) { 
            throw new \InvalidArgumentException('Attribute Points accept only number greater than zero.');
        }
        
        $this->points = $points;
        return $this;
    }

    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function setCreatedAt()
    {
        $this->createdAt = new \DateTime("now");
        return $this;
    }

    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
    
    /**
     * @ORM\PreUpdate
     */
    public function setUpdatedAt()
    {        
        $this->updatedAt = new \DateTime("now");
        return $this;
    }
    
    public function encryptPassword($password) 
    {
        return base64_encode(Pbkdf2::calc('sha256', $password, $this->salt, 10000, strlen($password*2)));
    }
    
    public function toArray()
    {
        $array = (new Hydrator\ClassMethods)->extract($this);
        unset($array['password']);
        unset($array['salt']);
        
        return $array;
    }
}
