<?php

namespace Front\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;  
use Zend\Stdlib\Hydrator;

/**
 * @ORM\Entity
 * @ORM\Table(name="produtos")
 */
class Produto
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     * @var int
     */
    protected $id;

    /**
     * @ORM\Column(type="text")
     * @var string
     */
    protected $nome;

    /**
     * @ORM\Column(type="text")
     * @var string
     */
    protected $codigo;
    
    /**
     * @ORM\OneToMany(targetEntity="Categoria", mappedBy="produto",cascade="persist")
     **/
    protected $categorias;
    
    public function __construct(array $options = array()) 
    {       
        $this->categorias = new ArrayCollection();
        (new Hydrator\ClassMethods)->hydrate($options, $this);
    }
        
    public function getId()
    {
        return $this->id;
    }

    public function getNome()
    {
        return $this->nome;
    }

    public function getCodigo()
    {
        return $this->codigo;
    }

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function setNome($nome)
    {
        $this->nome = $nome;
        return $this;
    }

    public function setCodigo($codigo)
    {
        $this->codigo = $codigo;
        return $this;
    }
    
    public function setCategorias($categorias)
    {
        if(count($categorias) > 0) {
            foreach($categorias as $categoria) {
                $ligacao = new \Front\Entity\ProdutoCategoria($categoria);
                $this->categorias[] = $ligacao;
            }
        }
        
        return $this;
    }
    
    public function getCategorias()
    {
        return $this->categorias;
    }
    
    public function toArray() 
    {
        return (new Hydrator\ClassMethods)->extract($this);
    }  
}
