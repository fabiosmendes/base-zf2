<?php

namespace Front\Entity;

use Doctrine\ORM\Mapping as ORM;
use Zend\Stdlib\Hydrator;

/**
 * @ORM\Entity
 * @ORM\Table(name="produto_categoria")
 */
class ProdutoCategoria
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     * @var int
     */
    protected $id;

    /**
     * @ORM\Column(type="integer")
     * @ORM\Column(name="categoria_id", type="integer", nullable=true)
     */
    protected $categoriaId;
    
    /**
     * @ORM\ManyToOne(targetEntity="Produto", inversedBy="categorias")
     * @ORM\JoinColumn(name="produto_id", referencedColumnName="id")
     **/
    protected $produto;
    
    public function __construct(array $options = array())
    {
        (new Hydrator\ClassMethods)->hydrate($options, $this);
    }

    public function getId()
    {
        return $this->id;
    }

    public function getCategoriaId()
    {
        return $this->categoriaId;
    }
    
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function setCategoriaId($categoriaId)
    {
        $this->categoriaId = $categoriaId;
        return $this;
    }
    
    public function toArray()
    {
        return (new Hydrator\ClassMethods)->extract($this);
    }

}
