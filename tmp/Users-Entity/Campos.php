<?php

use Doctrine\ORM\Mapping as ORM;

/**
 * Campos
 *
 * @ORM\Table(name="campos")
 * @ORM\Entity
 */
class Campos
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;
    
    
    /**
     * @var float
     *
     * @ORM\Column(name="valor", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $valor;

    /**
     * @var float
     *
     * @ORM\Column(name="valor_float", type="float", precision=10, scale=0, nullable=true)
     */
    private $valorFloat;

    /**
     * @var string
     *
     * @ORM\Column(name="string", type="string", length=45, nullable=true)
     */
    private $string;
    
    
    /**
     * @var string
     *
     * @ORM\Column(name="texto", type="text", nullable=true)
     */
    private $texto;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data", type="date", nullable=true)
     */
    private $data;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="tempo", type="time", nullable=true)
     */
    private $tempo;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_tempo", type="datetime", nullable=true)
     */
    private $dataTempo;

    /**
     * @var boolean
     *
     * @ORM\Column(name="enum", type="boolean", nullable=true)
     */
    private $enum;
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="booleanx", type="boolean", nullable=true)
     */
    private $booleanx;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="time_stamp", type="datetime", nullable=true)
     */
    private $timeStamp;

}
