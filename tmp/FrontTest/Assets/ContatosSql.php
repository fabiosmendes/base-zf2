<?php

namespace FrontTest\Assets;

class ContatosSql
{
    public static function criarTabelaContato($em)
    {
        //static::dropTable($em);
        
        $sqlCreateTable = ' CREATE TABLE contatos (
                            id int,
                            nome varchar(255),
                            telefone varchar(255),
                            email varchar(255),
                            mensagem varchar(255),
                            data_envio NUMERIC
                            ) ';
        
       $em->getConnection()->exec($sqlCreateTable);
    }
    
    
    public static function dropTable($em)
    {
        $sqlDropTable = ' DROP TABLE contatos ';
        $em->getConnection()->exec($sqlDropTable);
    }
}
