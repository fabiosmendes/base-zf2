<?php

namespace FrontTest\Controller;

use FrontTest\Bootstrap;
use PHPUnit_Framework_TestCase;
use Base\Controller\BaseController;

class BaseControllerTest extends PHPUnit_Framework_TestCase
{
    protected $base;
    public function setUp()
    {
        parent::setUp();
        $this->base = new BaseController();
        $this->base->setServiceLocator(Bootstrap::getServiceManager());
    }
    
    public function testRetornoRenderView() 
    {   
        $render = $this->base->renderView(array());
        $this->assertInstanceOf('Zend\View\Model\ViewModel', $render);
    }
    
    public function testRetornoEntityManager() 
    {
        $em   = $this->base->getEm();   
        $this->assertInstanceOf('Doctrine\ORM\EntityManager', $em);
    }    
}