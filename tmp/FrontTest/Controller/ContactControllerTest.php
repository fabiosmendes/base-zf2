<?php

namespace FrontTest\Controller;

use Zend\Http\Request as HttpRequest;

use FrontTest\Bootstrap;
use Base\Controller\TestCaseController;
use Front\Controller\ContactController;
use Front\Form\Contact as FrmContact;

/**
 * @group Contact
 */
class ContatoControllerTest extends TestCaseController
{
    public function setUp()
    {
        $this->setApplicationConfig(Bootstrap::getConfig());
        parent::setUp();
    }
    
    public function getDadosCompletos()
    {
        return array(
            'nome'    => 'José Henrique Silva',
            'email'   => 'jose@gmail.com',
            'telefone' => '41231321',
            'mensagem' => 'Uma mensagem de teste para testar o serviço',
       );    
    }
    
    public function getMockService($serviceManager)
    {
        $serviceContatoMock = $this->getMockBuilder('Front\Service\Contato')
                                   ->disableOriginalConstructor()
                                   ->getMock();
        
        $serviceContatoMock->expects($this->once())
                           ->method('enviarEmail')
                           ->will($this->returnValue(true));
                
        $serviceManager->setAllowOverride(true);
        $serviceManager->setService('Front\Service\Contato', $serviceContatoMock);
    }
    
    public function testRotaContato()
    {
        $this->dispatch('/contato');
        $this->assertResponseStatusCode(200);
        $this->assertMatchedRouteName('front-contato');
    }
    
    public function testIndexActionPorSerAcessado() 
    {   
        $this->dispatch('/contato');
        $this->assertResponseStatusCode(200);
        
        $this->assertModuleName('Front');
        $this->assertControllerName('contato');
        $this->assertControllerClass('ContatoController');
        $this->assertActionName('index');
        $this->assertMatchedRouteName('front-contato');   
    }
    
    public function testVariaveisSetadasNoIndexAction()
    {
        $controller = new ContatoController();
        $indexResult = $controller->indexAction();
        $this->assertInstanceOf('Zend\View\Model\ViewModel', $indexResult);
        
        $variables = $indexResult->getVariables();
        $this->assertObjectHasAttribute('form', $variables);   
    }
    
    public function testRenderIndexActionContentForm() 
    {
        $this->dispatch('/contato');
        $this->assertQuery('form[name="contato"]', "Tem que haver um formulario na pagina"); 
        // $this->assertQueryContentContains('form ul li', "Value is required and can't be empty");
    }
    
    public function testEnvioFormContatoSemNome()
    {
        $post = array(
            'nome'    => '',
            'email'   => '',
        );
        
        $this->dispatch('/contato', HttpRequest::METHOD_POST, $post);
        
        $response   = $this->getResponse();
        $htmlResult = $response->getContent();
        
        $this->assertContains('box-erro', $htmlResult);
    }
    
    public function testEnvioFormContato()
    {
        $post = $this->getDadosCompletos();
        $this->dispatch('/contato', HttpRequest::METHOD_POST, $post);
       // $this->assertRedirect();
    }
    
    public function testEnvioValido()
    {
        $postData = array(
            'nome'    => 'José Henrique Silva',
            'email'   => 'jose@gmail.com',
            'telefone' => '(41) 3677-5050',
            'mensagem' => 'Uma mensagem de teste para testar o serviço',
        );
        
        $serviceManager = Bootstrap::getServiceManager();
        
        $serviceContatoMock = $this->getMockBuilder('Front\Service\Contato')
                                   ->disableOriginalConstructor()
                                   ->getMock();
        
        $serviceContatoMock->expects($this->once())
                           ->method('enviarEmail')
                           ->will($this->returnValue(true));
                
        $serviceManager->setAllowOverride(true);
        $serviceManager->setService('Front\Service\Contato', $serviceContatoMock);
        
        $controller = new ContatoController();
        $controller->setServiceLocator($serviceManager);
        $resultEnvio =$controller->enviarEmail($postData);                
        
        $this->assertNull($resultEnvio);
        /* 
        $post = array(
            'nome'    => 'José Henrique Silva',
            'email'   => 'jose@gmail.com',
            'telefone' => '(41) 3677-5050',
            'mensagem' => 'Uma mensagem de teste para testar o serviço',
        );
        
        $this->dispatch('/contato', HttpRequest::METHOD_POST, $post);
        
        $response   = $this->getResponse();
        var_dump(get_class_methods(get_class($response))); exit();
        
        $htmlResult = $response->getContent();
        
        $controller = new ContatoController();
        $indexResult = $controller->indexAction();
        $this->assertInstanceOf('Zend\View\Model\ViewModel', $indexResult);
        
        $variables = $indexResult->getVariables();
        $this->assertObjectHasAttribute('form', $variables);
        */
    }
}