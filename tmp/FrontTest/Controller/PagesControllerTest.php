<?php

namespace FrontTest\Controller;

use Zend\Http\Request as HttpRequest;

use FrontTest\Bootstrap;
use Base\Controller\TestCaseController;
use Front\Controller\PagesController;

class PagesControllerTest extends TestCaseController
{
    public function setUp()
    {
        $this->setApplicationConfig(Bootstrap::getConfig());
        parent::setUp();
    }
    
    public function testIndexActionQuemSomosPorSerAcessada() 
    {   
        $this->dispatch('/quem-somos');
        $this->assertResponseStatusCode(200);
        
        $this->assertModuleName('Front');
        $this->assertControllerName('pages');
        $this->assertControllerClass('PagesController');
        $this->assertActionName('quem-somos');
        $this->assertMatchedRouteName('front-quem-somos');   
    }
}