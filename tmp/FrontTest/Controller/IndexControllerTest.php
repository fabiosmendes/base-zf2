<?php

namespace FrontTest\Controller;

use Zend\Http\Request as HttpRequest;

use FrontTest\Bootstrap;
use Base\Controller\TestCaseController;
use Front\Controller\IndexController;

class IndexControllerTest extends TestCaseController
{
    public function setUp()
    {
        $this->setApplicationConfig(Bootstrap::getConfig());
        parent::setUp();
    }
    
    public function testIndexActionPorSerAcessado() 
    {   
        $this->dispatch('/');
        $this->assertResponseStatusCode(200);
        
        $this->assertModuleName('Front');
        $this->assertControllerName('index');
        $this->assertControllerClass('IndexController');
        $this->assertActionName('index');
        $this->assertMatchedRouteName('home');   
    }
    
    /*
    public function testRedirecionamentoParaOPasso2AposEnvioFormCotacao() 
    {
        $post = array(
            'nome'    => 'Fabio de Souza',
            'email'   => 'fbiosm@gmail.com',
            'horas'   => '1',
            'minutos' => '45',
        );
        
        $this->dispatch('/', HttpRequest::METHOD_POST, $post);
        $this->assertRedirectTo('./cotacao/passo-2');
    }
    */
}