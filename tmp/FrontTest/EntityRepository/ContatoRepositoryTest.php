<?php

namespace FrontTest\EntityRepository;

use Base\Fixture\TestDataFixture;
use Front\Fixture\LoadContato;

/**
 * Tests for ContatoRepository
 */
class ContatoRepositoryTest extends TestDataFixture
{
    /**
     * @var ContatoRepository
     */
    protected $repository;
    
    /**
     * @var fixture
     */
    protected $fixture;
    
    public function setUp()
    {
        $this->sm = \FrontTest\Bootstrap::getServiceManager();
        parent::setUp();
        
        $this->repository = $this->em->getRepository('Front\Entity\Contato');
        $this->fixture = new LoadContato(); 
        $this->loadFixtures(array($this->fixture));
    }
    
    public function testOrdemFixture()
    {
        $isOrdem = false;
        if(method_exists($this->fixture, 'getOrder')) {
            $isOrdem = ($this->fixture->getOrder() > 0);
        }
        
        $this->assertTrue($isOrdem);
    }
    
    
    public function testTipoRepositorioContato()
    {
        $this->assertInstanceOf('Front\Entity\ContatoRepository', $this->repository);
    }
    
    public function testListaContatos()
    {
        $lista = $this->repository->findAll();
        $this->assertTrue(count($lista) > 0);
    }
    
    
    public function testRemoverContato()
    {
        $contato = $this->repository->findOneByEmail('fbiosm@gmail.com');
        $this->em->remove($contato);
        $this->em->flush();
        
        $contatoEmail = $this->repository->findOneByEmail('fbiosm@gmail.com');
        $this->assertNull($contatoEmail);
    }
    
    public function testExcluirTodos()
    {
        $lista = $this->repository->findAll();
        foreach($lista as $entidade) {
            $this->em->remove($entidade);
        }
        
        $this->em->flush();
        
        $listaAtual = $this->repository->findAll();
        $this->assertTrue(count($listaAtual) == 0);
        
        $this->loadFixtures(array(new LoadContato));
    }
}
