jQuery(document).ready(function() {
// dynamic table
    jQuery('#dyntable').dataTable({
        "sPaginationType": "full_numbers",
        "aaSortingFixed": [[0, 'asc']],
        "fnDrawCallback": function(oSettings) {
            jQuery.uniform.update();
        }
    });
    jQuery('.tabbedwidget').tabs();

    jQuery('.taglist .close').click(function() {
        jQuery(this).parent().remove();
        return false;
    });

    /*pega caixa e valor a ser removido*/
    jQuery('.close').click(function() {
        elementId = jQuery(this).attr('id');
        typeId = jQuery(this).parent().attr('class');
    });

    /*Remove socials network links*/

    /*Remove langues options*/
    
    /*Add languages*/

    /*Add Post Elements*/
    //getPostCategories();

    function labelCheckboxGenarete(categories)
    {
        var items = "";
        jQuery.each(categories, function(i, val) {
            items += "<label>\n\
                            <div id='uniform-undefined' class='checker'>\n\
                                <span>\n\
                                    <input class='uniform-undefined' type='checkbox' value='" + categories[i].id + "' name='category_id[]' style='opacity: 0;'>\n\
                                </span>\n\
                            </div>\n\
                            " + categories[i].title + "\n\
                     </label>";
        });
        jQuery('#postCategoriesWrapper').html(items);
    }

    function getPostCategories() {
        jQuery.ajax({
            type: "POST",
            url: "/painel/pegar-categoria-de-post-ajax"
        }).done(function(categories) {
            labelCheckboxGenarete(categories);
        });
    }

    jQuery("input[name$='new_category']").keypress(function(e) {
        if (e.which === 13) {
            jQuery('#new_post_category').trigger("click");
        }
    });

    jQuery('#new_post_category').click(function() {
        title = jQuery("input[name$='new_category']").val();
        post_id = "NULL";
        jQuery.ajax({
            type: "POST",
            url: "/painel/nova-categoria-de-post-ajax",
            data: {title: title, post_category_id: post_id}
        }).done(function(msg) {
            jQuery("input:text[name=new_category]").val('');
            element_result = getPostCategories();
        });
    });


    /*Add Banner Elements*/
    jQuery('#BannerTitle').keyup(function() {
        jQuery('#BannerSlug').val(convertToSlug(jQuery(this).val()));
    });
    /*Add PostCategory Elements*/
    jQuery('#PostCategoryTitle').keyup(function() {
        jQuery('#PostCategorySlug').val(convertToSlug(jQuery(this).val()));
    });
    /*Add PostTag Elements*/
    jQuery('#PostTagTitle').keyup(function() {
        jQuery('#PostTagSlug').val(convertToSlug(jQuery(this).val()));
    });
    /**
     * get tag elements with keywords
     */
    jQuery('#tags_tag').keypress(function(e) {
        if (e.which === 13 || e.which === 44) {
            getNewTagElement();
        }
    }).change(function() {
        getNewTagElement();
    });
    /*Functions*/

    /**
     * 
     * @param {type} Text
     * @returns {unresolved}
     */
    function convertToSlug(Text)
    {
        return Text
                .toLowerCase()
                .replace(/ /g, '-')
                .replace(/[^\w-]+/g, '')
                ;
    }

    /**
     * 
     * @returns {string} String
     */
    function getNewTagElement() {
        tag_element = jQuery('.tag').last().children().html();
        tag_element = tag_element.replace("&nbsp;&nbsp;", "");
        count = jQuery('.tag').children().length;
        count = count / 2;
        if (jQuery("#input_tag_content").length === 0) {
            jQuery("#tags_tagsinput").append("<div id='input_tag_content'></div>");
        }
        jQuery("#input_tag_content").append("<input type='hidden' name='tag[" + count + "]' value='" + tag_element + "'>");
    }

});