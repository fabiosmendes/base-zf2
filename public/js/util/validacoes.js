// JavaScript Document

/**  
 * @method isEmpty(valor) 
 * @param  string valor
 * @return bool
 * @desc 
 */
function isEmpty(valor)
{
	if(valor.length  == 0 || valor == null || valor == '')
		return true;	
	
	return false;
}


/**  
 * @method isInt(obj)
 * @param  int x
 * @return bool
 * @desc 
 */
function isInt(obj) 
{
	var x = obj.value;
	var y = parseInt(x);
   	
	if(isNaN(y)) 
		return false;
   	
	return x == y && x.toString() == y.toString();
} 



function isEqual(valor1, valor2)
{
	return (valor1 == valor2);
}
 

/**  
 * @method verificaDatas(data) 
 * @param  obj data
 * @return bool
 * @desc: regra
 * 		  1- "As datas so iguais"
 * 		  2- "Data 1 maior que data 2"
 * 		  3- "Data 2 maior que data 1"
 */
function verificaDatas(dt1, dt2, regra)
{
	// dt1 e dt2: Devem ser tipo String, para evitar confusao de tipos
    if(typeof dt1 != "string" || typeof dt2 != "string"){
    	mensagem += "As datas devem ser passadas como strings";
        return false;
    }
 	
    // Instanciamos as datas, para poder usar getTime();
    data1 = new Date(dt1);
    data2 = new Date(dt2);
    
	if(!data1 || !data2){
    	mensagem += "Erro ao criar objetos";
        return false;
    }
 	
    // milliSegundos1: ir conter a quantidade de segundos corridos desde 1/1/1970 0h ate dt1
    milliSegundos1 = data1.getTime();
    // milliSegundos2: ir conter a quantidade de segundos corridos desde 1/1/1970 0h ate dt2
    milliSegundos2 = data2.getTime();
 	
    // Comparando millisegundos para retornar a concluso de quem  maior que quem...
    if(regra == 1 && milliSegundos1 == milliSegundos2){
    	mensagem += "As datas so iguais";
        return true;
    } 
	
	if(regra == 2 && milliSegundos1 > milliSegundos2){
     	mensagem += "Data 1 maior que data 2";
    	return true;
    } 
	
	if(regra == 3 && milliSegundos1 < milliSegundos2){
      	mensagem += "Data 2 maior que data 1";
    	return true;
    } 
	 
	return false;
}



function verificaData(digData) 
{
        var bissexto = 0;
        var data 	 = digData; 
        var tam 	 = data.length;
		
        if (tam == 10) 
        {
			var dia = data.substr(0,2);
			var mes = data.substr(3,2);
			var ano = data.substr(6,4);
			
			if ((ano > 1900)||(ano < 2100))
			{
				switch (mes) 
				{
					case '01':
					case '03':
					case '05':
					case '07':
					case '08':
					case '10':
					case '12':
						if  (dia <= 31) 
							return true;
					break
					
					case '04':              
					case '06':
					case '09':
					case '11':
						if  (dia <= 30) 
							return true;
					break
					case '02':
						/* Validando ano Bissexto / fevereiro / dia */ 
						if ((ano % 4 == 0) || (ano % 100 == 0) || (ano % 400 == 0)) 
								bissexto = 1; 
						if ((bissexto == 1) && (dia <= 29)) 
								return true;                             
						
						if ((bissexto != 1) && (dia <= 28)) 
								return true; 
					break                                           
				}
			}
	}       

	return false;
}



/**  
 * @method verificaData(data) 
 * @param  obj data
 * @return bool
 * @desc:   
 */
function verificaData1(data) 
{ 
	dia = data.substring(0,2); 
    mes = data.substring(3,5); 
    ano = data.substring(6,10) ; 

    situacao = true;
	
    // verifica o dia valido para cada mes 
     if((dia < '01')||(dia < '01' || dia > '30') && (  mes == '04' || mes == '06' || mes == '09' || mes == '11' ) || dia > '31')  
     	situacao = false; 

     // verifica se o mes e valido 
     if(mes < '01' || mes > '12' ) 
         situacao = false; 
    
     // verifica se e ano bissexto 
     if(mes == '2' && ( dia < '01' || dia > '29' || ( dia > '28' && (parseInt(ano / '4') != ano / '4')))) 
     	situacao = false; 
    
     if(data == "") 
     	situacao = false; 
        
     return situacao;
} 


/**  
 * @method validaCNPJ(CNPJ) 
 * @param  CNPJ
 * @return bool
 * @desc:   
 */
function validaCNPJ(CNPJ) 
{
	if (CNPJ.length < 18) {
		return false;
	}
	if ((CNPJ.charAt(2) != ".") || (CNPJ.charAt(6) != ".") || (CNPJ.charAt(10) != "/") || (CNPJ.charAt(15) != "-")){
		return false;
	}
	//substituir os caracteres que nao sao numeros
	if(document.layers && parseInt(navigator.appVersion) == 4){
	x = CNPJ.substring(0,2);
	x += CNPJ.substring(3,6);
	x += CNPJ.substring(7,10);
	x += CNPJ.substring(11,15);
	x += CNPJ.substring(16,18);
	CNPJ = x;
	} else {
	CNPJ = CNPJ.replace(".","");
	CNPJ = CNPJ.replace(".","");
	CNPJ = CNPJ.replace("-","");
	CNPJ = CNPJ.replace("/","");
	}
	var nonNumbers = /\D/;
	if (nonNumbers.test(CNPJ)){
		return false;
	}
	var a = [];
	var b = new Number;
	var c = [6,5,4,3,2,9,8,7,6,5,4,3,2];
	for (i=0; i<12; i++){
	a[i] = CNPJ.charAt(i);
	b += a[i] * c[i+1];
	}
	if ((x = b % 11) < 2) { a[12] = 0 } else { a[12] = 11-x }
	b = 0;
	for (y=0; y<13; y++) {
	b += (a[y] * c[y]);
	}
	if ((x = b % 11) < 2) { a[13] = 0; } else { a[13] = 11-x; }
	if ((CNPJ.charAt(12) != a[12]) || (CNPJ.charAt(13) != a[13])){
		return false;
	
	}
	
	
	return true;
}





/**  
 * @method validaCNPJ(objNode) 
 * @param  objNode
 * @return bool
 * @desc:   
 */
function validaCPF(cpf)	
{
	cpf = cpf.replace(".","");
	cpf = cpf.replace(".","");
	cpf = cpf.replace("-","");
	cpf = cpf.replace("/","");
	
	var i;
	var c = cpf.substr(0,9);
	var dv = cpf.substr(9,2);
	var d1 = 0;
	
	if (cpf == '11111111111')
		return false;
	if (cpf == '22222222222')
		return false;
	if (cpf == '33333333333')
		return false;
	if (cpf == '44444444444')
		return false;
	if (cpf == '55555555555')
		return false;
	if (cpf == '66666666666')
		return false;
	if (cpf == '77777777777')
		return false;
	if (cpf == '88888888888')
		return false;
	if (cpf == '99999999999')
		return false;
	if (cpf == '00000000000')
		return false;
	if (cpf == '01234567890')
		return false;
		
	
	for (i = 0; i < 9; i++)
	{
		d1 += c.charAt(i)*(10-i);
	}
        if (d1 == 0) return false;
	d1 = 11 - (d1 % 11);
	if (d1 > 9) d1 = 0;
	if (dv.charAt(0) != d1)
		return false;

	d1 *= 2;
	for (i = 0; i < 9; i++)
	{
		d1 += c.charAt(i)*(11-i);
	}
	d1 = 11 - (d1 % 11);
	if (d1 > 9) d1 = 0;
	if (dv.charAt(1) != d1)
		return false;
    
	return true;
}

/**  
 * @method checkMail(mail) 
 * @param  obj mail
 * @return bool
 * @desc:   
 */
function checkMail(obj)
{
    var er = new RegExp(/^[A-Za-z0-9_\-\.]+@[A-Za-z0-9_\-\.]{2,}\.[A-Za-z0-9]{2,}(\.[A-Za-z0-9])?/);
    
	if(typeof(obj) == "string"){
        if(er.test(obj)){ 
			return true; 
		}
    }
	
	else if(typeof(obj) == "object"){
        if(er.test(obj.value)){
        	return true;
        }
    }
	
	else{
    	return false;
    }
}


/**  
 * @method checkTextArea(textarea) 
 * @param  obj textarea
 * @return bool
 * @desc:   
 */
function checkTextArea(textarea) 
{
	var max_=80;
	if (textarea.value.length > max_) {
		alert(textarea.title);
		return false;
	}
	else {
		return true;
	}	
}
