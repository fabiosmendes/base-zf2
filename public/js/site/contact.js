
function sendFormContact() 
{
    var valid   = true;
    var message = '';
    
    /**
     * Validar Nome
     */
    if($("#name").val() == '') {
        message += $('#error_name').html() + "\n"; 
        valid = false;
    }
    
    /**
     * Validar E-mail
     */
    if($("#email").val() == '') {
        message += $('#error_email').html() + "\n"; 
        valid = false;
    } else {
        var checkEmail = checkMail( $("#email").val() );
        if(!checkEmail) {
            mensagem += $('#error_email').html() + "\n"; 
            valid = false;
        }
    }
    
    /**
     * Validar Telefone
     */ 
    if($("#phone").val() == '' || $("#phone").val() == '(__) ____-_____') {
        message += $('#error_phone').html() + "\n";    
        valid = false;
    } 
    
    if($("#captcha").val() == '') {
        message += $('#error_captcha').html() + "\n"; 
        valid = false;
    } 
    
    /**
     * Validar Mensagem
     */ 
    if($("#message").val() == '') {
        message += $('#error_message').html() + "\n"; 
        valid = false;
    } 
    
    if(valid) {
        $("#contact").submit();
    } else {
        alert(message);
        $("#name").focus();
    }
    
    return valid;
}

$(function() {  
    $("#name").focus();
    $("#phone").mask("(99) 9999-99999");
});