<?php 

chdir(dirname(__DIR__));

// Setup autoloading
require 'init_autoloader.php';

use Zend\Math\Rand;

$number = Rand::getFloat(true);
echo 'Rand::getFloat(true) = ';
var_dump($number);

echo " --------------------------------------- </br>";

$status = Rand::getBoolean(true);
echo 'Rand::getBoolean(true) = ';
var_dump($status);


echo " --------------------------------------- </br>";

$integer = Rand::getInteger(1, 100);
echo 'Rand::getInteger(1, 100) = ';
var_dump($integer);
