<?php 

require '../../vendor/autoload.php';

use Imagine\Image\Box;
use Imagine\Image\ImageInterface;
use Imagine\Gd\Imagine;

$dimensoes = '100x100';
if(isset($_GET['dim'])) {
   $dimensoes = $_GET['dim']; 
}

$mode = ImageInterface::THUMBNAIL_OUTBOUND;
if(isset($_GET['mode']) && $_GET['mode'] == 'inset') {
   $mode = ImageInterface::THUMBNAIL_INSET; 
}

$partsDim = explode('x', $dimensoes);
$w = $partsDim[0];
$h = $partsDim[1];

$imagine = new Imagine();
$size = new Box($w, $h);

if(isset($_GET['src'])) {
    $imgSrc = filter_var($_GET['src']);
    $ext = pathinfo($imgSrc, PATHINFO_EXTENSION);
    $imgNotExt = str_replace('.'.$ext, '', $imgSrc);
    
    $newImg = $imgSrc;
    if($dimensoes != '1500x1500') {
        $newImg = $imgNotExt.'-'.$dimensoes.'.'.$ext;
    }
    
    if(is_file($newImg)) {
        $image = file_get_contents($newImg);
    } else {
        $image = $imagine->open($imgSrc)
                         ->thumbnail($size, $mode)
                         ->save($newImg, array('quality' => 90));
    }
    
    header("Content-Type:image/jpg");
    echo $image;
}
