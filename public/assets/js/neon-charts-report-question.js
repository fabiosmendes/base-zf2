/**
 *	Neon Charts Scripts
 *
 *	Developed by Arlind Nushi - www.laborator.co
 */

;(function($, window, undefined)
{
	"use strict";
	
	$(document).ready(function()
	{
	
            // Morris.js Graphs
            if(typeof Morris != 'undefined')
            {
                // Bar Charts
                Morris.Bar({
                    element: 'chart3',
                    axes: true,
                    data: [
                        {x: '', y: getRandomInt(1,50), z: getRandomInt(1,120)},
                        {x: '', y: getRandomInt(1,50), z: getRandomInt(1,10)},
                        {x: '', y: getRandomInt(1,50), z: getRandomInt(1,10)},
                        {x: '', y: getRandomInt(1,50), z: getRandomInt(1,10)},
                        {x: '', y: getRandomInt(1,50), z: getRandomInt(1,10)},
                        {x: '', y: getRandomInt(1,50), z: getRandomInt(1,10)},
                    ],
                    xkey: 'x',
                    ykeys: ['y', 'z'],
                    labels: ['a1', 'a2'],
                    barColors: ['#003366', '#449966']
                });	
            }
		
                
           // Peity Graphs
            if($.isFunction($.fn.peity))
            {
                    $("span.pie").peity("pie", {colours: ['#0e8bcb', '#57b400'], width: 150, height: 25});
                    $(".panel span.line").peity("line", {width: 150});
                    $("span.bar").peity("bar", {width: 150});

                    var updatingChart = $(".updating-chart").peity("line", { width: 150 })

                    setInterval(function() 
                    {
                            var random = Math.round(Math.random() * 10);
                            var values = updatingChart.text().split(",");

                            values.shift()
                            values.push(random);

                            updatingChart.text(values.join(",")).change();
                            $("#peity-right-now").text(random + ' user' + (random != 1 ? 's' : ''));

                    }, 1000)
            }

                
            
	});
	
})(jQuery, window);


			
function data(offset) {
	var ret = [];
	for (var x = 0; x <= 360; x += 10) {
		var v = (offset + x) % 360;
		ret.push({
			x: x,
			y: Math.sin(Math.PI * v / 180).toFixed(4),
			z: Math.cos(Math.PI * v / 180).toFixed(4),
		});
	}
	return ret;
}
 
 
function getRandomInt(min, max) 
{
	return Math.floor(Math.random() * (max - min + 1)) + min;
}