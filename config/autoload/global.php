<?php

return array(
    'db' => array(
        'driver' => 'Pdo',
        'dsn' => 'mysql:dbname=base;host=localhost',
        'driver_options' => array(
            //PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF-8\''
        ),
    ),
    'service_manager' => array(
        'factories' => array(
            'Zend\Db\Adapter\Adapter' => 'Zend\Db\Adapter\AdapterServiceFactory'
        ),
    ),
    'mail' => array(
        'name' => 'localhost',  
        'host' => 'smtp.gmail.com',  
        'port'=> 587,  
        'connection_class' => 'login',  
        'connection_config' => array(  
            'username' => 'suporte@trumpagenciaweb.com.br',  
            'password' => 'trumpkey123',  
            'ssl'=> 'tls',  
            'from' => 'suporte@trumpagenciaweb.com.br',
        ),  
    ),
);
