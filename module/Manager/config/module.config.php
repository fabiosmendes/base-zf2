<?php

namespace Manager;

return array(
    'router' => array(
        'routes' => array(
            'manager-barra' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/manager/',
                    'defaults' => array(
                        'controller' => 'manager-dashboard',
                        'action' => 'index',
                    ),
                ),
            ),
            'manager-internal' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/manager[/:controller[/:action]][/:id]',
                    'constraints' => array(
                        'id' => '[0-9]+',
                    )
                ),
            ),
            'manager' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/manager[/:controller[/:action][/page/:page]]',
                    'defaults' => array(
                        'controller' => 'manager-dashboard',
                        'action' => 'index',
                        'page'  => 1,
                    ),
                ),
            ),
            'manager-block-access' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/manager-block-access',
                    'defaults' => array(
                        'controller' => 'users',
                        'action' => 'block-access'
                    ),
                ),
            ),
        ),
    ),
    'controllers' => array(
        'invokables' => array(
            // Defaults
            'manager-dashboard' => 'Manager\Controller\DashboardController',
        ),
    ),
    'controller_plugins' => array(
        'invokables' => array(
            'helperPlug' => 'Manager\Controller\Plugin\HelperPlug',
        )
    ),
    'module_layouts' => array(
        'Manager' => 'layout-admin/layout', // quando usar outro layout, colocar um nome único
    ),
    'view_manager' => array(
        'template_map' => array(
            'layout-admin/layout' => __DIR__ . '/../view/layout-admin/layout.phtml',
            //'error/404' => __DIR__ . '/../view/error/404.phtml',
        ),
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),
);
