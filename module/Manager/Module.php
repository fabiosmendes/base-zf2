<?php

namespace Manager;

use Zend\Mvc\MvcEvent;

class Module
{
    public function getModuleDependencies()
    {
        return array('Users');
    }
    
    public function onBootstrap(MvcEvent $e)
    {
        $em = $e->getApplication()->getEventManager();
        $em->attach(MvcEvent::EVENT_DISPATCH, new \Manager\Listeners\Dispatch(), -500);
        $em->attach(MvcEvent::EVENT_DISPATCH_ERROR, new \Manager\Listeners\DispatchError());
        
        $e->getApplication()
          ->getEventManager()
          ->getSharedManager()
          ->attach('Zend\Mvc\Controller\AbstractActionController', 'dispatch', 
                   new \Manager\Listeners\SelectLayout(), 
                   98);
        
    }
    
    
    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }
    
    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }
    
    public function getControllerConfig()
    {
        return array(
            'initializers' => array(
                function($instance, $sm) {
                    if($instance instanceof ConfigAwareInterface) {
                        $locator = $sm->getServiceLocator();
                        $instance->setSessionContainer($locator->get('SessionManager'));
                    }
                }    
            )
        );
    }
    
    public function getServiceConfig()
    {
        return include __DIR__ . '/config/service.config.php';
    }

    public function getViewHelperConfig()
    {
        return array(
            'factories' => array(
                'renderField' => function($sm) {
                    return new View\Helper\RenderField();
                },
                'formataDoc' => function($sm) {
                    return new View\Helper\FormataDoc();
                },        
                'paginationHelper' => function($sm) {
                    return new \Manager\View\Helper\PaginationHelper();
                },        
            ),       
        );
    }
}
