<?php

namespace Manager\Listeners;

use Zend\EventManager\EventInterface as Event;

class SelectLayout
{
    /**
     * Key for module Manager  
     */
    const MANAGER_MODULE = "Manager";
    
    public function __invoke(Event $e)
    {
        $controller = $e->getTarget();
        $controllerClass = get_class($controller);
        $moduleNamespace = substr($controllerClass, 0, strpos($controllerClass, '\\'));
        $config = $e->getApplication()->getServiceManager()->get('Config');
        $moduleHasLayout  = isset($config['module_layouts'][$moduleNamespace]);
        $isManagerModule = (strstr($moduleNamespace, self::MANAGER_MODULE) != "");
        
        if ($moduleHasLayout || $isManagerModule) {
            $controller->layout($config['module_layouts'][self::MANAGER_MODULE]);
        }
    }
}