<?php

namespace Manager\Listeners;

use Zend\Authentication\Storage\Session as SessionStorage;
use Zend\Mvc\MvcEvent;

class Dispatch
{
    
    const EXTEND_CLASS_TARGET = 'Manager\Controller\ManagerController';
    
    /**
     *
     * @var Zend\Mvc\MvcEvent 
     */
    private $e;
    
    private $controller;
    
    private function checkAuthenticationAndAuthorization($sm)
    {
        $auth = $sm->get('Zend\Authentication\AuthenticationService');
        $auth->setStorage(new SessionStorage("Manager"));

        if (!$auth->hasIdentity()) {
            return $this->controller->redirect()->toRoute('user-account-login');
        } else {
            // @todo - rever controle de acesso
            // 
//            $user   = $auth->getIdentity();
//            $roleId = $user->getRoleId();
//
//            $controllerClass = get_class($this->controller);
//            $actionName     = $this->e->getRouteMatch()->getParam('action');
//            $actionMethod   = $this->controller->getMethodFromAction($actionName);
//
//            $controlAccess    = $sm->get('Manager\Permissions\ControlAccess');
//            $condMethodName   = ($actionMethod != 'blockAccessAction');
//            $condCheckAcccess = !$controlAccess->checkAccess($controllerClass, $actionMethod, $roleId);
//            
//            if($condMethodName && $condCheckAcccess) {
//                return $this->controller->redirect()->toRoute('manager-block-access');
//            }
        }
    }
    
    public function __invoke(MvcEvent $e)
    {
        $this->e = $e;
        $this->controller = $this->e->getTarget();
        $sm           = $this->e->getApplication()->getServiceManager();    
        $matchedRoute = $e->getRouteMatch()->getMatchedRouteName();
        $parentClass  = get_parent_class($this->controller);
        
        $partsNameController = explode('\\', get_class($this->controller));
        $currentModule       = reset($partsNameController);
        
        if ( ((strstr($currentModule, 'Manager') != '')  && $matchedRoute != 'manager-auth') || 
                $parentClass == self::EXTEND_CLASS_TARGET) {    
           return $this->checkAuthenticationAndAuthorization($sm);
        }
    }
}
