<?php

namespace Manager\View\Helper;

use Zend\View\Helper\AbstractHelper;

class RenderField extends AbstractHelper
{
    public function __invoke($fieldForm, $label = '', $msgErrorDefault = '')
    {
        $templateField = 'manager/partials/form-field';
        $dados = array(
            'field' => $fieldForm,
            'label' => $label,
            'msgErrorDefault' => $msgErrorDefault,
        );
        
        $htmlForm = $this->getView()->render($templateField, $dados);
        return $htmlForm;
    }

}
