<?php

namespace Manager\Validate;

use Doctrine\ORM\EntityManager;

class AnuncioTag
{
    /**
     *
     * @var EntityManager
     */
    protected $em;
    
    /**
     * @var array 
     */
    protected $data;
    
    /**
     *
     * @var array 
     */
    protected $messages = array();
    
    public function __construct(EntityManager $em, array $data = array())
    {
        $this->em   = $em;
        $this->data = $data;
    }
    
    public function setData(array $data)
    {
        $this->data = $data;
    }
    
    public function validate()
    {
        $data   = $this->data;
        $status = true;
        
        if($data['condicao_pagamento'] == '') {
            $this->messages['condicao_pagamento'] = array(
                'vazio' => 'Informe uma condição de pagamento.'
            );
            $status = false;
        }
        
        if($data['texto_anuncio'] == '') {
            $this->messages['texto_anuncio'] = array(
                'vazio' => 'Informe um texto para o anúncio.'
            );
            $status = false;
        }
        
        $limiteTextoAnuncio = 120;
        if(strlen($data['texto_anuncio']) > $limiteTextoAnuncio) {
            $this->messages['texto_anuncio'] = array(
                'vazio' => 'O texto do anúncio não pode exceder o limite '.$limiteTextoAnuncio.' caracteres.'
            );
            $status = false;
        }
        
        
        if(!isset($data['tags']) || $data['tags'] == null) {
            $this->messages['tags[]'] = array(
                'vazio' => 'Informe alguma tag para o anúncio.'
            );
            $status = false;
        }
        
        if($data['descricao'] == '') {
            $this->messages['descricao'] = array(
                'vazio' => 'Informe uma descrição para o anúncio.'
            );
            $status = false;
        }
        /*
        if($data['banner_lista']['name'] == '') {
            $this->messages['banner_lista'] = array(
                'vazio' => 'Envie uma imagem nas dimensões especificadas.'
            );
            $status = false;
        }
        
        if($data['banner_anuncio']['name'] == '') {
            $this->messages['banner_anuncio'] = array(
                'vazio' => 'Envie uma imagem nas dimensões especificadas.'
            );
            $status = false;
        }
        */
        return $status;
    }
    
    public function getMessages()
    {
        return $this->messages;
    }
}