<?php

namespace Manager\Controller;

use Zend\Paginator\Paginator,
    Zend\Paginator\Adapter\ArrayAdapter;

class ModeloHelperController extends ManagerController
{
    protected $plugin;
    
    public function __construct()
    {
        $this->service = 'Manager\Service\CadastroAnuncioPublicidade';
        $this->entity  = 'Anunciante\Entity\Anuncio';
        $this->form    = 'Manager\Form\Anuncio';
        $this->route      = 'manager';
        $this->controller = 'anuncios-publicidade';  
        
        parent::__construct();
    }
    
    public function indexAction() 
    {   
        $service    = $this->getService('Manager\Service\Anuncios');
        $searchPars = $this->params()->fromQuery();
        $list       = $service->getListPublicidade($searchPars);
        
        $page = $this->params()->fromRoute('pager');    
        $paginator = new Paginator(new ArrayAdapter($list));
        $paginator->setCurrentPageNumber($page);
        $paginator->setDefaultItemCountPerPage(10);
        
        $message = $this->helperPlug()->getMessage();
        
        return $this->renderView(array('data' => $paginator, 
                                       'page' => $page, 
                                       'message'   => $message, 
                                       'queryPars' => json_encode($searchPars)));
    }
    
    public function novoAnuncioAction() 
    {           
       $service     = $this->getServiceLocator()->get('Manager\Service\Cadastros');
        $searchPars = $this->params()->fromQuery();
        $cities     = $this->helperPlug()->defineCities();
        $list       = $service->getList($searchPars, 10, $cities);
        
        $page      = $this->params()->fromRoute('pager');    
        $paginator = new Paginator(new ArrayAdapter($list));
        
        $paginator->setCurrentPageNumber($page);
        $paginator->setDefaultItemCountPerPage(10);
        
        $message = $this->helperPlug()->getMessage();
        
        return $this->renderView(array('data' => $paginator, 
                                       'page' => $page, 
                                       'message'   => $message, 
                                       'queryPars' => json_encode($searchPars)));
    }
    
    public function checarAcesso()
    {
        $pessoaId = $this->params()->fromRoute('id', 0);
        
        if($pessoaId == 0) {
            return false;
        }
        
        $pessoaService = $this->getServiceLocator()->get('Manager\Service\Cadastros'); 
        $pessoa = $pessoaService->getPessoaPorId($pessoaId);
        
        return $pessoa;
    }
    
    public function getPostData()
    {
        $data = parent::getPostData();
        $data['banner_anuncio'] = $this->params()->fromFiles('banner_anuncio');
        $data['banner_publicidade'] = $this->params()->fromFiles('banner_publicidade');
        
        return $data;
    }
    
    public function checkEnvio($cadastro, $formAnuncio)
    {
        if($this->checkPostData() ) {
            $data = $this->getPostData();
            if($cadastro->validarCadastro($data)) {
                $cadastro->criarAnuncio($data);
                return true;
            } else {
                $formAnuncio->setMessages( $cadastro->getMessages() );
            }
        } 
        
        return false;
    }
    
    public function posicaoAnuncioPublicidadeAction()
    {
        $cidadeId = $this->params()->fromPost('cidadeId');
        
        $cadastro = $this->getServiceLocator()->get($this->service);
        $posicao  = $cadastro->getPosicaoAnuncioCidade($cidadeId);
        
        return $this->renderJson(array('posicao' => $posicao));
    }
    
    public function getListaRegioes()
    {
        $serviceRegiao = $this->getServiceLocator()->get('Local\Service\Regioes');
        return $serviceRegiao->getCombo();
    }
    
    public function getListaEstados()
    {
        $serviceEstado = $this->getServiceLocator()->get('Local\Service\Estados');
        return $serviceEstado->getCombo();
    }
    
    public function getListaCidades()
    {
        $serviceCidade = $this->getServiceLocator()->get('Local\Service\Cidades');
        return $serviceCidade->getComboIdCidadesEstado();
    }
    
    public function getListaFaixasCidades()
    {
        $serviceFaixaCidade = $this->getServiceLocator()->get('Manager\Service\FaixasValoresCidades');
        return $serviceFaixaCidade->getListaItens();
    }
    
    public function getFormAnuncio($pessoa)
    {
        $form = new \Manager\Form\Anuncio\Publicidade();
        $form->get('id_pessoa')->setValue($pessoa->getId());
        $form->get('regiao')->setValueOptions($this->getListaRegioes());
        
        return $form;
    }
    
    public function renderViewPageForm($pars)
    {
        $pars['listaFaixaCidades'] = $this->getListaFaixasCidades();
        $pars['listaRegioes'] = $this->getListaRegioes();
        $pars['listaCidades'] = $this->getListaCidades();
        $pars['listaEstados'] = $this->getListaEstados();
        
        return $this->renderView($pars); 
    }
    
    public function getRedirectLista()
    {
        return $this->redirect()->toRoute('site', array('controller' => $this->controller));
    }
    
    public function criarAnuncioAction()
    {
        $pessoa   = $this->checarAcesso();
         if(!$pessoa) {
            return $this->redirect()->toRoute('site', array('controller' => $this->controller));
        }
        
        $cadastro    = $this->getServiceLocator()->get($this->service);
        $formAnuncio = $this->getFormAnuncio($pessoa); 
        
        if($this->checkEnvio($cadastro, $formAnuncio)) {
            return $this->getRedirectLista();
        }
        
        $pars = array(
            'form'   => $formAnuncio, 
            'pessoa' => $pessoa
        );
        
        return $this->renderViewPageForm($pars);
    }
    
    public function editarAnuncioAction()
    {
        $anuncioId = (int)$this->params()->fromRoute('id', 0);
        if($anuncioId > 0) {
            $cadastro = $this->getServiceLocator()->get($this->service);
            $cadastro->excluirAnuncio($anuncioId);
        } else {
            return $this->getRedirectLista();
        }
    }
    
    public function deleteAnuncioAction()
    {
        $access = $this->checkAuthorization(__METHOD__);
        if($access) {
            return $access;
        }
        
        $anuncioId = (int)$this->params()->fromRoute('id', 0);
        if($anuncioId > 0) {
            $cadastro = $this->getServiceLocator()->get($this->service);
            $cadastro->excluirAnuncio($anuncioId);
        }
        
        return $this->getRedirectLista();
    }
}