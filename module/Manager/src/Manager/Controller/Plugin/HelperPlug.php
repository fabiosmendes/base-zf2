<?php

namespace Manager\Controller\Plugin;

use Zend\Mvc\Controller\Plugin\AbstractPlugin;

class HelperPlug extends AbstractPlugin 
{
    public function defineCities()
    {
        $identity       = $this->controller->getIdentity('Manager');
        $roleNameHelper = $this->controller->getService('viewhelpermanager')->get('roleName');
        
        $roleName =  $roleNameHelper($identity->getRoleId());
        $cities   = $identity->getCities();
        if($roleName == 'Admin') {
            $cities = 'all';  
        }
        return $cities;
    }
    
    public function getMessage()
    {
        $flashMessenger = $this->controller->getService('ServiceFlashMessenger');
        $message = '';
        if ($flashMessenger->hasMessages()) {
            $messages = $flashMessenger->getMessages(); // segmentar mensagens (erro, por exemplo e success)
            $message = $messages[0];   
        }
        return $message;
    }
}
