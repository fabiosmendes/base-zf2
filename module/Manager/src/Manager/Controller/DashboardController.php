<?php

namespace Manager\Controller;

class DashboardController extends ManagerController 
{       
    
    const SERVICE = '\Module\Service\Service';
    
    public function __construct()
    {
        $this->service = self::SERVICE;
        $this->route = 'manager';
        
        parent::__construct();
    }
    
    public function indexAction()
    {
        if(!$this->getIdentity('Manager')) {
            return $this->redirect()->toRoute('user-account-login');
        }
        
        $pars = array();
        return $this->renderView($pars);
    }
}