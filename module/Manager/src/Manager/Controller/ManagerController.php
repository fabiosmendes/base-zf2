<?php

namespace Manager\Controller;

use BaseAdmin\Controller\CrudController;
use Users\Parameters\Roles;

class ManagerController extends CrudController 
{
    const GENERIC_MESSAGE = 'Operação realiza com sucesso!';
    
    protected $msgSuccessInsert = '';
    protected $msgSuccessUpdate = '';
    protected $msgSuccessDelete = '';
    
    public function getMessage($key)
    {
        return isset($this->messages[$key]) ? $this->messages[$key] : self::GENERIC_MESSAGE; 
    }
    
    public function isAdmin()
    {
        $user = $this->getIdentity('Manager');
        return ($user->getRole()->getId() === Roles::ADMIN);
    }
    
    public function checkAuthorization($actionMethod)
    { 
        $user   = $this->getIdentity('Manager');
        $roleId = $user->getRole()->getId(); 
        
        $controllerClass = get_class($this);
        $sm = $this->getServiceLocator();
        $controlAccess    = $sm->get('UsersManager\Permissions\ControlAccess');
        $condMethodName   = ($actionMethod != 'blockAccessAction');
        $condCheckAcccess = !$controlAccess->checkAccess($controllerClass, $actionMethod, $roleId);
        
        if($condMethodName && $condCheckAcccess) {
            return $this->redirect()->toRoute('manager-block-access');
        }
        
        return false;
    }
    
    public function getMainUnitUser()
    {
        $user = $this->getIdentity('Manager');
        return $user->getUnit();
    }
    
    public function getUnitsUser()
    {
        $user = $this->getIdentity('Manager');
        
        $unitsUser = $this->getService('UsersManager\Service\UserUnit');
        return $unitsUser->getUnitsUser($user->getId());
    }
    
    public function deleteAction() 
    {
        $access = $this->checkAuthorization(__METHOD__);
        if($access) {
            return $access;
        }
        
        return parent::deleteAction();       
    }
}

