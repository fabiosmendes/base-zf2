<?php

namespace Gallery\Entity;

use Doctrine\ORM\Mapping as ORM;
use Zend\Stdlib\Hydrator;

/**
 * Gallery
 *
 * @ORM\Table(name="galleries")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 * @ORM\Entity(repositoryClass="Gallery\Entity\Repository\GalleryRepository")
 */
class Gallery
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=70, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=70, nullable=true)
     */
    private $slug;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="integer", nullable=true, options={"comment" = ""})
     */
    private $status;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime", nullable=false)
     */
    private $modified;
    
    /**
     * @ORM\OneToMany(targetEntity="GalleryImage", mappedBy="gallery",cascade={"persist", "remove", "merge"})
     */
    private $images;
    
    public function __construct(array $options = array())
    {
        $hydrator = new Hydrator\ClassMethods();
        $hydrator->hydrate($options, $this);

        $this->images = new \Doctrine\Common\Collections\ArrayCollection();
       
        $this->status = 1;
        $this->created = new \DateTime("now");
        $this->modified = new \DateTime("now");
    }

    /**
     * 
     * @param \Gallery\Entity\GalleryImage $galleryImage
     * @return $this
     */
    public function addImage(GalleryImage $galleryImage)
    {
        $this->images[] = $galleryImage;
        $galleryImage->setGallery($this);
        return $this;
    }
    
    /**
     * 
     * @param \Gallery\Entity\GalleryImage $galleryImage
     */
    public function removeGalleryImage(GalleryImage $galleryImage)
    {
        $this->images->removeElement($galleryImage);
    }
   
    /**
     * 
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGalleryImages()
    {
        return $this->images;
    }
    
    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getSlug()
    {
        return $this->slug;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function getModified()
    {
        return $this->modified;
    }
    
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    public function setSlug($slug)
    {
        $this->slug = $slug;
        return $this;
    }

    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    public function setCreated()
    {
        $this->created = new \DateTime("now");
        return $this;
    }

    /**
     * @ORM\PreUpdate
     */
    public function setModified()
    {
        $this->modified = new \DateTime("now");
        return $this;
    }

    public function toArray()
    {
        $hydrator = new Hydrator\ClassMethods();
        return $hydrator->extract($this);
    }

    public function __toString()
    {
        return $this->name;
    }

}
