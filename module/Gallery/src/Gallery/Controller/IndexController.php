<?php

namespace Gallery\Controller;

use Base\Controller\BaseController;

class IndexController extends BaseController 
{
    public function indexAction() 
    {    
        $pars = array();
        
        $gallery = new \Gallery\Entity\Gallery();
        $gallery->setName('Main')
                ->setSlug('main')
                ->setDescription('main gallery for all porpuses');
        
        $galleryImage = new \Gallery\Entity\GalleryImage();
        $galleryImage->setName('arquivo_1.jpg')
                     ->setDescription('Imagem desc 1');
        
        $gallery->addImage($galleryImage);
        
        $galleryImage = new \Gallery\Entity\GalleryImage();
        $galleryImage->setName('arquivo_2.jpg')
                     ->setDescription('Imagem desc 2');
        
        $gallery->addImage($galleryImage);
        
        
        $em = $this->getEm();
        $em->persist($gallery);
        $em->flush();
        
        var_dump($gallery); exit();
        exit();
        
        return $this->renderView($pars);
    }
}
