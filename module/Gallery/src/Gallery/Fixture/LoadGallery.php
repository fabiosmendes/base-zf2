<?php

namespace Gallery\Fixture;

use Doctrine\Common\DataFixtures\AbstractFixture,
    Doctrine\Common\Persistence\ObjectManager;

class LoadGallery extends AbstractFixture
{
    public function load(ObjectManager $manager)
    {
        $gallery = new \Gallery\Entity\Gallery();
        $gallery->setName('Main')
                ->setSlug('main')
                ->setDescription('main gallery for all porpuses');
        
        $galleryImage = new \Gallery\Entity\GalleryImage();
        $galleryImage->setName('arquivo_1.jpg')
                     ->setDescription('Imagem desc 1');
        
        $gallery->addImage($galleryImage);
        
        $manager->persist($gallery);
        $manager->flush();
    }
    
    public function getOrder()
    {
        return 5;
    }
}
