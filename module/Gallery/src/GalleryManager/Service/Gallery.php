<?php

namespace GalleryManager\Service;

use Doctrine\ORM\EntityManager;
use Base\Service\AbstractService;

use GalleryManager\Service\GalleryImage;

class Gallery extends AbstractService
{
    /**
     *
     * @var GalleryManager\Service\GalleryImage 
     */
    protected $galleryImage;
    
    public function __construct(EntityManager $em, GalleryImage $galleryImage)
    {
        parent::__construct($em);
        $this->entity = "Gallery\Entity\Gallery";
        $this->galleryImage = $galleryImage;
    }
    
    public function getServiceGalleryImage()
    {
        return $this->galleryImage;
    }
}
