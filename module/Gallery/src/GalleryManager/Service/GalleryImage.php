<?php

namespace GalleryManager\Service;

use Doctrine\ORM\EntityManager;
use Base\Service\AbstractService;

use Base\Http\FileTransfer;

class GalleryImage extends AbstractService
{
    public function __construct(EntityManager $em, FileTransfer $fileTransfer)
    {
        parent::__construct($em);
        $this->entity = "Gallery\Entity\GalleryImage";
        $this->fileTransfer = $fileTransfer;
    }
    
    public function getImageGallery($galleryId)
    {
        return $this->getRepository()->findBy(array('gallery' => $galleryId), array('id' => 'DESC'));
    }
    
    /**
     * Save image
     * 
     * @todo - mover para service PostFile
     * @param type $data
     * @param type $image
     */
    public function sendNewImage($data, $image)
    {
        if($image['tmp_name'] == '') {
            return false;
        }
        
        $imageName = $data['name'];
        if($imageName === "") {
            $imageName = $image['name'];
            $data['name'] = $imageName;
        }
        
        $data['slug']  = \Base\Filter\Inflector\Slug::slugToLower($imageName, '-');
        $pathGallery =  $this->getPathMedia().'/gallery/';
        
        $data['file'] = $this->uploadFile($pathGallery, $image, $data['slug'].'_'.rand(99,999));
        if($data['file'] != '') {
            $gallery = $this->em->getReference('Gallery\Entity\Gallery', $data['gallery']);
            $data['gallery'] = $gallery; 
            $galleryImage = new $this->entity($data);
            $this->em->persist($galleryImage);
            $this->em->flush();
        }
    }
    
    public function setUpdateEntity($data) 
    {
        $gallery = $this->em->getReference('Gallery\Entity\Gallery', $data['gallery']);
        $data['gallery'] = $gallery;
            
        $galleryImage = $this->getRepository()->find($data['id']);
        $galleryImage->setName($data['name'])
                     ->setSlug($data['slug']) 
                     ->setDescription($data['description'])
                     ->setStatus($data['status'])
                     ->setGallery($gallery);
        
        if($data['file']) {
            $galleryImage->setFile($data['file']); 
        }
        
        $this->em->persist($galleryImage);
        $this->em->flush();
        return true;
    }
    
    /**
     * Save image
     * 
     * @todo - mover para service PostFile
     * @param type $data
     * @param type $image
     */
    public function updateImage($data, $image)
    {
        $imageName = $data['name'];
        if($imageName === "") {
            $imageName = $image['name'];
            $data['name'] = $imageName;
        }
        
        $data['status'] = (int) $data['status'];
        
        $data['slug']  = \Base\Filter\Inflector\Slug::slugToLower($imageName, '-');
        $pathGallery =  $this->getPathMedia().'/gallery/';
        
        $data['file'] = $this->uploadFile($pathGallery, $image, $data['slug'].'_'.rand(99,999));
        return $this->setUpdateEntity($data);
    }
    
    public function delete($id)
    {
        $image = $this->getRepository()->find($id);
        
        if($image) {
            $pathFile = $this->getPathMedia() . '/gallery/' . $image->getFile();
            $this->removeFile($pathFile);
            
            $tmpImage = $image; // retorna entidade para usar os dados
            $this->em->remove($image);
            $this->em->flush();
            return $tmpImage;
        }
        return false;
    }
}
