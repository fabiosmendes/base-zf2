<?php

namespace GalleryManager\Controller;

use Manager\Controller\ManagerController;

class GalleryController extends ManagerController 
{
    public function __construct()
    {
        $this->entity  = "Gallery\Entity\Gallery";
        $this->service = "GalleryManager\Service\Gallery";
        $this->form    = "GalleryManager\Form\Gallery";
        $this->controller = "manager-gallery";
        $this->route      = "general";
        
        parent::__construct();
    }
}
