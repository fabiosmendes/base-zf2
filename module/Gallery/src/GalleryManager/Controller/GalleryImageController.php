<?php

namespace GalleryManager\Controller;

use Zend\Paginator\Paginator,
    Zend\Paginator\Adapter\ArrayAdapter;

use Manager\Controller\ManagerController;

class GalleryImageController extends ManagerController 
{
    public function __construct()
    {
        $this->entity  = "Gallery\Entity\GalleryImage";
        $this->service = "GalleryManager\Service\GalleryImage";
        $this->form    = "GalleryManager\Form\SendImage";
        $this->controller = "manager-gallery-image";
        $this->route      = "general";
        
        parent::__construct();
    }
    
    public function indexAction() 
    {   
        
        $pars = array();
        
        return $this->renderView($pars);
    }
    
    public function getFlashMessenger()
    {
        $flashMessenger = $this->getServiceLocator()->get('ServiceFlashMessenger');
        $message = '';
        if ($flashMessenger->hasMessages()) {
            $messages = $flashMessenger->getMessages(); // segmentar mensagens (erro, por exemplo e success)
            $message = $messages[0];   
        }
        return $message;
    }
    
    public function listAction() 
    {   
        $galleryId = $this->params()->fromRoute('id', 0);
        if($galleryId == 0) {
            return $this->redirect()->toRoute('general', array('controller' => 'manager-gallery'));
        }
        
        $serviceGallery = $this->getServiceLocator()->get('GalleryManager\Service\Gallery');
        $serviceImages  = $serviceGallery->getServiceGalleryImage();
        $list = $serviceImages->getImageGallery($galleryId);
        
        $page = $this->params()->fromRoute('pager');    
        $paginator = new Paginator(new ArrayAdapter($list));
        $paginator->setCurrentPageNumber($page);
        $paginator->setDefaultItemCountPerPage(50);
        
        $message = $this->getFlashMessenger();
        
        return $this->renderView(array('data'      => $paginator,
                                       'galleryId' => $galleryId, 
                                       'page'    => $page, 
                                       'message' => $message));
    }
    
    public function sendImageAction()
    {
        $id = $this->params()->fromRoute('id');   
        
        $request = $this->getRequest();
        if($request->isPost()) {
            $serviceGallery = $this->getServiceLocator()->get('GalleryManager\Service\Gallery');
            $serviceImages  = $serviceGallery->getServiceGalleryImage();
        
            $data   = $request->getPost()->toArray();
            $image  = $this->params()->fromFiles('image'); 
            $serviceImages->sendNewImage($data, $image);
            
            return $this->redirect()->toRoute('general', array('controller' => 'manager-gallery-image', 
                                                               'action' => 'list', 
                                                               'id' => $id));
        }
        
        $frmUploadImage = new \GalleryManager\Form\SendImage();
        $frmUploadImage->get('gallery')->setValue($id);
        
        $message = '';
        return $this->renderView(array('form' => $frmUploadImage,
                                       'message' => $message));
    }
    
    public function editAction() 
    {
        $form = new $this->form();
        
        $request = $this->getRequest();
        
        if($request->isPost()) {
            $postData = $request->getPost()->toArray();
            $form->setData($postData);
            if($form->isValid()) {
                $service = $this->getServiceLocator()->get($this->service);
                $image  = $this->params()->fromFiles('image'); 
                $service->updateImage($postData, $image);
                
                $galleryId = $postData['gallery'];
                return $this->redirect()->toRoute($this->route, array('controller' => $this->controller,
                                                                      'action' => 'list',
                                                                      'id' => $galleryId));
            }
        }
        else {
            $repository  = $this->getEm()->getRepository($this->entity);
            $id          = $this->params()->fromRoute('id', 0);
            $entity      = $repository->find($id);
            
            if(null == $entity) {
                return $this->redirect()->toRoute($this->route, array('controller' => $this->controller, 
                                                                      'action'     => 'list', 'id' => 1));
            }
            else { 
                $form->setData($entity->toArray());
            }  
        }
        
        return $this->renderView(array('form' => $form));
    }
    
    public function deleteAction()
    {
        parent::deleteAction();
        
        return $this->redirect()->toRoute($this->route, array('controller' => $this->controller, 
                                                              'action'     => 'list', 'id' => 1));
    }
}
