<?php

namespace GalleryManager\Form;

use Zend\Form\Form;

class SendImage extends Form
{
    public function __construct($name = null)
    {
        parent::__construct('gallery');
        
        $this->setAttribute('method', 'post');
        $this->setInputFilter(new GalleryFilter());
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Hidden',
            'name' => 'id',
            'attributes' => array(
                'id' => 'id',
            )
        ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Hidden',
            'name' => 'gallery',
            'attributes' => array(
                'id' => 'gallery',
            )
        ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Hidden',
            'name' => 'status',
            'attributes' => array(
                'id' => 'status',
            )
        ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Hidden',
            'name' => 'file',
            'attributes' => array(
                'id' => 'file',
            )
        ));
        
        $this->add(array(
            'name' => 'name',
            'options' => array(
                'label' => 'Nome'
            ),
            'attributes' => array(
                'id' => 'name',
                'placeholder' => 'Digite o nome da imagem (se julgar necessário)',
                'class' => 'cx-form1',
                'style' => 'width:300px',
                'title' => 'Digite o nome da image',
            )
        ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Textarea',
            'name' => 'description',
            'options' => array(
                'label' => 'Descrição',
            ),
            'attributes' => array(
                'id' => 'description',
                'placeholder' => 'Digite uma descrição',
                'class' => 'cx-form2',
                'title' => 'Digite uma descrição',
            )
        ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\File',
            'name' => 'image',
            'options' => array(
                'type' => 'file',
                'label' => 'Imagem',
            ),
            'attributes' => array(
                'class' => '',
            )
        ));
    }
}
