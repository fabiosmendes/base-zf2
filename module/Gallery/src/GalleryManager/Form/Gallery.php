<?php

namespace GalleryManager\Form;

use Zend\Form\Form;

class Gallery extends Form
{
    public function __construct($name = null)
    {
        parent::__construct('gallery');
        
        $this->setAttribute('method', 'post');
        $this->setInputFilter(new GalleryFilter());
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Hidden',
            'name' => 'id',
            'attributes' => array(
                'id' => 'id',
            )
        ));
        
        $this->add(array(
            'name' => 'name',
            'options' => array(
                'label' => 'Nome'
            ),
            'attributes' => array(
                'id' => 'name',
                'placeholder' => 'Digite o nome da galeria',
                'class' => 'cx-form1',
                'title' => 'Digite o nome da galeria',
            )
        ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Textarea',
            'name' => 'description',
            'options' => array(
                'label' => 'Descrição Categoria',
            ),
            'attributes' => array(
                'id' => 'description',
                'placeholder' => 'Digite uma descrição',
                'class' => 'cx-form2',
                'title' => 'Digite uma descrição',
            )
        ));
    }
}
