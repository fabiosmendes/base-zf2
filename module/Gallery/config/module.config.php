<?php

namespace Gallery;

return array(
    'router' => array(
    ),
    'controllers' => array(
        'invokables' => array(
            // public 
            'gallery'               => __NAMESPACE__. '\Controller\IndexController',
            // manager
            'manager-gallery'       => __NAMESPACE__. 'Manager\Controller\GalleryController',
            'manager-gallery-image' => __NAMESPACE__. 'Manager\Controller\GalleryImageController',
        ),
    ),
    'view_manager' => array(
        'template_map' => array(
            'gallery/index/index' => __DIR__ . '/../view/gallery/index/index.phtml',
        ),
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),
    'doctrine' => array(
        'driver' => array(
            __NAMESPACE__ . '_driver' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(__DIR__ . '/../src/' . __NAMESPACE__ . '/Entity')
            ),
            'orm_default' => array(
                'drivers' => array(
                    __NAMESPACE__ . '\Entity' => __NAMESPACE__ . '_driver'
                )
            )
        ),
        'fixture' => array(
            __NAMESPACE__ . '_fixture' => __DIR__ . '/../src/' . __NAMESPACE__ . '/Fixture',
        ),
    ),
);