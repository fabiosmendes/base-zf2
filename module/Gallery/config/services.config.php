<?php

namespace Gallery;

return array(
    'factories' => array(
        'GalleryManager\Service\Gallery' => function($sm) {
            return new \GalleryManager\Service\Gallery($sm->get('Doctrine\ORM\EntityManager'), 
                                                       $sm->get('GalleryManager\Service\GalleryImage'));
        },
        'GalleryManager\Service\GalleryImage' => function($sm) {
            return new \GalleryManager\Service\GalleryImage($sm->get('Doctrine\ORM\EntityManager'), 
                                                            $sm->get('Base\Http\FileTransfer'));
        },
    ),
);