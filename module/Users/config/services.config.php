<?php

namespace Users;

use Users\Auth\Adapter as AuthAdapter;
use Users\Auth\AuthEmail;

return array(
    'factories' => array(
        'Users\Auth\Adapter' => function($sm) {
            return new AuthAdapter($sm->get('Doctrine\ORM\EntityManager'));
        },
        'Users\Auth\AuthEmail' => function($sm) {
            return new AuthEmail($sm->get('Doctrine\ORM\EntityManager'));
        },
        'Users\Service\Register' => function($sm) {
            return  new \Users\Service\Register($sm->get('Doctrine\ORM\EntityManager'));
        },
        'Users\Service\RecoverPassword' => function($sm) {
            return  new \Users\Service\RecoverPassword($sm->get('Doctrine\ORM\EntityManager'),
                                                       $sm->get('Base\Mail\Mail'));
        },       
        'UsersManager\Service\User' => function($sm) {
            return new \UsersManager\Service\User($sm->get('Doctrine\ORM\EntityManager'),
                                                  $sm->get('Base\Http\FileTransfer'));
        },        
        'UsersManager\Form\Role' => function($sm) {
            $em = $sm->get('Doctrine\ORM\EntityManager');
            $repo = $em->getRepository('Users\Entity\UserRole');
            $parent = $repo->fetchParent();

            return new \UsersManager\Form\Role('role', $parent);
        },
        'UsersManager\Form\Privilege' => function($sm) {
            $em = $sm->get('Doctrine\ORM\EntityManager');

            $repoResources = $em->getRepository('Users\Entity\UserResource');
            $resources = $repoResources->fetchPairs();

            return new \UsersManager\Form\Privilege("privilege", $resources);
        },
        'UsersManager\Service\Role' => function($sm) {
            return new \UsersManager\Service\Role($sm->get('Doctrine\ORM\Entitymanager'));
        },
        'UsersManager\Service\Resource' => function($sm) {
            $modules = $sm->get('UsersManager\Service\Modules');
            $modules->analysisApplicationControllers();
            $controllers = $modules->getListControllersManager();
            
            return new \UsersManager\Service\Resource($sm->get('Doctrine\ORM\Entitymanager'), $controllers);
        },
        'UsersManager\Service\Privilege' => function($sm) {
            return new \UsersManager\Service\Privilege($sm->get('Doctrine\ORM\Entitymanager'));
        },
        'UsersManager\Permissions\ControlAccess' => function($sm) {
            return new \UsersManager\Permissions\ControlAccess($sm->get('Doctrine\ORM\Entitymanager'));
        },        
        'UsersManager\Service\Modules' => function($sm) {
            return new \UsersManager\Service\Modules($sm->get('ModuleManager'));
        },        
        'UsersManager\Service\UserUnit' => function($sm) {
            return new \UsersManager\Service\UserUnit($sm->get('Doctrine\ORM\Entitymanager'));
        },        
    ),
);