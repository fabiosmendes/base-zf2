<?php

namespace Users;

return array(
    'router' => array(
        'routes' => array(
            'user-account-login' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/user-account-login',
                    'defaults' => array(
                        'controller' => 'users-login',
                        'action' => 'access-account',
                    ),
                ),
            ),
            'user-login-register' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/user-login-register',
                    'defaults' => array(
                        'controller' => 'users-login',
                        'action' => 'auth-register',
                    ),
                ),
            ),
            'user-logoff' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/user-logoff',
                    'defaults' => array(
                        'controller' => 'users-login',
                        'action' => 'logoff',
                    ),
                ),
            ),
            'user-account-dashboard' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/user-dashboard',
                    'defaults' => array(
                        'action' => 'index',
                        'controller' => 'users-dashboard',
                    ),
                ),
            ),
            'user-recover-password' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/user-dashboard',
                    'defaults' => array(
                        'action' => 'users-login',
                        'controller' => 'users-dashboard',
                    ),
                )
            )
        ),
    ),
    'controllers' => array(
        'invokables' => array(
            'users-login'      => 'Users\Controller\LoginController',
            'users-register'   => 'Users\Controller\RegisterController',
            
            // ACL
            'users'      => 'UsersManager\Controller\UsersController',
            'roles'      => 'UsersManager\Controller\RolesController',
            'resources'  => 'UsersManager\Controller\ResourcesController',
            'privileges' => 'UsersManager\Controller\PrivilegesController',
        ),
    ),
    'service_manager' => array(
        'factories' => array(
            'translator' => 'Zend\I18n\Translator\TranslatorServiceFactory',
        ),
    ),
    'translator' => array(
        'locale' => 'en_US',
        'translation_patterns' => array(
            array(
                'type' => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern' => '%s.mo',
            ),
        ),
    ),
    'view_manager' => array(
        'template_map' => array(
            'users/index/index'    => __DIR__ . '/../view/users/index/index.phtml',
        ),
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),
    'doctrine' => array(
        'driver' => array(
            __NAMESPACE__ . '_driver' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(__DIR__ . '/../src/' . __NAMESPACE__ . '/Entity')
            ),
            'orm_default' => array(
                'drivers' => array(
                    __NAMESPACE__ . '\Entity' => __NAMESPACE__ . '_driver'
                )
            )
        ),
        'fixture' => array(
            __NAMESPACE__ . '_fixture' => __DIR__ . '/../src/' . __NAMESPACE__ . '/Fixture',
        ),
    ),
);