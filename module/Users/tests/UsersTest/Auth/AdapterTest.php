<?php

namespace ClientesTest\Adapter;

use PHPUnit_Framework_TestCase as TestCase;
//use Base\Fixture\TestDataFixture as TestCase;
use Users\Auth\Adapter;

use Mockery;

class AdapterTest extends TestCase
{
    public function setUp()
    {
        $this->class = '\\Users\\Auth\\Adapter';
        parent::setUp();
    }

    public function testExistsClass()
    {
        $class = class_exists($this->class);
        $this->assertTrue($class);
    }
    
    public function testSetsGetsAdapter()
    {
        $doctrineMock = Mockery::mock('Doctrine\ORM\EntityManager');
        $adapter = new Adapter($doctrineMock);
        
        $this->assertEmpty($adapter->getEmail());
        $this->assertEmpty($adapter->getPassword());
        
        $email = 'john@beatles.com';
        $senha = '1962';
        
        $adapter->setEmail($email)
                ->setPassword($senha);
      
        $this->assertEquals($email, $adapter->getEmail());
        $this->assertEquals($senha, $adapter->getPassword());
    }   
    
    public function testIfMethotAutheticationReturnFalse()
    {
        $email = 'john@beatles.com';
        $senha = '1962';
        
        $userMock = Mockery::mock('Users\Entity\User');
        $userMock->shouldReceive('findByEmailAndPassword')->andReturn(false);
        
        $em = Mockery::mock('Doctrine\ORM\EntityManager');
        $em->shouldReceive('getRepository')->andReturn($userMock);
        
        $adapter = new Adapter($em);
        $adapter->setEmail($email)
                ->setPassword($senha);
        
        $result = $adapter->authenticate();
        $this->assertInstanceOf('Zend\Authentication\Result', $result);
        $this->assertNull($result->getIdentity());
    }
    
    public function testIfMethodAutheticationReturnEntity()
    {
        $email = 'john@beatles.com';
        $senha = '1962';
        
        $userDB = new \Users\Entity\User();
        $userDB->setEmail($email)
                  ->setPassword($senha);
        
        $entityName = 'Users\Entity\User';
        $userMock = Mockery::mock($entityName);
        $userMock->shouldReceive('findByEmailAndPassword')->andReturn($userDB);
        
        $em = Mockery::mock('Doctrine\ORM\EntityManager');
        $em->shouldReceive('getRepository')->andReturn($userMock);
        
        $adapter = new Adapter($em);
        $adapter->setEmail($email)
                ->setPassword($senha);
        
        $result = $adapter->authenticate();
        $this->assertInstanceOf('Zend\Authentication\Result', $result);
        $this->assertInstanceOf($entityName, $result->getIdentity()['user']);
        $this->assertEquals($email, $result->getIdentity()['user']->getEmail());
    }
}