<?php

namespace UsersTest\Entity;

use Base\Entity\EntityUnitTestCase;

/**
 * @group Entities
 */
class UserRolePrivilegeTest extends EntityUnitTestCase
{
    const ENTITY_NAME = '\Users\Entity\UserRolePrivilege';
    
    public function setUp()
    {
        $this->entity = self::ENTITY_NAME;
        parent::setUp();
    }
    
    public function dataProviderAttributes()
    {
        return array(
            array('id', 1),
            array('role', null),
            array('privilege', null),
            array('created', new \DateTime()),
            array('modified', new \DateTime())
        );
    }
    
}    