<?php

namespace UsersTest\Entity;

use Base\Entity\EntityUnitTestCase;

/**
 * @group Entities
 */
class UserTest extends EntityUnitTestCase
{
    const ENTITY_NAME = '\Users\Entity\User';
    
    public function setUp()
    {
        $this->entity = self::ENTITY_NAME;
        parent::setUp();
    }
    
    public function dataProviderAttributes()
    {
        return array(
            array('id', 1),
            array('role', new \Users\Entity\UserRole(array('id' => 1))),
            array('firstName', 'John'),
            array('lastName', 'Lennon'),
            array('email', 'john.beatles@gmail.com'),
            array('password', 'master1209'),
            array('salt', ''),
            array('created', new \DateTime()),
            array('modified', new \DateTime())
        );
    }
    
    /**
     * @dataProvider dataProviderAttributes
     */
    public function testChecksIfClassHasGetAndSetsOfAttributes($attribute, $value)
    {
        $get = 'get' . $this->formatAttribute($attribute);
        $set = 'set' . $this->formatAttribute($attribute);

        $class = new $this->entity();
        
        $methodSetExitis = method_exists($class, $set);
        $methodGetExitis = method_exists($class, $get);
        
        $this->assertTrue($methodSetExitis);
        $this->assertTrue($methodGetExitis);
        
        if(!$methodGetExitis || !$methodGetExitis) {
            return false;
        }
        
        $class->$set($value);
        $attributesNotApplicateForTest = array(
            'password', 'salt', 'created', 'modified',
        );
        
        if(!in_array($attribute, $attributesNotApplicateForTest)) {
            $this->assertEquals($value, $class->$get(), 'Fail Attribute -' . $attribute . ' | ');
        }
    }
    
    /**
     * @expectedException InvalidArgumentException
     * @expectedExceptionMessage ID accept only integers.
     */
    public function testChecksReceiveErrorIfIdNotInteger()
    {
        $class = new $this->entity();
        $class->setId('string');
    }
    
    /**
     * @expectedException InvalidArgumentException
     * @expectedExceptionMessage ID accept only number greater than zero.
     */
    public function testChecksIfReceiveErrorIfIdNegativeOrZero()
    {
        $class = new $this->entity();
        $class->setId(-1);
        $class->setId(0);
    }
    
    public function testChecksIfCreadedAtAndUpdatedAtAreInicialized()
    {
        $class = new $this->entity();
        $this->assertInstanceOf("\\DateTime", $class->getCreated());
        $this->assertInstanceOf("\\DateTime", $class->getModified());
    }
    
    public function testChecksIfSaltIsInicialized()
    {
        $class = new $this->entity();
        $this->assertNotNull($class->getSalt());
    }
    
    public function testChecksIfPasswordIsEncrypted()
    {
        $class = new $this->entity();
        $senha = 'password@master';
        $class->setPassword($senha);
        $this->assertNotEquals($senha, $class->getPassword());
    }
    
    public function testChecksMethodToArray()
    {
        $class = new $this->entity();
        $class->setId(1)
              ->setFirstName("John")
              ->setLastName("Lennon")  
              ->setEmail("j.beatles@gmail.com");
        
        $result = $class->toArray();
        
        $array = array(
          'id'   => 1,
          'first_name' => 'John',
          'last_name' => 'Lennon',
          'email'  => 'j.beatles@gmail.com',
        );
        
        foreach($array as $field => $value) {
            $this->assertEquals($result[$field], $value);
        }   
    }
    
    public function testChecksHydratorInConstrutor()
    {
        $array = array(
            'id'   => 1,
            'role' => new \Users\Entity\UserRole(array('id' => 1)),
            'first_name' => 'John',
            'last_name' => 'Lennon',
            'username' => 'lennon',
            'email'  => 'j.beatles@gmail.com',
            'phone'  => '+55 41 9643 0788',
            'photo'  => '',
            'role'  => null,
        );
        
        $class = new $this->entity($array);
        $classToArray = $class->toArray();
        unset($classToArray['password']);
        unset($classToArray['salt']);
        unset($classToArray['created']);
        unset($classToArray['modified']);
        
        $this->assertEquals($array, $classToArray);
        
    }
}
