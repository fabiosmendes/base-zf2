<?php

namespace UsersTest\Entity;

use Base\Entity\EntityUnitTestCase;

/**
 * @group Entities
 */
class UserPrivilegeTest extends EntityUnitTestCase
{
    const ENTITY_NAME = '\Users\Entity\UserPrivilege';
    
    public function setUp()
    {
        $this->entity = self::ENTITY_NAME;
        parent::setUp();
    }
    
    public function dataProviderAttributes()
    {
        return array(
            array('id', 1),
            array('resource', null),
            array('userRolePrivilege', null),
            array('name', 'name-attribute'),
            array('alias', 'Name Attribute'),
            array('display', 1),
            array('created', new \DateTime()),
            array('modified', new \DateTime())
        );
    }
    
    
    /**
     * @dataProvider dataProviderAttributes
     */
    public function testChecksIfClassHasGetAndSetsOfAttributes($attribute, $value)
    {
        $get = 'get' . $this->formatAttribute($attribute);
        $set = 'set' . $this->formatAttribute($attribute);

        $class = new $this->entity();
        
        $methodSetExitis = method_exists($class, $set);
        $methodGetExitis = method_exists($class, $get);
        
        $this->assertTrue($methodSetExitis);
        $this->assertTrue($methodGetExitis);
        
        if(!$methodGetExitis || !$methodGetExitis) {
            return false;
        }
        
        $class->$set($value); 
        
        $attributesNotApplicateForTest = array(
            'created', 'modified',
        );
        
        if(!in_array($attribute, $attributesNotApplicateForTest)) {
            $this->assertEquals($value, $class->$get(), 'Fail Attribute -' . $attribute . ' | ');
        }
    }
    
    /**
     * @expectedException InvalidArgumentException
     * @expectedExceptionMessage ID accept only integers.
     */
    public function testChecksReceiveErrorIfIdNotInteger()
    {
        $class = new $this->entity();
        $class->setId('string');
    }
    
    /**
     * @expectedException InvalidArgumentException
     * @expectedExceptionMessage ID accept only number greater than zero.
     */
    public function testChecksIfReceiveErrorIfIdNegativeOrZero()
    {
        $class = new $this->entity();
        $class->setId(-1);
        $class->setId(0);
    }
    
    public function testChecksIfCreadedAtAndUpdatedAtAreInicialized()
    {
        $class = new $this->entity();
        $this->assertInstanceOf("\\DateTime", $class->getCreated());
        $this->assertInstanceOf("\\DateTime", $class->getModified());
    }
}    