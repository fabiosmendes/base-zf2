<?php

namespace UsersTest\Entity\Repository;

use Doctrine\ORM\Mapping\ClassMetadata;

use Base\Fixture\TestDataFixture;
use UsersTest\Bootstrap;
use Users\Entity\Repository\UserRepository;
use Users\Fixture\LoadUsers;

/**
 * @group Repositories
 */
class UserRepositoryTest extends TestDataFixture
{   
    public $sm;
    public $em;
    public $repository;
    
    
    public function setUp()
    {        
        if(!Bootstrap::getTestDBClasses()){
            $this->markTestSkipped();
        }
        
        $this->sm = Bootstrap::getServiceManager();
        $this->em = $this->sm->get('Doctrine\ORM\EntityManager');
        
        parent::setUp();
        
    }
    
    public function loadData()
    {
        $classMetaCliente = new ClassMetadata('Users\Entity\User');
        $this->repository = new UserRepository($this->em, $classMetaCliente);
        
        $loadUsers = new LoadUsers();
        $loadUsers->load($this->em);
    }
    
    public function testFindByEmailAndPasswordError()
    {
        $this->loadData();
        $email    = 'john@gmail.com'; 
        $password = '12312131';
        
        $result = $this->repository->findByEmailAndPassword($email, $password);
        $this->assertFalse($result);        
    }
    
    public function testFindByEmailAndPasswordWithEmailFoundAndPasswordNotFount()
    {   
        $this->loadData();
        $result = $this->repository->findByEmailAndPassword('john@gmail.com', '12345678');
        $this->assertFalse($result);
    }
    
    public function testFindByEmailAndPasswordOk()
    {   
        $this->loadData();
        $result = $this->repository->findByEmailAndPassword('john@gmail.com', 'secretkey');
        $this->assertInstanceOf('Users\Entity\User', $result);
    }
    
}   