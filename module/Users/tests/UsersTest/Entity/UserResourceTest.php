<?php

namespace UsersTest\Entity;

use Base\Entity\EntityUnitTestCase;

/**
 * @group Entities
 */
class UserResourceTest extends EntityUnitTestCase
{
    const ENTITY_NAME = '\Users\Entity\UserResource';
    const ENTITY_PRIVILEGE_ONE_TO_MANY = '\Users\Entity\UserPrivilege';
    
    public function setUp()
    {
        $this->entity = self::ENTITY_NAME;
        parent::setUp();
    }
    
    public function dataProviderAttributes()
    {
        return array(
            array('id', 1),
            array('name', 'name-internal'),
            array('alias', 'name-user'),
            array('display', 0),
            array('privileges', null),
            array('created', new \DateTime()),
            array('modified', new \DateTime())
        );
    }
    
    /**
     * @dataProvider dataProviderAttributes
     */
    public function testChecksIfClassHasGetAndSetsOfAttributes($attribute, $value)
    {
        $get = 'get' . $this->formatAttribute($attribute);
        $set = 'set' . $this->formatAttribute($attribute);

        $class = new $this->entity();
        
        $methodSetExitis = method_exists($class, $set);
        $methodGetExitis = method_exists($class, $get);
        
        $this->assertTrue($methodSetExitis);
        $this->assertTrue($methodGetExitis);
        
        if(!$methodGetExitis || !$methodGetExitis) {
            return false;
        }
        
        $class->$set($value); 
        
        $attributesNotApplicateForTest = array(
            'created', 'modified',
        );
        
        if(!in_array($attribute, $attributesNotApplicateForTest)) {
            $this->assertEquals($value, $class->$get(), 'Fail Attribute -' . $attribute . ' | ');
        }
    }
    
    public function testChecksIfCreadedAtAndUpdatedAtAreInicialized()
    {
        $class = new $this->entity();
        $this->assertInstanceOf("\\DateTime", $class->getCreated());
        $this->assertInstanceOf("\\DateTime", $class->getModified());
    }
    
    public function getPrivilegeForResource($resource)
    {
        $entityPrivilege = self::ENTITY_PRIVILEGE_ONE_TO_MANY;
        
        $privilege = new $entityPrivilege(array('id' => 1));
        $resource->addPrivilege($privilege);
        
        return $privilege;
        
    }
    public function testIfMethodAddPrivilegeAcceptOnlyTypePrivilege()
    {
        $resource = new $this->entity();
        $privilege = $this->getPrivilegeForResource($resource);
        
        $this->assertEquals($privilege, $resource->getPrivileges()->first());
    }
    
    public function testIfMethodRemovePrivilegeDeleteItem()
    {
        $resource = new $this->entity();
        $privilege = $this->getPrivilegeForResource($resource);
        
        $this->assertEquals(1, $resource->getPrivileges()->count());
        $resource->removePrivilege($privilege);
        $this->assertEquals(0, $resource->getPrivileges()->count());
        
    }
    
    public function testIfMethodgetPrivilegesReturnTypeDefined()
    {
        $resource = new $this->entity();
        $this->assertInstanceOf('\Doctrine\Common\Collections\Collection', $resource->getPrivileges());
    }
    
}    