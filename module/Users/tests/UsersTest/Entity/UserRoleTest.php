<?php

namespace UsersTest\Entity;

use Base\Entity\EntityUnitTestCase;

/**
 * @group Entities
 */
class UserRoleTest extends EntityUnitTestCase
{
    const ENTITY_NAME = '\Users\Entity\UserRole';
    
    public function setUp()
    {
        $this->entity = self::ENTITY_NAME;
        parent::setUp();
    }
    
    public function dataProviderAttributes()
    {
        $parentEntity = self::ENTITY_NAME;
        $dataParent = array('id' => 1, 'name' => 'Admin');
        $parent = new $parentEntity($dataParent);
        
        return array(
            array('id', 2), 
            array('parent', $parent),
            //array('userRolePrivilege', null), // @todo - criar entidade e testes
            array('name', 'Admin'),
            array('isAdmin', 1),
            array('created', new \DateTime()),
            array('modified', new \DateTime())
        );
    }
    
    /**
     * @dataProvider dataProviderAttributes
     */
    public function testChecksIfClassHasGetAndSetsOfAttributes($attribute, $value)
    {
        $get = 'get' . $this->formatAttribute($attribute);
        $set = 'set' . $this->formatAttribute($attribute);

        $class = new $this->entity();
        
        $methodSetExitis = method_exists($class, $set);
        $methodGetExitis = method_exists($class, $get);
        
        $this->assertTrue($methodSetExitis);
        $this->assertTrue($methodGetExitis);
        
        if(!$methodGetExitis || !$methodGetExitis) {
            return false;
        }
        
        $class->$set($value); 
        
        $attributesNotApplicateForTest = array(
            'created', 'modified',
        );
        
        if(!in_array($attribute, $attributesNotApplicateForTest)) {
            $this->assertEquals($value, $class->$get(), 'Fail Attribute -' . $attribute . ' | ');
        }
    }
    
    /**
     * @expectedException InvalidArgumentException
     * @expectedExceptionMessage ID accept only integers.
     */
    public function testChecksReceiveErrorIfIdNotInteger()
    {
        $class = new $this->entity();
        $class->setId('string');
    }
    
    /**
     * @expectedException InvalidArgumentException
     * @expectedExceptionMessage ID accept only number greater than zero.
     */
    public function testChecksIfReceiveErrorIfIdNegativeOrZero()
    {
        $class = new $this->entity();
        $class->setId(-1);
        $class->setId(0);
    }
    
    public function testChecksIfCreadedAtAndUpdatedAtAreInicialized()
    {
        $class = new $this->entity();
        $this->assertInstanceOf("\\DateTime", $class->getCreated());
        $this->assertInstanceOf("\\DateTime", $class->getModified());
    }
    
    public function testIfEntityHasMethodToArray()
    {
        $class = new $this->entity();
        $methodToArrayExits = method_exists($class, "toArray");
        $this->assertTrue($methodToArrayExits);
    }
    
    /**
     * @todo implements entity UserRolePrivilege and add to array
     * @depends testIfEntityHasMethodToArray
     */
    public function testChecksMethodToArray()
    {
        $parent  = new $this->entity(array('id' => 1));
        
        $data = array(
            'id' => 2,
            'parent' => $parent,
            //'user_role_privilege' => null,
            'name' => "Admin",
            'is_admin' => 1,
        );
        
        $class = new $this->entity($data);
        $result = $class->toArray();
        
        foreach($data as $field => $value) {
            $this->assertEquals($result[$field], $value);
        }   
    }
    
    /**
     * @todo implements entity UserRolePrivilege and add to array
     * @depends testIfEntityHasMethodToArray
     */
    public function testChecksHydratorInConstrutor()
    {
        $parent  = new $this->entity(array('id' => 1));
        
        $data = array(
            'id' => 2,
            'parent' => $parent,
            //'user_role_privilege' => null,
            'name' => "Admin",
            'is_admin' => 1,
        );
        
        $class = new $this->entity($data);
        $classToArray = $class->toArray();
        
        unset($classToArray['created']);
        unset($classToArray['modified']);
        
        $this->assertEquals($data, $classToArray);
        
    }
    
    /**
     * @test
     */
    public function ifMethodSetParentAcceptOnlyTypeUserRole()
    {
        $parent  = new $this->entity(array('id' => 1));
        $current = new $this->entity(array('id' => 2));
        $entity  = $current->setParent($parent);
        $this->assertInstanceOf(self::ENTITY_NAME, $entity);
    }
    
    /**
     * @test
     */
    public function IfMethodGetUserRolePrivilegeReturnEntity()
    {
        $entity = new $this->entity(array('id' => 2));
        $entity->setUserRolePrivilege(new \Users\Entity\UserRolePrivilege());
        
        $this->assertInstanceOf('\Users\Entity\UserRolePrivilege', $entity->getUserRolePrivilege());
    }   
}
