<?php

namespace UsersTest\Service;

use Base\Service\ServiceUnitTestCase as TestCase;
use UsersTest\Bootstrap;

use Mockery;

/**
 * @group Services
 */
class UserManagerTest extends TestCase
{
    const SERVICE_NAME = 'Users\Service\UserManager';
    const EXTEND_CLASS = 'Base\Service\AbstractService';
    
    public function setUp()
    {
        if(!Bootstrap::getTestDBClasses()){
            $this->markTestSkipped();
        }
        
        $this->sm = Bootstrap::getServiceManager();
        $this->serviceName = self::SERVICE_NAME;
        $this->extendClass = self::EXTEND_CLASS;
        parent::setUp();
    }   
    
    public function testGetList()
    {
        $this->assertTrue(false);
    }
    
    public function testCheckPassword()
    {
        $this->assertTrue(false);
    }

    public function testCheckEmailUser()
    {
        $this->assertTrue(false);
    }
    
    public function testInsert()
    {
        $this->assertTrue(false);
    }
    
    public function testUpdate()
    {
        $this->assertTrue(false);
    }
    
    public function testGetCountUsers()
    {
        $this->assertTrue(false);
    }
    
    public function testDelete()
    {
        $this->assertTrue(false);
    }
    
    public function testGetCriptSenha()
    {
        $this->assertTrue(false);
    }
    
    public function testGetUser()
    {
        $this->assertTrue(false);
    }
    
    public function testSetUserIdAccount()
    {
        $this->assertTrue(false);
    }
}    