<?php

namespace UsersTest\Service;

use Base\Service\ServiceUnitTestCase as TestCase;
use UsersTest\Bootstrap;

use Mockery;

/**
 * @group Services
 */
class RecoverPasswordTest extends TestCase
{
    const SERVICE_NAME = 'Users\Service\RecoverPassword';
    const EXTEND_CLASS = 'Base\Service\AbstractService';
    
    public function setUp()
    {
        if(!Bootstrap::getTestDBClasses()){
            $this->markTestSkipped();
        }
        
        $this->sm = Bootstrap::getServiceManager();
        $this->serviceName = self::SERVICE_NAME;
        $this->extendClass = self::EXTEND_CLASS;
        parent::setUp();
    }   
    
    public function getMockServiceMail()
    {
        $serviceName = '\Base\Mail\Mail';
        $service = Mockery::mock($serviceName);
        
        $service->shouldReceive('setPage')->with('template-mail')->andReturn($service);
        $service->shouldReceive('setSubject')->with('Assunto')->andReturn($service);
        $service->shouldReceive('setTo')->with('destinatario@gmail.com')->andReturn($service);
        $service->shouldReceive('setData')->with(array('name' => 'Paul McCartney'))->andReturn($service);
        $service->shouldReceive('prepare')->andReturn($service);
        $service->shouldReceive('send')->andReturn(true);
        
        return $service;
    }
    
    public function dataProviderAttributes()
    {
        return array(
            array('em'),
            array('contactService'),
        );
    }
    
    /**
     * @test
     */
    public function verifySendMailRecoverPasswordAcceptInvalidData()
    {
        $service = new $this->serviceName($this->getEmMock(), $this->getMockServiceMail());
        $data = array();
        $result = $service->sendMailRecoverPassword($data);
        
        $this->assertTrue($result);
    }
    
} 