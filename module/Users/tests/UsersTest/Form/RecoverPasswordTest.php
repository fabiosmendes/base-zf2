<?php

namespace UsersTest\Form;

use Base\Form\FormTestCase;
use Users\Entity\User;
use Users\Form\RecoverPassword as FrmRecoverPassword;


class RecoverPasswordTest extends FormTestCase
{
    public function setUp()
    {
        parent::setUp();
        
        $this->entity = new User();
        $this->form   = new FrmRecoverPassword();
    }
    
    public function testExistsClass()
    {
        $class = class_exists('\\Users\\Form\\RecoverPassword');
        $this->assertTrue($class);
    }
    
    public function formFields() 
    {
        return array(
            array('email'),
            array('submit'),
        );
    }
    
    /**
     * 
     * @dataProvider formFields()
     */
    public function testIfFormHasDefinedFields($fieldName) 
    {
        $this->assertTrue($this->form->has($fieldName), 'Form field "'.$fieldName.'" not found.');           
    }
    
    /**
     * Test if the attributes that exist in form and are set in testing 
     */
    public function testIfAttributesAreMirrored()
    {
        $dataProviderTest = $this->formFields();
        $attributesDefined = array();
        foreach($dataProviderTest as $item) {
            $attributesDefined[] = $item[0];
        }
        
        $attributesFormClass = $this->form->getElements();
        $attributesForm = array();
        foreach($attributesFormClass as $key => $value) {
            $attributesForm[] = $key;
            $this->assertContains($key, $attributesDefined, 'Attribute "'.$key.'" not found in test class.');
        }
        
        $this->assertTrue(($attributesDefined === $attributesForm), 'There are no attributes sets.');        
    }
    
    public function getCompleteData()
    {
        return array(
            'email' => 'john@gmail.com',
        );
    }
    
    public function testIfCompleteDataAreValid()
    { 
        $data = $this->getCompleteData();
        
        $this->form->setData($data);
        $this->assertTrue($this->form->isValid());
    }
    
    public function testIfEmptyFieldEmailIsInvalid()
    { 
        $fieldName = 'email';
        $data = $this->getCompleteData();
        $data[$fieldName] = '';
        
        $this->form->setData($data);
        $this->assertFalse($this->form->isValid());
        $this->assertTrue(count($this->form->get($fieldName)->getMessages()) > 0);
    }
}