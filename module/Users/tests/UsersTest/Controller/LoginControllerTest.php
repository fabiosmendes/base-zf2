<?php

namespace UsersTest\Controller;

use Zend\Http\Request as HttpRequest;

use UsersTest\Bootstrap;
use Base\Controller\TestCaseController;

use Mockery;

/**
 * @group Controllers
 */
class LoginControllerTest extends TestCaseController
{
    const CONTROLLER_CLASS = 'Users\Controller\LoginController';
    const EXTEND_CLASS     = 'Base\Controller\BaseController';
    
    /**
     * Main Service Controller
     */
    const MAIN_SERVICE     = 'Events\Service\Event';

    public function setUp()
    {        
        $this->setApplicationConfig(Bootstrap::getConfig());
        parent::setUp();
        
        $this->controllerClass = self::CONTROLLER_CLASS;
        $this->extendClass     = self::EXTEND_CLASS;
    }
    
    public function testIfAccessAccountActionCanBeAccessed()
    {
        $this->dispatch('/user-account-login');
        $this->assertResponseStatusCode(200);
        
        $this->assertModuleName('Users');
        $this->assertControllerName('users-login');
        $this->assertControllerClass('LoginController');
        $this->assertActionName('access-account');
        $this->assertMatchedRouteName('user-account-login');
    }
    
    public function getAuthEntity()
    {
        $user = new \Users\Entity\User();
        $user->setId(1);
        $user->setFirstName('John');
        $user->setLastName('Lennon');
        $user->setEmail('user@app.com');
        
        return $user;
    }
    
    public  function  createMockAuthSuccess()
    {
        $serviceName = 'Zend\Authentication\AuthenticationService';
        $service = Mockery::mock($serviceName);
        $service->shouldReceive('setStorage')->andReturn(null);
        $service->shouldReceive('hasIdentity')->andReturn(true);
        $data = array('user' => $this->getAuthEntity());
        $service->shouldReceive('getIdentity')->andReturn($data);
        
        $authClass = Mockery::mock('AuthClass');
        $authClass->shouldReceive('isValid')->andReturn(true);
        $service->shouldReceive('authenticate')->andReturn($authClass);
        
        $this->services[$serviceName] = $service;
    }
    
    public function createMockSession()
    {
        $serviceName = 'SessionManager';
        $service = Mockery::mock($serviceName);
        $service->shouldReceive('offsetGet')->with('palavra')->andReturn('cantada');
        $service->shouldReceive('offsetGet')->with('modelo')->andReturn('padrao');
        
        $this->services[$serviceName] = $service;
    }
    
    public function createMockAuthAdapterValid()
    {
        $serviceName = 'Users\Auth\Adapter';
        $service = Mockery::mock($serviceName);
        $service->shouldReceive('setEmail')->andReturn($service);
        $service->shouldReceive('setPassword');
        
        $this->services[$serviceName] = $service;
    }
    
    public function createServicesForAuthAccountSuccess()
    {
        $this->createMockAuthSuccess();
        //$this->createMockSession();
        //$this->createMockAuthAdapterValid();
        //$this->createMockFlashMessager();
        
        $this->setMockServices();
    }
    
    public function testIfSendValidPostForAccessAccountActionIsRedirectedForDashboard()
    {
        $this->createServicesForAuthAccountSuccess();
        
        $post = array(
            'email'    => 'user@app.com',
            'password' => 'secret_key',
        );
        $this->dispatch('/user-account-login', HttpRequest::METHOD_POST, $post);
        
        $this->assertResponseStatusCode(302);
        
        $this->assertModuleName('Users');
        $this->assertControllerName('users-login');
        $this->assertControllerClass('LoginController');
        $this->assertActionName('access-account');
        $this->assertMatchedRouteName('user-account-login');
        
        $this->assertRedirectTo('/user-dashboard');    
    }
    
    public  function  createMockAuthFail()
    {
        $serviceName = 'Zend\Authentication\AuthenticationService';
        $service = Mockery::mock($serviceName);
        $service->shouldReceive('setStorage')->andReturn(null);
        
        $authClass = Mockery::mock('AuthClass');
        $authClass->shouldReceive('isValid')->andReturn(false);
        $service->shouldReceive('authenticate')->andReturn($authClass);
        
        $this->services[$serviceName] = $service;
    }
    
    public function createServicesForAuthAccountFail()
    {
        $this->createMockAuthFail();
        $this->setMockServices();
    }
    
    public function createServicesForInvalidFormPost()
    {
        $this->createMockFlashMessager();
        $this->setMockServices();
    }
    
    public function testIfSendPostForFormInvalid()
    {
        $this->createServicesForInvalidFormPost();
        
        $post = array(
            'email'    => 'user@app.com',
            'password' => '',
        );
        $this->dispatch('/user-account-login', HttpRequest::METHOD_POST, $post);
        
        $this->assertResponseStatusCode(200);
        
        $this->assertModuleName('Users');
        $this->assertControllerName('users-login');
        $this->assertControllerClass('LoginController');
        $this->assertActionName('access-account');
        $this->assertMatchedRouteName('user-account-login');
        
    }
    
    public function testIfSendInvalidPostForAccessAccountActionIsRedirectedForDashboard()
    {
        $this->createServicesForAuthAccountFail();
        
        $post = array(
            'email'    => 'user@app.com',
            'password' => 'secret_key_1111',
        );
        $this->dispatch('/user-account-login', HttpRequest::METHOD_POST, $post);
        
        $this->assertResponseStatusCode(302);
        
        $this->assertModuleName('Users');
        $this->assertControllerName('users-login');
        $this->assertControllerClass('LoginController');
        $this->assertActionName('access-account');
        $this->assertMatchedRouteName('user-account-login');
        
        $this->assertRedirectTo('/user-account-login');
    }
    
    public function createServicesForAuthRegisterSuccess()
    {
        //$this->createMockAuthFail();
        
        $this->setMockServices();
    }
    
    public function testIfAuthRegisterActionSendForDashboard()
    { 
        $this->createServicesForAuthRegisterSuccess();
        
        $this->dispatch('/user-login-register');
        $this->assertResponseStatusCode(302);
        
        $this->assertModuleName('Users');
        $this->assertControllerName('users-login');
        $this->assertControllerClass('LoginController');
        $this->assertMatchedRouteName('user-login-register');
        
        $this->assertRedirectTo('/user-account-login');
    }
    
    public function testIfAuthRegisterActionCanBeAccessed()
    {
        //$this->createMockAuthRegister();
        
        $this->dispatch('/user-login-register');
        $this->assertResponseStatusCode(302);
        
        $this->assertModuleName('Users');
        $this->assertControllerName('users-login');
        $this->assertControllerClass('LoginController');
        $this->assertActionName('auth-register');
        $this->assertMatchedRouteName('user-login-register');
        
        $this->assertRedirectTo('/user-account-login');
    }
    
    public function createMockForRecoverPassword()
    {
        $serviceName = 'Users\Service\RecoverPassword';
        $service = Mockery::mock($serviceName);
        $service->shouldReceive('sendMailRecoverPassword')->andReturn(true);
        
        $this->services[$serviceName] = $service;
    }
    
    public function createMockForRecoverPasswordFail()
    {
        $serviceName = 'Users\Service\RecoverPassword';
        $service = Mockery::mock($serviceName);
        $service->shouldReceive('sendMailRecoverPassword')->andReturn(false);
        
        $this->services[$serviceName] = $service;
    }
    
    public function createServicesForRecoverPassword()
    {
        $this->createMockForRecoverPassword();
        $this->setMockServices();
    }
    
    public function createServicesForRecoverPasswordFail()
    {
        $this->createMockForRecoverPasswordFail();
        $this->setMockServices();
    }
    
    public function testIfRecoverPasswordActionCanBeAccessed()
    {
        $this->createServicesForRecoverPassword();
        
        $this->dispatch('/users-login/recover-password');
        $this->assertResponseStatusCode(200);
        
        $this->assertModuleName('Users');
        $this->assertControllerName('users-login');
        $this->assertControllerClass('LoginController');
        $this->assertActionName('recover-password');
        $this->assertMatchedRouteName('general');
    }
    
    public function testIfSendPostValidRecoverPasswordAction()
    {
        $this->createServicesForRecoverPassword();
        
        $post = array(
            'email' => 'fbiosm@gmail.com'
        );
        
        $this->dispatch('/users-login/recover-password', HttpRequest::METHOD_POST, $post);
        
        $this->assertResponseStatusCode(302);
        
        $this->assertModuleName('Users');
        $this->assertControllerName('users-login');
        $this->assertControllerClass('LoginController');
        $this->assertActionName('recover-password');
        $this->assertMatchedRouteName('general');
    }
    
    
    public function testIfSendInvalidPostForRecoverPasswordAction()
    {
        $this->createServicesForRecoverPasswordFail();
        
        $post = array(
            'email' => 'fabio@gmail.com'
        );
        
        $this->dispatch('/users-login/recover-password', HttpRequest::METHOD_POST, $post);
        $this->assertResponseStatusCode(302);
        
        $this->assertModuleName('Users');
        $this->assertControllerName('users-login');
        $this->assertControllerClass('LoginController');
        $this->assertActionName('recover-password');
        $this->assertMatchedRouteName('general');
    }
}