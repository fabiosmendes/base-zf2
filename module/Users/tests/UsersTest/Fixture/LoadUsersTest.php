<?php

namespace UsersTest\Fixture;

use PHPUnit_Framework_TestCase as TestCase;
use Users\Fixture\LoadUsers;

use Mockery;

class LoadUsersTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();
    }
    
    public function testLoadData()
    {
        $entityManagerMock = Mockery::mock('Doctrine\ORM\EntityManager');
        $entityManagerMock->shouldReceive('persist')->andReturn(true);
        $entityManagerMock->shouldReceive('flush');
        
        $loadUsers = new LoadUsers();
        $campo = $loadUsers->load($entityManagerMock);
        
        $this->assertEmpty($campo);
    }
    
    public function testMetodoOrder()
    {
        $loadUsers = new LoadUsers();
        $this->assertTrue(is_int($loadUsers->getOrder()));
    }
    
}