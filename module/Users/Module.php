<?php

namespace Users;

use Zend\Authentication\Storage\Session as SessionStorage;
use Zend\Mvc\MvcEvent;

class Module
{
    public function onBootstrap(MvcEvent $e)
    {
        $em = $e->getApplication()->getEventManager();
        $em->attach(MvcEvent::EVENT_DISPATCH, array($this, 'onDispatch'));
    }
    
    public function onDispatch(MvcEvent $e)
    {
        $sm = $e->getApplication()->getServiceManager();
        $controller = $e->getTarget();
        $serviceManager = $e->getApplication()->getServiceManager();
        $partsNameController = explode('\\', get_class($controller));
        $currentModule     = reset($partsNameController);
        $namespaceRestrict = 'UsersAccount';
        
        if ($currentModule == $namespaceRestrict) {    
            $auth = $serviceManager->get('Zend\Authentication\AuthenticationService');
            $auth->setStorage(new SessionStorage($namespaceRestrict));
            
            if (!$auth->hasIdentity()) {
                return $controller->redirect()->toRoute('user-account-login');
            }
        }
    }
    
    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }
    
    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ . 'Manager' => __DIR__ . '/src/' . __NAMESPACE__ . 'Manager',
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getServiceConfig()
    {
        return include __DIR__ . '/config/services.config.php';
    }
    
    public function getViewHelperConfig()
    {
        return array(
            'factories' => array(
                'roleName' => function($sm) {
                    $serviceLocator   = $sm->getServiceLocator();
                    $roleService = $serviceLocator->get('Manager\Service\Role');
                    return new \UsersManager\View\Helper\RoleName($roleService);
                },
                'UserIdentity' => function(\Zend\View\HelperPluginManager $helperPluginManager) {
                    $serviceLocator   = $helperPluginManager->getServiceLocator();
                    return new \UsersManager\View\Helper\UserIdentity($serviceLocator->get('Doctrine\ORM\EntityManager'));
                },   
            ),
        );
    }
}
