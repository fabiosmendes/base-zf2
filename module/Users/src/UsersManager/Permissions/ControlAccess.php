<?php

namespace UsersManager\Permissions;

use Doctrine\ORM\EntityManager;

class ControlAccess
{
    protected $em;
    protected $infoResource;
    
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }
    
    public function checkInfoResourcePrivilege($controller, $method)
    {
        $controllerEscape = str_replace("\\", "\\\\", $controller);
             
        $sqlRole  = " SELECT ur.id AS resource_id, ur.alias AS resource, up.id AS privilege_id, up.alias AS privilege";
        $sqlRole .= " FROM users_resources ur, users_privileges up";
        $sqlRole .= " WHERE ur.name =  '".$controllerEscape."'";
        $sqlRole .= " AND up.name =  '".$method."'";
        $sqlRole .= " AND ur.id = up.resource_id";
        
        $stmt = $this->em->getConnection()->prepare($sqlRole);
        $stmt->execute();
        $this->infoResource = $stmt->fetch(); 
        
        return $this->infoResource;
    }
    
    public function getInfoResource()
    {
        return $this->infoResource;
    }
    
    public function checkRolePrivilege($privilegeId, $roleId)
    {
        $sqlRolePrivilege  = " SELECT id";
        $sqlRolePrivilege .= " FROM users_roles_privileges urp ";
        $sqlRolePrivilege .= " WHERE urp.privilege_id =  '".$privilegeId."'";
        $sqlRolePrivilege .= " AND urp.role_id =  '".$roleId."'";
        
        $stmt = $this->em->getConnection()->prepare($sqlRolePrivilege);
        $stmt->execute();
        
        return ($stmt->fetch() != false);
    }
    
    public function isRoleAdmin($roleId) 
    {
        $sqlRoleAdmin  = " SELECT is_admin";
        $sqlRoleAdmin .= " FROM users_roles ";
        $sqlRoleAdmin .= " WHERE id =  '".$roleId."'";
        $sqlRoleAdmin .= " AND is_admin = 1";
        
        $stmt = $this->em->getConnection()->prepare($sqlRoleAdmin);
        $stmt->execute();
        
        return $stmt->fetch();
    }
    
    public function checkAccess($controller, $method, $roleId)
    {
        if($this->isRoleAdmin($roleId)) {
            return true;
        }
        
        $infoResource = $this->checkInfoResourcePrivilege($controller, $method);
        if($infoResource) {
            $privilegeId = $infoResource['privilege_id'];
            return $this->checkRolePrivilege($privilegeId, $roleId);
        }
        
        return false;
    }
}

