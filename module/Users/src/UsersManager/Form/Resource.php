<?php

namespace UsersManager\Form;

use Zend\Form\Form;

class Resource extends Form
{
    public function __construct($name = null)
    {
        parent::__construct('resources');

        $this->setAttribute('method', 'post');

        $id = new \Zend\Form\Element\Hidden('id');
        $this->add($id);
        
        $this->add(array(
            'name' => 'alias',
            'type' => 'Zend\Form\Element\Text',
            'options' => array(
                'label' => 'Apelido'
            ),
            'attributes' => array(
                'id' => 'alias',
                'placeholder' => '',
                'class' => '',
                'style' => 'width:500px;'
            )
        ));
        
        $nome = new \Zend\Form\Element\Text("name");
        $nome->setLabel("Nome: ")
             ->setAttribute('placeholder', "Entre com o nome")
             ->setAttribute('class', 'medium')
             ->setAttribute('style', 'width:500px;');
        $this->add($nome);
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'display',
            'options' => array(
                'label' => 'Exibir na lista de Recursos',
                'use_hidden_element' => true,
                'checked_value'   => '1',
            )
        ));
        
        
        $this->add(array(
            'name' => 'submit',
            'type' => 'Zend\Form\Element\Submit',
            'attributes' => array(
                'value' => 'Salvar',
                'class' => 'btn btn-success'
            )
        ));
        
        
    }

}
