<?php

namespace UsersManager\Form\User;

use Zend\InputFilter\InputFilter;

class EditUserFilter extends InputFilter 
{
    public function __construct() 
    {
        $this->add(array(
            'name' => 'first_name',
            'required' => true,
            'filters' => array(
                array('name' => 'StripTags'),
                array('name' => 'StringTrim'),
             ),
            'validators' => array(
                array(
                    'name' => 'NotEmpty',
                    'options' => array(
                        'messages' => array('isEmpty' => 'Informe seu primeiro nome.')
                    )
                ),
            )
        ));
        
        $this->add(array(
            'name' => 'last_name',
            'required' => true,
            'filters' => array(
                array('name' => 'StripTags'),
                array('name' => 'StringTrim'),
             ),
            'validators' => array(
                array(
                    'name' => 'NotEmpty',
                    'options' => array(
                        'messages' => array('isEmpty' => 'Informe seu sobrenome.')
                    )
                ),
            )
        ));
        
        $this->add(array(
            'name' => 'email',
            'required' => true,
            'filters' => array(
                array('name' => 'StringTrim'),
            ),
            'validators' => array(
                array(
                    'name' => 'NotEmpty',
                    'options' => array(
                        'messages' => array('isEmpty' => 'E-mail não pode estar vazio.'),
                    ),
                ),
                array(
                    'name' => 'EmailAddress',
                    'options' => array(
                        'messages' => array(
                            'emailAddressInvalidFormat' => 'E-mail inválido.',
                            'emailAddressInvalidHostname' => 'O domínio do e-mail inválido.',
                            'hostnameUnknownTld' => 'Houve um problema ao identificar o domínio.',
                            'hostnameLocalNameNotAllowed' => 'Houve um problema ao identificar  o e-mail.',
                        ),
                    ),
                ),
            )
        ));
        
        $this->add(array(
            'name' => 'current_password',
            'required' => false,
            'filters' => array(
                array('name' => 'StringTrim'),
            ),
            'validators' => array(
                array(
                    'name' => 'Zend\Validator\StringLength',
                    'options' => array(
                        'min' => 5,
                        'max' => 63,
                    ),
                ),
            ),
        ));
        
        $this->add(array(
            'name' => 'new_password',
            'required' => false,
            'filters' => array(
                array('name' => 'StringTrim'),
            ),
            'validators' => array(
                array(
                    'name' => 'Zend\Validator\StringLength',
                    'options' => array(
                        'min' => 5,
                        'max' => 63,
                    ),
                ),
            ),   
        ));
    }
}
