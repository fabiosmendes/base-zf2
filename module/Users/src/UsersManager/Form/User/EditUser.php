<?php

namespace UsersManager\Form\User;

use Zend\Form\Form;

class EditUser extends Form
{

    protected $em = null;

    public function __construct($name = null, $roles = array())
    {
        parent::__construct('user_edit');
        $this->setAttribute('method', 'post');
        $this->setInputFilter(new EditUserFilter());
    }

    public function init()
    {

        $this->add(array(
            'name' => 'id',
            'type' => 'Zend\Form\Element\Hidden',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Hidden',
            'name' => 'account',
            'attributes' => array(
                'id' => 'account',
            )
        ));

        $this->setSelectUnit();

        $this->setCheckboxUnits();

        $this->setSelectRole();

        $this->add(array(
            'name' => 'first_name',
            'options' => array(
                'type' => 'text',
                'label' => 'Primeiro Nome'
            ),
            'attributes' => array(
                'placeholder' => '',
                'class' => 'form-control',
            )
        ));

        $this->add(array(
            'name' => 'last_name',
            'options' => array(
                'type' => 'text',
                'label' => 'Sobrenome'
            ),
            'attributes' => array(
                'placeholder' => '',
                'class' => 'form-control',
            )
        ));

        $this->add(array(
            'name' => 'username',
            'options' => array(
                'type' => 'text',
                'label' => 'Username'
            ),
            'attributes' => array(
                'placeholder' => '',
                'class' => 'form-control',
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Email',
            'name' => 'email',
            'options' => array(
                'type' => 'text',
                'label' => 'E-mail'
            ),
            'attributes' => array(
                'placeholder' => '',
                'class' => 'form-control',
            )
        ));

        $this->add(array(
            'name' => 'phone',
            'type' => 'Zend\Form\Element\Text',
            'options' => array(
                'label' => 'Telefone'
            ),
            'attributes' => array(
                'id' => 'phone',
                'placeholder' => '',
                'class' => 'telefone form-control',
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Password',
            'name' => 'new_password',
            'options' => array(
                'type' => 'text',
                'label' => 'Nova Senha'
            ),
            'attributes' => array(
                'placeholder' => '',
                'class' => 'form-control',
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Password',
            'name' => 'confirm_new_password',
            'options' => array(
                'type' => 'text',
                'label' => 'Confirmar Nova Senha'
            ),
            'attributes' => array(
                'placeholder' => '',
                'class' => 'form-control',
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\File',
            'name' => 'photo',
            'options' => array(
                'type' => 'text',
                'label' => 'Foto'
            ),
            'attributes' => array(
                'placeholder' => '',
                'class' => 'form-control file2 inline btn btn-primary',
                'data-label' => "<i class='glyphicon glyphicon-file'></i> Procurar",
            )
        ));
    }

    /**
     * @link https://github.com/doctrine/DoctrineModule/blob/master/docs/form-element.md
     */
    public function setSelectRole()
    {
        $this->add(array(
            'type' => 'DoctrineModule\Form\Element\ObjectSelect',
            'name' => 'role_id',
            'options' => array(
                'label' => 'Grupo',
                'display_empty_item' => true,
                'empty_item_label' => '---',
                'disable_inarray_validator' => true, // desabilitar validação do elemento no select, 
                'object_manager' => $this->getObjectManager(),
                'target_class' => 'Users\Entity\UserRole',
                'property' => 'name',
                'is_method' => true,
                'find_method' => array(
                    'name' => 'findBy',
                    'params' => array(
                        'criteria' => array(),
                        'orderBy' => array('name' => 'ASC'),
                    ),
                ),
            ),
            'attributes' => array(
                'class' => 'form-control',
            )
        ));
    }

    /**
     * @link https://github.com/doctrine/DoctrineModule/blob/master/docs/form-element.md
     */
    public function setSelectUnit()
    {
        $this->add(array(
            'type' => 'DoctrineModule\Form\Element\ObjectSelect',
            'name' => 'unit',
            'options' => array(
                'label' => 'Unidade',
                'display_empty_item' => true,
                'empty_item_label' => '---',
                'disable_inarray_validator' => true, // desabilitar validação do elemento no select, 
                'object_manager' => $this->getObjectManager(),
                'target_class' => 'Units\Entity\Unit',
                'property' => 'local',
                'is_method' => true,
                'find_method' => array(
                    'name' => 'findBy',
                    'params' => array(
                        'criteria' => array(),
                        'orderBy' => array('local' => 'ASC'),
                    ),
                ),
            ),
            'attributes' => array(
                'class' => 'form-control',
            )
        ));
    }

    public function setUnitsUser($userId)
    {
        $em = $this->getObjectManager();
        $ids = $em->getRepository('Users\Entity\UserUnit')->getUnitsUser($userId);
        $this->get('units_access')->setOption('checked_options', $ids);
    }

    public function setCheckboxUnits()
    {
        $this->add(array(
            'type' => 'DoctrineModule\Form\Element\ObjectMultiCheckbox',
            'name' => 'units_access',
            'options' => array(
                'label' => 'Unidades',
                'disable_inarray_validator' => true, // desabilitar validação do elemento no select,
                'object_manager' => $this->getObjectManager(),
                'target_class' => 'Units\Entity\Unit',
                'property' => 'local',
                'is_method' => true,
                'find_method' => array(
                    'name' => 'findBy',
                    'params' => array(
                        'criteria' => array(),
                        'orderBy' => array('local' => 'ASC'),
                    ),
                ),
            ),
            'attributes' => array(
                'class' => 'form-control',
            )
        ));
    }

    /**
     * 
     * @return Doctrine\ORM\EntityManager     
     */
    public function getObjectManager()
    {
        if (null === $this->em) {
            $elementManager = $this->getFormFactory()->getFormElementManager();
            $sm = $elementManager->getServiceLocator();

            $this->em = $sm->get('Doctrine\ORM\EntityManager');
        }

        return $this->em;
    }

    public function getServiceLocator()
    {
        $elementManager = $this->getFormFactory()->getFormElementManager();
        return $elementManager->getServiceLocator();
    }

    public function isValid()
    {
        $userService = $this->getServiceLocator()->get('UsersManager\Service\User');
        $isValid = parent::isValid();
        
        
        /**
         * Confirmação de e-mail
         */
        $id = $this->get('id')->getValue();
        $email = $this->get('email')->getValue();
        if (!$userService->checkEmailUser($email, $id)) {
            $messagesCadastro = array(
                'emailCadastrado' => 'E-mail já cadastrado com outro usuário',
            );

            $this->get('email')->setMessages($messagesCadastro);
            $isValid = false;
        }

        $newPassword = $this->get('new_password')->getValue();
        $confirmNewPassword = $this->get('confirm_new_password')->getValue();
        if ($newPassword != null) {
            if ($newPassword !== $confirmNewPassword) {
                $confirmacaoSenha = array(
                    'confirmacaoSenha' => 'A confirmação da senha não confere com a senha informada!',
                );
                $this->get('confirm_new_password')->setMessages($confirmacaoSenha);
                $isValid = false;
            }
        }

        $messages = $this->getMessages();
        if (count($messages) === 1 && isset($messages['units_access'])) { // exceção para os casos das unidades (checkbox)
            $isValid = true;
        } else if(count($messages) > 0) {
            if(isset($messages['unit']) && isset($messages['units_access']) && $this->get('account')->getValue() == 1) {
                $isValid = true;
            }
        }

        
        
        return $isValid;
    }

}
