<?php

namespace UsersManager\Form\User;

use Zend\Form\Form;

class NewUser extends Form
{
    
    protected $em = null;
    protected $serviceLocator;
    
    public function __construct($name = null, $roles = array())
    {
        parent::__construct('user_new');
        $this->setAttribute('method', 'post');
        $this->setInputFilter(new NewUserFilter());
    }
    
    public function init()
    {      
        $this->setSelectUnit();
        
        $this->setCheckboxUnits();
        
        $this->setSelectRole();
        
        $this->add(array(
            'name' => 'first_name',
            'options' => array(
                'type' => 'text',
                'label' => 'Primeiro Nome'
            ),
            'attributes' => array(
                'placeholder' => '',
                'class' => 'form-control',
            )
        ));
        
        $this->add(array(
            'name' => 'last_name',
            'options' => array(
                'type' => 'text',
                'label' => 'Sobrenome'
            ),
            'attributes' => array(
                'placeholder' => '',
                'class' => 'form-control',
            )
        ));
        
        $this->add(array(
            'name' => 'username',
            'options' => array(
                'type' => 'text',
                'label' => 'Username'
            ),
            'attributes' => array(
                'placeholder' => '',
                'class' => 'form-control',
            )
        ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Email',
            'name' => 'email',
            'options' => array(
                'type' => 'text',
                'label' => 'E-mail'
            ),
            'attributes' => array(
                'placeholder' => '',
                'class' => 'form-control',
            )
        ));
        
        $this->add(array(
            'name' => 'phone',
            'type' => 'Zend\Form\Element\Text',
            'options' => array(
                'label' => 'Telefone'
            ),
            'attributes' => array(
                'id' => 'phone',
                'placeholder' => '',
                'class' => 'telefone form-control',
            )
        ));
        
        $this->add(array(
            'name' => 'commission',
            'type' => 'Zend\Form\Element\Text',
            'options' => array(
                'label' => 'Comissão'
            ),
            'attributes' => array(
                'id' => 'commission',
                'placeholder' => '',
                'class' => 'form-control',
            )
        ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Password',
            'name' => 'password',
            'options' => array(
                'type' => 'text',
                'label' => 'Senha Atual'
            ),
            'attributes' => array(
                'placeholder' => '',
                'class' => 'form-control',
            )
        ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Password',
            'name' => 'password',
            'options' => array(
                'type' => 'text',
                'label' => 'Senha'
            ),
            'attributes' => array(
                'placeholder' => '',
                'class' => 'form-control',
            )
        ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Password',
            'name' => 'confirm_password',
            'options' => array(
                'type' => 'text',
                'label' => 'Confirmar Senha'
            ),
            'attributes' => array(
                'placeholder' => '',
                'class' => 'form-control',
            )
        ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\File',
            'name' => 'photo',
            'options' => array(
                'type' => 'text',
                'label' => 'Foto'
            ),
            'attributes' => array(
                'placeholder' => '',
                'class' => 'form-control file2 inline btn btn-primary',
                'data-label' => "<i class='glyphicon glyphicon-file'></i> Procurar",
            )
        ));  
    }
    
    
    /**
     * @link https://github.com/doctrine/DoctrineModule/blob/master/docs/form-element.md
     */
    public function setSelectRole()
    {
        $this->add(array(
            'type' => 'DoctrineModule\Form\Element\ObjectSelect',
            'name' => 'role_id',
            'options' => array(
                'label' => 'Grupo',
                'display_empty_item' => true,
                'empty_item_label'   => '---',
                'disable_inarray_validator' => true, // desabilitar validação do elemento no select, 
                'object_manager' => $this->getObjectManager(),
                'target_class'   => 'Users\Entity\UserRole',
                'property'       => 'name',
                'is_method'      => true,
                'find_method' => array(
                    'name' => 'findBy',
                    'params' => array(
                        'criteria' => array(),
                        'orderBy' => array('name' => 'ASC'),
                    ),
                ),       
            ),
            'attributes' => array(
                'class' => 'form-control',
            )
        ));
    }
    
    /**
     * @link https://github.com/doctrine/DoctrineModule/blob/master/docs/form-element.md
     */
    public function setSelectUnit()
    {
        $this->add(array(
            'type' => 'DoctrineModule\Form\Element\ObjectSelect',
            'name' => 'unit',
            'options' => array(
                'label' => 'Unidade Principal',
                'display_empty_item' => true,
                'empty_item_label'   => '---',
                'disable_inarray_validator' => true, // desabilitar validação do elemento no select, 
                'object_manager' => $this->getObjectManager(),
                'target_class'   => 'Units\Entity\Unit',
                'property'       => 'local',
                'is_method'      => true,
                'find_method' => array(
                    'name' => 'findBy',
                    'params' => array(
                        'criteria' => array(),
                        'orderBy' => array('local' => 'ASC'),
                    ),
                ),       
            ),
            'attributes' => array(
                'class' => 'form-control',
            )
        ));
    }
    
    public function setCheckboxUnits()
    {
        $this->add(array(
            'type' => 'DoctrineModule\Form\Element\ObjectMultiCheckbox',
            'name' => 'units_access',
            'options' => array(
                'label' => 'Unidades',
                'disable_inarray_validator' => true, // desabilitar validação do elemento no select, 
                'object_manager' => $this->getObjectManager(),
                'target_class'   => 'Units\Entity\Unit',
                'property'       => 'local',
                'is_method'      => true,
                'find_method' => array(
                    'name' => 'findBy',
                    'params' => array(
                        'criteria' => array(),
                        'orderBy' => array('local' => 'ASC'),
                    ),
                ),       
            ),
            'attributes' => array(
                'class' => 'form-control',
            )
        ));
    }
    
    public function getObjectManager()
    {
        if (null === $this->em) {
            $elementManager = $this->getFormFactory()->getFormElementManager();
            $sm = $elementManager->getServiceLocator();

            $this->em = $sm->get('Doctrine\ORM\EntityManager');
        }

        return $this->em;
    }
    
    public function getServiceLocator()
    {
        $elementManager = $this->getFormFactory()->getFormElementManager();
        return $elementManager->getServiceLocator();
    }  
    
    public function isValid()
    {
        $userService = $this->getServiceLocator()->get('UsersManager\Service\User');
        $isValid = parent::isValid();
        
        $email = $this->get('email')->getValue();
        if($userService->checkEmailUser($email)) {
            $messagesCadastro = array(
                'emailCadastrado' => 'E-mail já cadastrado com outro usuário',
            );
            
            $this->get('email')->setMessages($messagesCadastro);
            $isValid = false;
        }
        
        $password     = $this->get('password')->getValue();
        $confirmPassword     = $this->get('confirm_password')->getValue();
        if($password !== $confirmPassword) { 
            $confirmacaoSenha = array(
                'confirmacaoSenha' => 'A confirmação da senha não confere com a senha informada!',
            );
            $this->get('confirm_password')->setMessages($confirmacaoSenha);
            $isValid = false;
        }
        
        $messages = $this->getMessages();
        
        if(count($messages) === 1 && isset($messages['units'])) { // exceção para os casos das unidades (checkbox)
            $isValid = true;
        }
        
        return $isValid;
    }
    
    
}
