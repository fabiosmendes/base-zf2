<?php

namespace UsersManager\Form;

use Zend\Form\Form;

class Privilege extends Form
{
    protected $resources;

    public function __construct($name = null, array $resources = null)
    {
        parent::__construct($name);
        $this->resources = $resources;

        $this->setAttribute('method', 'post');

        $id = new \Zend\Form\Element\Hidden('id');
        $this->add($id);
        
        $this->add(array(
            'name' => 'alias',
            'type' => 'Zend\Form\Element\Text',
            'options' => array(
                'label' => 'Apelido'
            ),
            'attributes' => array(
                'id' => 'alias',
                'placeholder' => '',
                'class' => 'medium',
            )
        ));
        
        $this->add(array(
            'name' => 'name',
            'type' => 'Zend\Form\Element\Text',
            'options' => array(
                'label' => 'Nome'
            ),
            'attributes' => array(
                'id' => 'name',
                'placeholder' => '',
                'class'    => 'medium',
                'disabled' => 'true'
            )
        ));
        
        $this->add(array(
            'name' => 'resource',
            'type' => 'Zend\Form\Element\Text',
            'options' => array(
                'label' => 'Recurso'
            ),
            'attributes' => array(
                'id' => 'resource',
                'placeholder' => '',
                'class' => '',
                'disabled' => 'true'
            )
        ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'display',
            'options' => array(
                'label' => 'Exibir na lista de Privilégios',
                'use_hidden_element' => true,
                'checked_value'   => '1',
            )
        ));
        
        $this->add(array(
            'name' => 'submit',
            'type' => 'Zend\Form\Element\Submit',
            'attributes' => array(
                'value' => 'Salvar',
                'class' => 'btn btn-success'
            )
        ));
    }

}
