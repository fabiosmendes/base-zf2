<?php

namespace UsersManager\Form;

use Zend\Form\Form;

class Role extends Form
{
    protected $parent;
    
    public function __construct($name = null, array $parent = null)
    {
        parent::__construct('roles');
        //$this->setInputFilter(new RoleFilter());
        $this->parent = $parent;

        $this->setAttribute('method', 'post');

        $id = new \Zend\Form\Element\Hidden('id');
        $this->add($id);

        $nome = new \Zend\Form\Element\Text("name");
        $nome->setLabel("Nome: ")
             ->setAttribute('placeholder', "Entre com o nome")
             ->setAttribute('class', 'form-control');
        
        $this->add($nome);
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'parent',
            'options' => array(
                'empty_option' => ' Nenhum ',
                'label' => 'Herda: ',
                'value_options' => $this->parent,
            ),
            'attributes' => array(
                'class' => 'form-control'
            )
        ));
        
        $isAdmin = new \Zend\Form\Element\Checkbox("is_admin");
        $isAdmin->setLabel("Admin?: ");
        $isAdmin->setUncheckedValue(0);
        $isAdmin->setUseHiddenElement(true);
        //$isAdmin->setAttribute('class', 'form-control');
        $this->add($isAdmin);

        $this->add(array(
            'name' => 'submit',
            'type' => 'Zend\Form\Element\Submit',
            'attributes' => array(
                'value' => 'Salvar',
                'class' => 'btn btn-success'
            )
        ));
    }
    
    public function isValid()
    {
        $valid = parent::isValid();
        return $valid;
    }

}
