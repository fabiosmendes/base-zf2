<?php

namespace UsersManager\Controller;

use Zend\Session\Container;

use Zend\Paginator\Paginator,
    Zend\Paginator\Adapter\ArrayAdapter;

use Manager\Controller\ManagerController;

class UsersController extends ManagerController
{
    const SERVICE_SESSION        = 'SessionManager';
    
    public function __construct()
    {
        $this->service = 'UsersManager\Service\User';
        $this->entity  = 'Users\Entity\User';
        $this->form    = 'UsersManager\Form\User\NewUser';
        $this->route      = 'manager';
        $this->controller = 'users';
    }
    
    public function indexAction() 
    {   
        $service  = $this->getServiceLocator()->get($this->service);
        $searchPars = $this->params()->fromQuery();
        $list     = $service->getList($searchPars);
        
        $page = $this->params()->fromRoute('pager');    
        $paginator = new Paginator(new ArrayAdapter($list));
        $paginator->setCurrentPageNumber($page);
        $paginator->setDefaultItemCountPerPage(10);
        
        $flashMessenger = $this->getServiceLocator()->get('ServiceFlashMessenger');
        $message = '';
        if ($flashMessenger->hasMessages()) {
            $messages = $flashMessenger->getMessages(); // segmentar mensagens (erro, por exemplo e success)
            $message = $messages[0];   
        }
        
        return $this->renderView(array('data' => $paginator, 
                                       'page' => $page, 
                                       'message'   => $message, 
                                       'queryPars' => json_encode($searchPars)));
    }
    
    public function editAccountAction() 
    {
        $userData = $this->getIdentity("Manager");
        $form     = $this->getForm('UsersManager\Form\User\EditUser');
        $service  = $this->getServiceLocator()->get($this->service);
        $flashMessenger = $this->getServiceLocator()->get('ServiceFlashMessenger');
        $request = $this->getRequest();
        
        if($request->isPost()) {
            $postData = $request->getPost()->toArray();
            
            $form->setData($postData);
            $form->remove('role_id'); // remove campo do formulário
            
            if($form->isValid()) {
                $postData['photo']  = $this->params()->fromFiles('photo'); // pegar arquivo no request
                $service->update($postData);
                
                $session = $this->getService(self::SERVICE_SESSION);
                $session->offsetSet('email', $postData['email']);
                
                $flashMessenger->addMessage('Dados do perfil alterados com sucesso!');
                return $this->redirect()->toRoute($this->route, array('controller' => 'users-login', 
                                                                      'action' => 'auth-reload'));
            }
        }
        else {
            if(null == $userData) {
                return $this->redirect()->toRoute($this->route, array('controller' => $this->controller));
            }
            else {
                $user = $service->getUser($userData->getId());
                if(!$user) {
                    return $this->redirect()->toRoute($this->route, array('controller' => 'users-login', 
                                                                          'action' => 'logout'));
                } 
                $form->setData($user->toArray());
            }
        }
        
        return $this->renderView(array('form' => $form));
    }
    
    public function editAction() 
    {
        $form     = $this->getForm('UsersManager\Form\User\EditUser');
        $service  = $this->getServiceLocator()->get($this->service);
        $flashMessenger = $this->getServiceLocator()->get('ServiceFlashMessenger');
        $request = $this->getRequest();
        
        if($request->isPost()) {
            $postData = $request->getPost()->toArray();
            $form->setData($postData);
            if($form->isValid()) {
                $postData['photo']  = $this->params()->fromFiles('photo'); // pegar arquivo no request
                $service->update($postData);
                $flashMessenger->addMessage('Dados do usuário alterados com sucesso!');
                return $this->redirect()->toRoute($this->route, array('controller' => $this->controller));
            }
        }
        else {
            $repository  = $this->getEm()->getRepository($this->entity);
            $id          = $this->params()->fromRoute('id', 0);
            $entity      = $repository->find($id);
                
            if(null == $entity) {
                return $this->redirect()->toRoute($this->route, array('controller' => $this->controller));
            }
            else {
                $dados = $entity->toArray();
                $form->setData($dados);
                $form->setUnitsUser($form->get('id')->getValue());
            }  
        }
        
        return $this->renderView(array('form' => $form));
    }
    
    public function newAction() 
    {
        $form     = $this->getForm('UsersManager\Form\User\NewUser');
        $flashMessenger = $this->getServiceLocator()->get('ServiceFlashMessenger');
        $request = $this->getRequest();
        
        if($request->isPost()) {
            $postData = $request->getPost()->toArray();
            $form->setData($postData);
            if($form->isValid()) {
                $postData['photo']  = $this->params()->fromFiles('photo'); // pegar arquivo no request
                $service = $this->getServiceLocator()->get($this->service);
                $service->insert($postData);
                
                $flashMessenger->addMessage('Novo usuário cadastrado com sucesso!');
                return $this->redirect()->toRoute($this->route, array('controller' => $this->controller));
            }
        }
        
        return $this->renderView(array('form' => $form));
    }
    
    public function deleteAction() 
    {
        $access = $this->checkAuthorization(__METHOD__);
        if($access) {
            return $access;
        }
        
        $service = $this->getServiceLocator()->get($this->service);
        $flashMessenger = $this->getServiceLocator()->get('ServiceFlashMessenger');
        $userData = $this->getIdentity("Manager");
        $service->setUserIdAccount($userData->getId());
        $id      = $this->params()->fromRoute('id', 0);
        $entity  = $service->getUser($id);
        
        if($service->delete($entity, $userData->getId())) {
            
            $usuarioDados = $entity->getFirstName().' '.$entity->getLastName();
            $flashMessenger->addMessage('Usuário "'.$usuarioDados.'" excluído!');
        } else {
            $messages = $service->getMessages();
            if(isset($messages[0])) {
                $flashMessenger->addMessage($messages[0]);
            }    
        }
        
        return $this->redirect()->toRoute($this->route, array('controller' => $this->controller));
            
    }
    
    public function blockAccessAction()
    {
        $info = new Container('info_block');
        $pars = array('info' => $info);
        return $this->renderView($pars);
    }
    
    public function passAction()
    {
        $service = $this->getServiceLocator()->get($this->service);
        $data = $service->getCriptSenha('chave1209');
        echo __LINE__ . ' - ' . __METHOD__;
        var_dump($data);
        exit();
    }
}
