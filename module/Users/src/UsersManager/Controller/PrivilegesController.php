<?php

namespace UsersManager\Controller;

use Zend\Paginator\Paginator,
    Zend\Paginator\Adapter\ArrayAdapter;

use Zend\View\Model\ViewModel;
use Manager\Controller\ManagerController;

class PrivilegesController extends ManagerController
{
    public function __construct()
    {
        $this->entity  = "Users\Entity\UserPrivilege";
        $this->service = "UsersManager\Service\Privilege";
        $this->form = "UsersManager\Form\Privilege";
        $this->controller = "privileges";
        $this->route      = "manager";
        
        parent::__construct();
    }

    public function indexAction() 
    {   
        $service  = $this->getServiceLocator()->get($this->service);
        $searchPars = $this->params()->fromQuery();
        $list     = $service->getList($searchPars);
        
        $page = $this->params()->fromQuery('page');
        
        $paginator = new Paginator(new ArrayAdapter($list));
        $paginator->setCurrentPageNumber($page);
        $paginator->setDefaultItemCountPerPage(50);
        
        $flashMessenger = $this->getServiceLocator()->get('ServiceFlashMessenger');
        $message = '';
        if ($flashMessenger->hasMessages()) {
            $messages = $flashMessenger->getMessages(); // segmentar mensagens (erro, por exemplo e success)
            $message = $messages[0];   
        }
        
        return $this->renderView(array('data' => $paginator, 
                                       'page' => $page, 
                                       'message'   => $message, 
                                       'queryPars' => json_encode($searchPars)));
    }
    
    public function newAction()
    {
        $form = $this->getServiceLocator()->get('UsersManager\Form\Privilege');
        $request = $this->getRequest();

        if($request->isPost()) {
            $form->setData($request->getPost());
            if($form->isValid()) {
                $service = $this->getServiceLocator()->get($this->service);
                $service->insert($request->getPost()->toArray());

                return $this->redirect()->toRoute($this->route, array('controller' => $this->controller));
            }
        }

        return new ViewModel(array('form' => $form));
    }

    public function editAction()
    {
        $access = $this->checkAuthorization(__METHOD__);
        if($access) {
            return $access;
        }
        
        $form = $this->getServiceLocator()->get('UsersManager\Form\Privilege');
        $request = $this->getRequest();

        $repository = $this->getEm()->getRepository($this->entity);
        $entity = $repository->find($this->params()->fromRoute('id', 0));

        if($this->params()->fromRoute('id', 0)) {
            $form->setData($entity->toArray());
        }
        
        if($request->isPost()) {
            $form->setData($request->getPost());
            if($form->isValid()) {
                $service = $this->getServiceLocator()->get($this->service);
                $entity  = $service->update($request->getPost()->toArray());
                $data = $entity->toArray();
                return $this->redirect()->toUrl('/users-manager-privileges?resource=' . $data['resource_id']);
            }
        }

        return new ViewModel(array('form' => $form));
    }

}
