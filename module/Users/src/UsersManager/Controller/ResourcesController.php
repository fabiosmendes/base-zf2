<?php

namespace UsersManager\Controller;

use Zend\Paginator\Paginator,
    Zend\Paginator\Adapter\ArrayAdapter;
use Manager\Controller\ManagerController;

class ResourcesController extends ManagerController
{
    public function __construct()
    {
        $this->entity  = "Users\Entity\UserResource";
        $this->service = "UsersManager\Service\Resource";
        $this->form    = "UsersManager\Form\Resource";
        $this->controller = "resources";
        $this->route      = "manager";
        
        parent::__construct();
    }
    
    public function indexAction() 
    {    
        $list = $this->getEm()
                     ->getRepository($this->entity)
                     ->findBy(array(), array('name' => 'asc'));
        $page = $this->params()->fromRoute('page');
        
        $paginator = new Paginator(new ArrayAdapter($list));
        $paginator->setCurrentPageNumber($page);
        $paginator->setDefaultItemCountPerPage(50);
        
        return $this->renderView(array('data' => $paginator, 'page' => $page));
    }
    
    public function listResourcesAction()
    {
        $service = $this->getService($this->service);
        $service->getList();
    }
    
    public function updateListResourcesControllerAction()
    {
        $service = $this->getService($this->service);
        $service->setControllerResources();
        
        return $this->redirect()->toRoute($this->route, array('controller' => 'resources'));
    }
}
