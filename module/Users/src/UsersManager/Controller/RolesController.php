<?php

namespace UsersManager\Controller;

use Zend\View\Model\ViewModel;
use Manager\Controller\ManagerController;

class RolesController extends ManagerController
{
    public function __construct()
    {
        $this->entity   = "Users\Entity\UserRole";
        $this->service  = "UsersManager\Service\Role";
        $this->form     = "UsersManager\Form\Role";
        $this->controller = "roles";
        $this->route      = "manager";
        
        parent::__construct();
    }

    public function newAction()
    {
        $form = $this->getServiceLocator()->get('UsersManager\Form\Role');
        $request = $this->getRequest();
        
        $serviceResource = $this->getServiceLocator()->get('UsersManager\Service\Resource');
        $listResources   = $serviceResource->getList();
        
        if($request->isPost()) {
            $service = $this->getServiceLocator()->get($this->service);
            $service->insert($request->getPost()->toArray());

            return $this->redirect()->toRoute($this->route, array('controller' => $this->controller));
            
        }

        return new ViewModel(array('form' => $form, 'resources' => $listResources));
    }

    public function editAction()
    {
        $form = $this->getServiceLocator()->get('UsersManager\Form\Role');
        $service = $this->getServiceLocator()->get($this->service);
        $request = $this->getRequest();
        
        $repository = $this->getEm()->getRepository($this->entity);
        $entity = $repository->find($this->params()->fromRoute('id', 0));
        
        $listRolesPrivileges = array();
        if($entity) {
            $serviceResource = $this->getServiceLocator()->get('UsersManager\Service\Resource');
            $listResources   = $serviceResource->getList();
            $listRolesPrivileges = $service->getRolePrivileges($entity);
            $form->setData($entity->toArray());
        }
        
        if($request->isPost()) {
            $service->update($request->getPost()->toArray());
            return $this->redirect()->toRoute($this->route, array('controller' => $this->controller)); 
        }
        
        return new ViewModel(array('form' => $form, 
                                   'resources' => $listResources, 
                                   'roles_resources' => $listRolesPrivileges));
    }
    
    public function deleteAction() 
    {
        $access = $this->checkAuthorization(__METHOD__);
        if($access) {
            return $access;
        }
        
        $service = $this->getServiceLocator()->get($this->service);
        $flashMessenger = $this->getServiceLocator()->get('ServiceFlashMessenger');
        $userData = $this->getIdentity("Manager");
        
        $id      = $this->params()->fromRoute('id', 0);
        $entity  = $service->getRole($id);
        
        if($service->deleteRole($entity, $userData->getRoleId())) {
            $flashMessenger->addMessage('Grupo "'.$entity->getName().'" excluído!');
        } else {
            $messages = $service->getMessages();
            if(isset($messages[0])) {
                $flashMessenger->addMessage($messages[0]);
            }    
        }
        
        return $this->redirect()->toRoute($this->route, array('controller' => $this->controller));    
    }
    
}
