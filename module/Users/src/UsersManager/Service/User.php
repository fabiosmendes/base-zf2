<?php

namespace UsersManager\Service;

use Doctrine\ORM\EntityManager;
use Zend\Stdlib\Hydrator;
use Base\Service\AbstractService;

use Users\Entity\UserUnit;

class User extends AbstractService
{
    /**
     * ID do usuário logado no sistema
     * @var int
     */
    protected $userIdAccount = 0;
    
    public function __construct(EntityManager $em, $fileTransfer = null)
    {
        parent::__construct($em);
        $this->entity = "Users\Entity\User";
        $this->fileTransfer = $fileTransfer;
    }
    
    public function getList($pars = array(), $limit = 10)
    {
        $dql  = " SELECT u FROM ".$this->entity." u ";
        $dql .= " WHERE u.id > 0";
            
        if(isset($pars['first_name']) && $pars['first_name'] != '') {
            $dql .= " AND u.firstName LIKE '%".$pars['first_name']."%'";  
        }

        if(isset($pars['last_name']) && $pars['last_name'] != '') {
            $dql .= " AND u.lastName LIKE '%".$pars['last_name']."%'"; 
        }
        
        if(isset($pars['email']) && $pars['email'] != '') {
            $dql .= " AND u.email LIKE '%".$pars['email']."%'"; 
        }
        
        $dql .= ' ORDER BY u.id DESC';
        $query = $this->em->createQuery($dql);
        
        $lista = $query->getResult();
        return $lista;
    }
    
    public function checkPassword($email, $password)
    {
        $entity = $this->getRepository()->findByEmailAndPassword($email, $password);
        return $entity;
    }
    
    public function checkEmailUser($email, $id = 0)
    {
        $classTarget = '\Users\Entity\User';
        if($id > 0) {
            $entity = $this->getRepository()->findOneBy(array('id' => $id,'email' => $email));
            return ($entity instanceof $classTarget);
        } 
        
        $entity = $this->getRepository()->findOneBy(array('email' => $email));
        return ($entity instanceof $classTarget);
    }
    
    public function insert(array $data)
    {
        $photo = $data['photo'];
        unset($data['photo']);
        $partsEmail = explode('@', $data['email']);
        if(is_array($partsEmail) && isset($partsEmail)) {
            $data['username'] = $partsEmail[0];
        }
        
        $data['role'] = $this->getRoleReference($data['role_id']);
        
        $entity = parent::insert($data);
        $photoUpload = $this->uploadFile('./public/media/users/', $photo, 'photo_'.$entity->getId().'-'.rand(99,999));
        if($photoUpload != '') {
            $entity->setPhoto($photoUpload);
        }
        
        $this->execute($entity);
        if(!isset($data['units_access'])) {
            $data['units_access'] = array();
        }
        $this->registerUnits($entity->getId(), $data['units_access']);
        
        return $entity;
    }
    
    public function update(array $data)
    {
        $entity = $this->em->getReference($this->entity, $data['id']);
        $photo = $data['photo'];
        unset($data['photo']);
        
        $photoUpload = $this->uploadFile('./public/media/users/', $photo, 'photo_'.$data['id'].'-'.rand(99,999));
        if($photoUpload != '') {
            $data['photo'] = $photoUpload;
        }
        
        if(!empty($data['new_password'])) {
            $data['password'] = $data['new_password'];
        }
        
        if(isset($data['role_id'])) {
            $roleRef = $this->getRoleReference($data['role_id']);
        }
        
        
        $hydrator = new Hydrator\ClassMethods();
        $hydrator->hydrate($data, $entity);
        
        if(isset($data['role_id'])) {
            $entity->setRole($roleRef);
        }
        
        $this->execute($entity);
        
        if(!isset($data['units_access'])) {
            $data['units_access'] = array();
        }
        $this->registerUnits($entity->getId(), $data['units_access']);
        
        return $entity;
    }
    
    public function registerUnits($userId, $units)
    {
        $this->removeUnits($userId);
        foreach($units as $unit) {
            $data = array(
                'unit' => $unit,
                'user' => $userId,
            );
            $userUnit = new UserUnit($data);
            $this->execute($userUnit);
        }
    }
    
    public function removeUnits($userId)
    {
        $sqlDeleteUnits  = " DELETE FROM users_units ";
        $sqlDeleteUnits .= " WHERE user_id = ".$userId;

        $this->executeFreeSql($sqlDeleteUnits);
    }
    
    public function getRoleReference($roleId)
    {
         return $this->em->getReference('Users\Entity\UserRole', $roleId);
    }
    
    public function getCountUsers()
    {
        $query = $this->em->createQuery('SELECT COUNT(u.id) FROM Users\Entity\User u');
        return $query->getSingleScalarResult();
    }
    
    /**
     * Permite a exclusão se houver mais de um usuário no sistema.
     * 
     * @param type $entity
     * @return boolean
     */
    public function delete($entity)
    {
        $numUsers = $this->getCountUsers();
        
        if($entity) {
            if($numUsers == 1) {
                $this->addMessage('É necessário pelo menos um usuário no sistema!');
                return false;
            } else if($this->userIdAccount == $entity->getId()) {
                $this->addMessage('Não é possível excluir seu próprio usuário!');
                return false;
            }
            
            $pathFile = './public/media/users/'.$entity->getPhoto();
            $this->removeFile($pathFile);
            
            $this->em->remove($entity);
            $this->em->flush();
        } 
        
        return $entity;
    }
    
    public function getCriptSenha($senhaDesc)
    {
        $entity = new $this->entity(array());
        $pars = array(
            'senha' => $entity->setPassword($senhaDesc)->getPassword(),
            'salt'  => $entity->getSalt()
        );

        return $pars;
    }
    
    public function getUser($id)
    {
        $entity = $this->getRepository()->find($id);
        return $entity;
    }   
    
    public function setUserIdAccount($userIdAccount)
    {
        $this->userIdAccount = $userIdAccount;
    }
}
