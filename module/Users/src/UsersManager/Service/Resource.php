<?php

namespace UsersManager\Service;

use Base\Service\AbstractService;
use Doctrine\ORM\EntityManager;

class Resource extends AbstractService
{
    protected $controllers;
    
    public function __construct(EntityManager $em, $controllers)
    {
        parent::__construct($em);
        $this->entity      = "Users\Entity\UserResource";
        $this->controllers = $controllers;
    }
    
    public function getExcludeMethodsForClass()
    {
        return array(
            'Zend\Mvc\Controller\AbstractActionController',
            'Zend\Mvc\Controller\AbstractController',    
        );
    }
    
    public function getMethodsController($class)
    {
        $methods = $class->getMethods(\ReflectionMethod::IS_PUBLIC);
        $filteringMethods = array();
        $excludeMethods = $this->getExcludeMethodsForClass();
        
        foreach($methods as $method) {
            $name  = $method->getName();
            if(strstr($name, "Action") != '' && !in_array($method->class, $excludeMethods) ) {
                $filteringMethods[] = $method->name;
            }    
        }
        
        return $filteringMethods;
    }
    
    public function persistPrivilege($method, $resourceId)
    {
        $privilege = new \Users\Entity\UserPrivilege();
        $resource  = $this->em->getReference('\Users\Entity\UserResource', $resourceId);

        $alias = ucfirst(str_replace("Action", "", $method)); // @todo - tornar método mais legível
        $privilege->setName($method)
                  ->setAlias($alias)  
                  ->setResource($resource);

        $this->em->persist($privilege);
    }
    
    public function registerPrivileges($controller, $resourceId) 
    {
        $class   = new \ReflectionClass($controller);
        $methods = $this->getMethodsController($class);
        
        foreach($methods as $method) {
            $entity = '\Users\Entity\UserPrivilege';
            $privilegeEntity = $this->em->getRepository($entity)
                                        ->findOneBy(array('resource' => $resourceId, 
                                                          'name' => $method));
            
            if(!$privilegeEntity) {
                $this->persistPrivilege($method, $resourceId);
            }
        }
        
        $this->em->flush();
    }
    
    public function persistResource($controller)
    {
        $partsController = explode("\\", $controller);
        $controllerAlias = str_replace("Controller", "", end($partsController));
        
        $resource = new \Users\Entity\UserResource();
        $resource->setName($controller)
                 ->setAlias($controllerAlias);
        
        $this->em->persist($resource);
        $this->em->flush();

        return $resource->getId();
    }
    
    public function registerResource($controller)
    {
        $entity = '\Users\Entity\UserResource';
        $resoureceEntity = $this->em->getRepository($entity)->findOneBy(array('name' => $controller));
        
        if(!$resoureceEntity) {
            $resourceId = $this->persistResource($controller);
        } else {
            $resourceId = $resoureceEntity->getId();
        }
        
        $this->registerPrivileges($controller, $resourceId);
        return $resourceId;
    }
    
    public function getPathModules()
    {
        $pathInfo     =  str_replace('\\', '/', pathinfo(__DIR__));
        $partsDirName = explode('module', $pathInfo['dirname']);
        return $partsDirName[0].'module/';
    }
    
    public function checkFileController($controller) 
    {
        $dirModule = getcwd().'/module';
        
        $partsNamespace = explode("/", $controller);
        if(!isset($partsNamespace[0])) {
            return false;
        }
        
        $rootNamespace  = str_replace('Manager', '', $partsNamespace[0]);
        $controllerPath =  $dirModule.'/'.$rootNamespace.'/src/'.$controller.'.php'; 
        
        return is_file($controllerPath);
    }
    
    public function checkClassExists($controllerName)
    {
        return class_exists('/'.$controllerName);
    }
    
    public function checkConditions($moduleNamespace, $controllerName)
    {
        $condNamespace         = ($moduleNamespace == 'Manager' || strstr($moduleNamespace, 'Manager') != '');
        if(!$condNamespace) {
            return false;
        }
        
        $condExcludeLoginClass   = !strstr($controllerName, 'Login');
        $condCheckControllerFile = $this->checkFileController($controllerName);
        $condCheckClassExists    = true; // $this->checkClassExists($controllerName); @todo - refinar verificação
        
        return ($condExcludeLoginClass && $condCheckControllerFile && $condCheckClassExists);
    }
    
    public function setControllerResources()
    {    
        foreach($this->controllers as $controllerNameOriginal)
        {
            $moduleNamespace = substr($controllerNameOriginal, 0, strpos($controllerNameOriginal, '\\'));
            $controllerName =  str_replace('\\', '/', $controllerNameOriginal);
            
            if($this->checkConditions($moduleNamespace, $controllerName)) {
                $this->registerResource($controllerNameOriginal);
            }
        }
        
        return $this->controllers;
    }
    
    public function getList($pars = array(), $limit = 10)
    {
        $list   = $this->getRepository()->findBy(array('display' => 1), array());
        return $list;
    }
}
