<?php

namespace UsersManager\Service;

use Base\Service\AbstractService;
use Doctrine\ORM\EntityManager;

class UserUnit extends AbstractService
{
    public function __construct(EntityManager $em)
    {
        parent::__construct($em);
        $this->entity = "Users\Entity\UserUnit";
    }
    
    public function getUnitsUser($userId)
    {
        return $this->getRepository()->getUnitsUser($userId);
    }
}
