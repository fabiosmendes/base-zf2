<?php

namespace UsersManager\Service;

use Base\Service\AbstractService;
use Doctrine\ORM\EntityManager;
use Zend\Stdlib\Hydrator;

class Privilege extends AbstractService
{
    public function __construct(EntityManager $em)
    {
        parent::__construct($em);
        $this->entity = "Users\Entity\UserPrivilege";
    }

    public function getList($pars = array(), $limit = 10)
    {
        $dql  = " SELECT p FROM ".$this->entity." p ";
        $dql .= " WHERE p.id > 0";
        
        if(isset($pars['resource']) && $pars['resource'] != '') {
            $dql .= " AND p.resource = '".$pars['resource']."'";  
        }
        
        if(isset($pars['name']) && $pars['name'] != '') {
            $dql .= " AND p.name LIKE '%".$pars['name']."%'";  
        }
        
        if(isset($pars['resource']) && $pars['resource'] != '') {
            $dql .= " AND p.resource = '".$pars['resource']."'";  
        }
        
        $dql .= ' ORDER BY p.id DESC';
        
        $query = $this->em->createQuery($dql);
        
        $lista = $query->getResult();
        return $lista;
    }
    
    public function insert(array $data)
    {
        $entity = new $this->entity($data);
        
        $resource = $this->em->getReference("Users\Entity\UserResource", $data['resource']);
        $entity->setResource($resource); // Injetando entidade carregada

        $this->em->persist($entity);
        $this->em->flush();
        return $entity;
    }

    public function update(array $data)
    {
        $entity = $this->em->getReference($this->entity, $data['id']);
        $hydrator = new Hydrator\ClassMethods();
        $hydrator->hydrate($data, $entity);
        
        $this->em->persist($entity);
        $this->em->flush();
        return $entity;
    }

}
