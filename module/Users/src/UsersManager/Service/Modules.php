<?php

namespace UsersManager\Service;

use Zend\ModuleManager\ModuleManager;

class Modules
{

    protected $moduleManager;
    protected $listControllers = array();
    
    public function __construct(ModuleManager $moduleManager)
    {
        $this->moduleManager = $moduleManager;
    }
    
    /**
     * @todo verificar limitação com factories.
     * 
     */
    public function analysisApplicationControllers()
    {
        $manager = $this->moduleManager;
        $modules = $manager->getLoadedModules();
        $excludeModules = $this->getExcludeModules();
        
        foreach($modules as $module) {
            $namespace = $this->getNamespace($module);
            
            if(!in_array($namespace, $excludeModules)) {
                $this->setControllersWithConfigControllers($module);
                $this->setControllersWithConfigMethod($module);
            }
        }
    }
    
    /**
     * 
     * @return array
     */
    public function getExcludeModules()
    {
        $excludeModules = array('ZendDeveloperTools', 'ZFTool', 'OcraServiceManager', 
                                'DoctrineModule', 'DoctrineORMModule', 'DoctrineDataFixtureModule');
        
        return $excludeModules;
    }
    
    public function getNamespace($module)
    {
        $class = get_class($module);
        $partsClass = explode('\\', $class);
        return $partsClass[0]; 
    }
    
    public function setControllersWithConfigControllers($module)
    {
        $configModule = $module->getConfig();
        if(isset($configModule['controllers'])) {
            $this->listControllers = array_merge_recursive($this->listControllers, $configModule['controllers']);
        } 
    }
    
    public function setControllersWithConfigMethod($module)
    {
        if(method_exists($module, 'getControllerConfig'))  {
            $controllersConfig = $module->getControllerConfig();
            $this->listControllers = array_merge_recursive($this->listControllers, $controllersConfig);
        } 
    }
    
    /**
     * 
     * @return array
     */
    public function getListControllersManager()
    {
        $controllers = $this->getListControllers()['invokables'];
        $filterKey   = 'Manager';
        $controllersManager = array(); 
        foreach($controllers as $controllerNamespace) {
            if(strstr($controllerNamespace, $filterKey) != '') {
                $controllersManager[] = $controllerNamespace;
            }
        }
        return $controllersManager;
    }
    
    /**
     * 
     * @return array
     */
    public function getListControllers()
    {
        return $this->listControllers;
    }

}
