<?php

namespace UsersManager\Service;

use Base\Service\AbstractService;
use Doctrine\ORM\EntityManager;
use Zend\Stdlib\Hydrator;

class Role extends AbstractService
{
    public function __construct(EntityManager $em)
    {
        parent::__construct($em);
        $this->entity = "Users\Entity\UserRole";
    }
    
    public function getRolePrivileges($role)
    {
        $listRolesPrivileges = $this->em->getRepository('Users\Entity\UserRolePrivilege')
                                        ->findBy(array('role' => $role->getId()));
        
        if($listRolesPrivileges) {
            $rolesPrivilegesIds = array();
            foreach($listRolesPrivileges as $rolePrivilege) {
                $rolesPrivilegesIds[] = $rolePrivilege->getPrivilege()->getId(); 
            }
            
            return $rolesPrivilegesIds;
        }
        
        return array();
    }
    
    public function deleteRolesPrivileges($role)
    {
        $listRolePrivilegeEntity = $this->em->getRepository('Users\Entity\UserRolePrivilege')
                                            ->findBy(array('role' => $role->getId()));
        
        if($listRolePrivilegeEntity) {
           foreach($listRolePrivilegeEntity as $rolePrivilegeEntity) {
               $this->em->remove($rolePrivilegeEntity); 
           }
           
           $this->em->flush();
        }    
    }
    
    public function registerRolesPrivileges($role, $privileges)
    {   
        $this->deleteRolesPrivileges($role);
        
        foreach($privileges as $privilegeId) {
            $privilege = $this->em->getReference('Users\Entity\UserPrivilege', $privilegeId);
            
            $userRolePrivilege = new \Users\Entity\UserRolePrivilege();
            $userRolePrivilege->setRole($role)
                              ->setPrivilege($privilege);
            
            $this->em->persist($userRolePrivilege);
        }
        
        $this->em->flush();
        
        return true;
    }
    
    public function insert(array $data)
    {
        $entity = new $this->entity($data);
        
        $entity->setParent(null);
        if(isset($data['parent']) && $data['parent']) {
            $parent = $this->em->getReference($this->entity, $data['parent']);
            $entity->setParent($parent);
        }
            
        $this->em->persist($entity);
        $this->em->flush();
        
        if(isset($data['privileges'])) {
            $this->registerRolesPrivileges($entity, $data['privileges']);
        }
        
        return $entity;
    }

    
    
    public function update(array $data)
    {
        $entity = $this->em->getReference($this->entity, $data['id']);
        $hydrator = new Hydrator\ClassMethods();
        $hydrator->hydrate($data, $entity);

        if(isset($data['parent']) && $data['parent']) {
            $parent = $this->em->getReference($this->entity, $data['parent']);
            $entity->setParent($parent);
        } else {
            $entity->setParent(null);
        }
        
        $this->em->persist($entity);
        $this->em->flush();
        
        if(isset($data['privileges'])) {
            $this->registerRolesPrivileges($entity, $data['privileges']);
        }
        
        return $entity;
    }
    
    public function getCountRoles()
    {
        $query = $this->em->createQuery('SELECT COUNT(ur.id) FROM Users\Entity\UserRole ur');
        return $query->getSingleScalarResult();
    }
    
    public function getCountUsersRole($roleId)
    {
        $sqlCountUsersRole  = " SELECT COUNT(u.id) AS num ";
        $sqlCountUsersRole .= " FROM   users u ";
        $sqlCountUsersRole .= " WHERE  u.role_id = ".$roleId;
        
        $rsCountUserRole = $this->executeSqlReturnStatement($sqlCountUsersRole)->fetch();
        return $rsCountUserRole['num'];
    }
    
    public function getRole($id)
    {
        $entity = $this->getRepository()->find($id);
        return $entity;
    } 
    
    public function deleteConditions($roleId, $roleIdUser)
    {
        if($this->getCountRoles() == 1) {
            $this->addMessage('É necessário pelo menos um grupo no sistema!');
            return false;
        } else if($this->getCountUsersRole($roleId) > 0) {
             $this->addMessage('Não é possível excluir um grupo que possua usuários vinculados a ele!');
            return false;
        } else if($roleId == $roleIdUser) {
            $this->addMessage('Não é possível excluir o grupo do seu usuário!');
            return false;
        }
        
        return true;
    }
    
    /**
     * Permite a exclusão se houver mais de um usuário no sistema.
     * 
     * @param type $entity
     * @return boolean
     */
    public function deleteRole($entity, $roleIdUser)
    {
        if($entity) {
            $roleId   = $entity->getId();
            if($this->deleteConditions($roleId, $roleIdUser)) {
                return parent::delete($roleId);
            }
        }
        return false;
    }

    
    public function getName($roleId)
    {
        $entity = $this->getRepository()->find($roleId);
        if($entity) {
            return $entity->getName();
        }
        
        return null;
    }
}
