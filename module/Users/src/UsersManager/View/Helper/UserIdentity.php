<?php

namespace UsersManager\View\Helper;

use Zend\View\Helper\AbstractHelper;
use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Storage\Session as SessionStorage;

use Doctrine\ORM\EntityManager;

class UserIdentity extends AbstractHelper
{
    /**
     *
     * @var Doctrine\ORM\EntityManager 
     */
    protected $em;
    
    protected $authService;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function getAuthService()
    {
        return $this->authService;
    }

    public function __invoke($namespace = null)
    {
        $sessionStorage = new SessionStorage($namespace);
        $this->authService = new AuthenticationService();
        $this->authService->setStorage($sessionStorage);

        if($this->getAuthService()->hasIdentity()) {
            $identityId = $this->getAuthService()->getIdentity();
            
            $entity = $this->em
                           ->getRepository('Users\Entity\User')
                           ->findOneBy(array('id' => $identityId));
            
            return $entity;
        }

        return false;
    }

}
