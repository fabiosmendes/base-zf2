<?php

namespace UsersManager\View\Helper;

use Zend\View\Helper\AbstractHelper;

class RoleName extends AbstractHelper
{
    protected $role;
    
    public function __construct($role)
    {
        $this->role = $role;
    }
    public function __invoke($roleId)
    {
        return $this->role->getName($roleId);
    }

}
