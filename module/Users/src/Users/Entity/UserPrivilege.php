<?php

namespace Users\Entity;

use Doctrine\ORM\Mapping as ORM;
use Zend\Stdlib\Hydrator;

/**
 * UsersPrivileges
 * 
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 * @ORM\Table(name="users_privileges")
 * @ORM\Entity(repositoryClass="Users\Entity\Repository\UserPrivilegeRepository")
 */
class UserPrivilege
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="UserResource", inversedBy="privileges")
     * @ORM\JoinColumn(name="resource_id", referencedColumnName="id")
     */
    protected $resource;

    /**
     * @ORM\OneToMany(targetEntity="UserRolePrivilege", mappedBy="privilege")
     */
    private $userRolePrivilege;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=45, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="alias", type="string", length=45, nullable=true)
     */
    private $alias;

    /**
     * @var integer
     *
     * @ORM\Column(name="display", type="integer", nullable=true)
     */
    private $display;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=true)
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime", nullable=true)
     */
    private $modified;

    public function __construct(array $options = array())
    {
        $hydrator = new Hydrator\ClassMethods();
        $hydrator->hydrate($options, $this);

        $this->created  = new \DateTime("now");
        $this->modified = new \DateTime("now");
    }
    
    public function getId()
    {
        return $this->id;
    }

    public function getResource()
    {
        return $this->resource;
    }

    public function getUserRolePrivilege()
    {
        return $this->userRolePrivilege;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getAlias()
    {
        return $this->alias;
    }

    public function getDisplay()
    {
        return $this->display;
    }

    public function getCreated()
    {
        return $this->created;
    }

    public function getModified()
    {
        return $this->modified;
    }

    public function setId($id)
    {
        if (!is_numeric($id)) {
            throw new \InvalidArgumentException('ID accept only integers.');
        }
        
        if ($id <= 0) {
            throw new \InvalidArgumentException('ID accept only number greater than zero.');
        }
        
        $this->id = $id;
        return $this;
    }

    public function setResource($resource)
    {
        $this->resource = $resource;
        return $this;
    }

    public function setUserRolePrivilege($userRolePrivilege)
    {
        $this->userRolePrivilege = $userRolePrivilege;
        return $this;
    }

    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    public function setAlias($alias)
    {
        $this->alias = $alias;
        return $this;
    }

    public function setDisplay($display)
    {
        $this->display = $display;
        return $this;
    }

    public function setCreated(\DateTime $created)
    {
        $this->created = $created;
        return $this;
    }

    public function setModified(\DateTime $modified)
    {
        $this->modified = $modified;
        return $this;
    }

    public function toArray() 
    {
        $hydrator = new Hydrator\ClassMethods();
        return $hydrator->extract($this);
    }  
}
