<?php

namespace Users\Entity;

use Doctrine\ORM\Mapping as ORM;
use Zend\Stdlib\Hydrator;

/**
 * UsersResources 
 * 
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 * @ORM\Table(name="users_resources")
 * @ORM\Entity(repositoryClass="Users\Entity\Repository\UserResourceRepository")
 */
class UserResource
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=80, nullable=true)
     */
    private $name;

    /**
     * Parameter to alias the original resource name.
     * ex: Manager\Controller\ResourceController -> Manager - Resource
     * By default, assumes the value of the 'name' attribute.
     *  
     * @var string
     *
     * @ORM\Column(name="alias", type="string", length=80, nullable=false)
     */
    private $alias;

    /**
     * Defines if the resource is displayed in the list of available resources
     * 
     * @var integer 
     * 
     * @ORM\Column(name="display", type="integer", nullable=true)
     */
    private $display;

    /**
     * @ORM\OneToMany(targetEntity="UserPrivilege", mappedBy="resource",cascade={"remove"})
     * */
    protected $privileges;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime", nullable=false)
     */
    private $modified;

    public function __construct(array $options = array())
    {
        $hydrator = new Hydrator\ClassMethods();
        $hydrator->hydrate($options, $this);
        
        $this->privileges = new \Doctrine\Common\Collections\ArrayCollection();
        $this->created  = new \DateTime("now");
        $this->modified = new \DateTime("now");
    }
    
    /**
     * 
     * @param \Users\Entity\UserPrivilege $privilege
     * @return $this
     */
    public function addPrivilege(UserPrivilege $privilege)
    {
        $privilege->setResource($this);
        $this->privileges[] = $privilege;
        
        return $this;
    }
    
    /**
     * 
     * @param \Users\Entity\UserPrivilege $privilege
     */
    public function removePrivilege($privilege)
    {
        $this->privileges->removeElement($privilege);
    }
   
    /**
     * 
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPrivileges()
    {
        return $this->privileges;
    }
    
    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getAlias()
    {
        return $this->alias;
    }

    public function getDisplay()
    {
        return $this->display;
    }
    
    public function getCreated()
    {
        return $this->created;
    }

    public function getModified()
    {
        return $this->modified;
    }

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    public function setAlias($alias)
    {
        $this->alias = $alias;
        return $this;
    }

    public function setDisplay($display)
    {
        $this->display = $display;
        return $this;
    }

    public function setPrivileges($privileges)
    {
        $this->privileges = $privileges;
        return $this;
    }

    public function setCreated()
    {
        $this->created = new \DateTime("now");
        return $this;
    }

    public function setModified()
    {
        $this->modified = new \DateTime("now");
        return $this;
    }
    
    public function toArray()
    {
        $hydrator = new Hydrator\ClassMethods();
        return $hydrator->extract($this);
    }
}
