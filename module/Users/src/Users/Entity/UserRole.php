<?php

namespace Users\Entity;

use Zend\Stdlib\Hydrator;
use Doctrine\ORM\Mapping as ORM;

use Doctrine\Common\Collections\Collection,
    Doctrine\Common\Collections\ArrayCollection;

/**
 * UserRole
 * 
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 * @ORM\Table(name="users_roles")
 * @ORM\Entity(repositoryClass="Users\Entity\Repository\UserRoleRepository")
 */
class UserRole
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="UserRole")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id")
     */
    protected $parent;

    /**
     * @var Collection
     * @ORM\OneToMany(targetEntity="User", mappedBy="role")
     */
    protected $users;
    
    /**
     * @ORM\OneToMany(targetEntity="UserRolePrivilege", mappedBy="role",cascade={"persist"})
     */
    private $userRolePrivilege;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=45, nullable=true)
     */
    private $name;

    /**
     * @var integer
     *
     * @ORM\Column(name="is_admin", type="integer", nullable=true)
     */
    private $isAdmin;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=true)
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime", nullable=true)
     */
    private $modified;

    public function __construct(array $options = array())
    {
        $hydrator = new Hydrator\ClassMethods();
        $hydrator->hydrate($options, $this);

        $this->users = new ArrayCollection();
        
        $this->created = new \DateTime("now");
        $this->modified = new \DateTime("now");
    }
    
    public function getId()
    {
        return $this->id;
    }

    public function getParent()
    {
        return $this->parent;
    }

    public function getUsers()
    {
        return $this->users;
    }
    
    public function getUserRolePrivilege()
    {
        return $this->userRolePrivilege;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getIsAdmin()
    {
        return $this->isAdmin;
    }

    public function getCreated()
    {
        return $this->created;
    }

    public function getModified()
    {
        return $this->modified;
    }

    public function setId($id)
    {
        if($id === '') {
            return $this;
        }
        
        if (!is_numeric($id)) {
            throw new \InvalidArgumentException('ID accept only integers.');
        }
        
        if ($id <= 0) {
            throw new \InvalidArgumentException('ID accept only number greater than zero.');
        }
        
        $this->id = $id;
        return $this;
    }

    public function setParent($parent)
    {
        $this->parent = $parent;
        return $this;
    }

    public function setUserRolePrivilege($userRolePrivilege)
    {
        if($userRolePrivilege == null) {
            return null;
        }
        
        if (!$userRolePrivilege instanceof UserRolePrivilege) {
            throw new \InvalidArgumentException('Attribute "userRolePrivilege" accept only instanceof "UserRolePrivilege".');
        }
        
        $this->userRolePrivilege = $userRolePrivilege;
        return $this;
    }

    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    public function setIsAdmin($isAdmin)
    {
        $this->isAdmin = $isAdmin;
        return $this;
    }

    public function setCreated()
    {
        $this->created = new \DateTime("now");
        return $this;
    }

    public function setModified()
    {
        $this->modified = new \DateTime("now");
        return $this;
    }

    public function toArray()
    {
        return array(
            'id' => $this->id,
            'parent' => $this->getParent(),
            'name'   => $this->name,
            //'user_role_privilege' => null,
            'is_admin' => $this->isAdmin,
        );
    }
}
