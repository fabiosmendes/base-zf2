<?php

namespace Users\Entity\Repository;

use Base\Entity\Repository\BaseRepository;

class UserResourceRepository extends BaseRepository
{
    public function fetchPairs()
    {
        $entities = $this->findAll();
        $array = array();
        foreach($entities as $entity) {
            $array[$entity->getId()] = $entity->getName();
        }

        return $array;
    }

}
