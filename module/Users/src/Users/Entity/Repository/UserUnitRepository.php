<?php

namespace Users\Entity\Repository;

use Base\Entity\Repository\BaseRepository;

class UserUnitRepository extends BaseRepository
{
    
    public function getUnitsUser($userId)
    {
        $list  = $this->findBy(array('user' => $userId));
        
        $ids = array();
        foreach($list as $item) {
           $ids[] = $item->getUnit();
        }
        return $ids;
    }
   
}
