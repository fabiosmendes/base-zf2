<?php

namespace Users\Entity\Repository;

use Base\Entity\Repository\BaseRepository;

class UserRoleRepository extends BaseRepository
{
    public function fetchParent()
    {
        $entities = $this->findAll();
        $array = array();
        foreach($entities as $entity) {
            $array[$entity->getId()] = $entity->getName();
        }
        
        return $array;
    }

}
