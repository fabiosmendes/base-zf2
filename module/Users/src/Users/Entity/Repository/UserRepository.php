<?php

namespace Users\Entity\Repository;

use Base\Entity\Repository\BaseRepository;

class UserRepository extends BaseRepository
{
    public function findByEmailAndPassword($email, $password)
    {
        $user = $this->findOneBy(array('email' => $email));
        
        if($user instanceof \Users\Entity\User) {
            $hashPassword = $user->encryptPassword($password);
            
            if($hashPassword == $user->getPassword()) {
                return $user;
            }    
        }

        return false;
    }
}
