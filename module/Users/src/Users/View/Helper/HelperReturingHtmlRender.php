<?php

namespace Front\View\Helper;

use Zend\View\Helper\AbstractHelper;

class HelperReturingHtmlRender extends AbstractHelper
{
    protected $postService;
    
    public function __construct($postService)
    {
        $this->postService = $postService;
    }
    
    public function __invoke($postCategorySlug, $limit = 4)
    {
        $listPosts = $this->postService
                          ->getPostsCategory($postCategorySlug, $limit);
        
        $template = "front/post/list-post-news";
        $pars = array('posts'  => $listPosts, 
                      'locale' => $this->locale);
        
        return $this->getView()->render($template, $pars);
    }

}
