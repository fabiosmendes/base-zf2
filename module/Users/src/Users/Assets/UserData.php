<?php

namespace Users\Assets;

class UserData
{
    public function getCompleteData()
    {
        return array(
            'first_name' => 'John',
            'last_name' => 'Lennon',
            'username' => 'lennon',
            'email' => 'john@beatles.com',
            'password' => '1969_end',
        );
    }
    
    public function getCompleteDataEdit()
    {
        return array(
            'id' => 1,
            'first_name' => 'John',
            'last_name' => 'Lennon',
            'username' => 'lennon',
            'email' => 'john@beatles.com',
            'password' => '1969_end',
        );
    }
        
    public function getCompleteDataForm()
    {
        return array(
            'first_name' => 'John',
            'last_name' => 'Lennon',
            'username' => 'lennon',
            'email' => 'john@beatles.com',
            'password' => '1969_end',
            'password_confirm' => '1969_end',
        );
    }
}