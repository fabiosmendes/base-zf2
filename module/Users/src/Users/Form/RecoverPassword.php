<?php

namespace Users\Form;

use Zend\Form\Form;

class RecoverPassword extends Form 
{
    public function __construct($name = '') 
    {
        parent::__construct('recover-password');
        
        $this->setAttribute('method', 'post');
        $this->setInputFilter(new RecoverPasswordFilter());
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Email',
            'name' => 'email',
            'options' => array(
                'label' => 'E-mail',
            ),
            'attributes' => array(
                'id' => 'email',
                'value' => '',
                'placeholder' => 'your e-mail',
                'class' => 'form-control',
            )
        ));
        
        $this->add(array(
            'name' => 'submit',
            'type' => 'Zend\Form\Element\Submit',
            'attributes' => array(
                'value' => 'Salvar',
                'class' => 'btn btn-default'
            )
        ));
    }
}
