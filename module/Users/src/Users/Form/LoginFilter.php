<?php

namespace Users\Form;

use Zend\InputFilter\InputFilter;

class LoginFilter extends InputFilter
{
    public function __construct()
    {
        $this->add(array(
            'name' => 'email',
            'required' => true,
            'filters' => array(
                array('name' => 'StringTrim'),
            ),
            'validators' => array(
                array(
                    'name' => 'NotEmpty',
                    'options' => array(
                        'messages' => array('isEmpty' => 'E-mail não pode estar vazio.'),
                    ),
                ),
                array(
                    'name' => 'EmailAddress',
                ),
            )
        ));
        
        $this->add(array(
            'name'=>'password',
            'required' => true,
            'filters'  => array(
                array('name' => 'StripTags'),
                array('name' => 'StringTrim'),
            ),
            'validators' => array(
                array(
                    'name' => 'NotEmpty',
                    'options' => array(
                        'messages'=>array('isEmpty' => 'Não pode estar em branco')
                    )
                )
            )
        ));  
    }

}
