<?php

namespace Users\Form;

use Zend\Form\Form;

class Login extends Form 
{
    public function __construct($name = '') 
    {
        parent::__construct('login');
        
        $this->setAttribute('method', 'post');
        $this->setInputFilter(new LoginFilter());
        
       $this->add(array(
            'type' => 'Zend\Form\Element\Email',
            'name' => 'email',
            'options' => array(
                'label' => 'E-mail',
            ),
            'attributes' => array(
                'id' => 'email',
                'value' => '',
                'placeholder' => 'e-mail de acesso',
                'class' => 'form-control',
                'autocomplete' => 'off',
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Password',
            'name' => 'password',
            'options' => array(
                'label' => 'Password',
            ),
            'attributes' => array(
                'id' => 'password',
                'value' => '',
                'placeholder' => 'senha',
                'class' => 'form-control',
                'autocomplete' => 'off',
            )
        ));
            
        $this->add(array(
            'name' => 'submit',
            'type' => 'Zend\Form\Element\Submit',
            'attributes' => array(
                'value' => 'Salvar',
                'class' => 'btn btn-success'
            )
        ));
    }
}
