<?php

namespace Users\Form;

use Zend\InputFilter\InputFilter;

class RecoverPasswordFilter extends InputFilter
{
    public function __construct()
    {
        $this->add(array(
            'name' => 'email',
            'required' => true,
            'filters' => array(
                array('name' => 'StringTrim'),
            ),
            'validators' => array(
                array(
                    'name' => 'NotEmpty',
                    'options' => array(
                        'messages' => array('isEmpty' => 'Informe seu e-mail de acesso.'),
                    ),
                ),
                array(
                    'name' => 'EmailAddress',
                ),
            )
        ));
    }

}
