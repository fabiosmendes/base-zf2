<?php

namespace Users\Parameters;

/**
 * Constantes os grupos de usuários
 */
class Roles
{
    /**
     * ID grupo admin
     */
    const ADMIN = 1;
    
    /**
     * Descrição de cada resposta
     * 
     * @var array
     */
    public static $descriptions = array(
        self::GREAT     => "Administrador",
    );
    
    public static function getDescription($index)
    {
        if(isset($this->descriptions[$index])) {
            return $this->descriptions[$index];
        }
        $title = "Tipo não definido";
        return $title;
    }
}    