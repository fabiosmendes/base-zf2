<?php

namespace Users\Service;

use Doctrine\ORM\EntityManager;
use Base\Service\AbstractService;

class UserManager extends AbstractService
{
    public function __construct(EntityManager $em) 
    {        
        $this->entity = 'Users\Entity\User';
        parent::__construct($em);
    }
    
    
}