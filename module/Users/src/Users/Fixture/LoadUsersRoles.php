<?php

namespace Users\Fixture;

use Doctrine\Common\DataFixtures\AbstractFixture,
    Doctrine\Common\DataFixtures\OrderedFixtureInterface,        
    Doctrine\Common\Persistence\ObjectManager;

use Users\Entity\UserRole;

class LoadUsersRoles extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        printf("Load Users Roles\n");
        
        $data = array(
            'name' => 'Admin',
            'is_admin'  => 1,
        );
        
        $user = new UserRole($data);
        
        $manager->persist($user);
        $manager->flush();
    }
    
    public function getOrder()
    {
        return 1;
    }
}