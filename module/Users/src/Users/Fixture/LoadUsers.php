<?php

namespace Users\Fixture;

use Doctrine\Common\DataFixtures\AbstractFixture,
    Doctrine\Common\DataFixtures\OrderedFixtureInterface,        
    Doctrine\Common\Persistence\ObjectManager;

use Users\Entity\User;

class LoadUsers extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        printf("Load Users\n");
        
        $role = $manager->find('\Users\Entity\UserRole', 1);
        
        $data = array(
            'first_name' => 'Admin',
            'last_name'  => 'Trump',
            'username'   => 'trumpag',
            'email'    => 'trumpag@gmail.com',
            'phone'    => '554196430788',
            'password' => 'keys',
            'role' => $role,
        );
        
        $user = new User($data);
        
        $manager->persist($user);
        $manager->flush();
    }
    
    public function getOrder()
    {
        return 2;
    }
}