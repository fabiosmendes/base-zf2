<?php

namespace Users\Controller;

use Zend\View\Model\ViewModel;
use Base\Controller\BaseController;
use Users\Form\Login as FrmLogin;
use Users\Form\RecoverPassword as FrmRecoverPassword;

use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Storage\Session as SessionStorage;

class LoginController extends BaseController 
{     
    const SERVICE_FLASHMESSENGER = 'ServiceFlashMessenger';
    const SERVICE_SESSION        = 'SessionManager';
    const SERVICE_AUTH           = 'Zend\Authentication\AuthenticationService';
    const SERVICE_ADAPTER        = 'Users\Auth\Adapter';
    const SERVICE_AUTH_EMAIL     = 'Users\Auth\AuthEmail';
    const SERVICE_RECOVER_PASSWORD = 'Users\Service\RecoverPassword';
    
    const MESSAGE_FAIL_LOGIN     = 'E-mail ou senha inválidos.';
    const MESSAGE_FAIL_RECOVER_PASSWORD = 'E-mail inválido ou não encontrado.';
    
    const ROUTE_LOGIN = 'user-account-login';
    const ROUTE_DASHBOARD = 'manager'; // @todo - move for parameters system 
    
    const SESSION_NAMESPACE = 'Manager'; // @todo - move for parameters system 
    
    public function accessAccountAction()
    {
        $frmLogin       = new FrmLogin();
        $request = $this->getRequest();
        
        if($request->isPost()) {
            $data = $request->getPost()->toArray();
            $frmLogin->setData($data);
            if($frmLogin->isValid()) {
                $auth = $this->getService(self::SERVICE_AUTH);
                $sessionStorage = new SessionStorage(self::SESSION_NAMESPACE);
                $auth->setStorage($sessionStorage);
                    
                $authAdapter = $this->getService(self::SERVICE_ADAPTER);
                $authAdapter->setEmail($data['email'])
                            ->setPassword($data['password']);
                
                $result = $auth->authenticate($authAdapter);
                $status = $this->getReturnAuth($result, $auth, $sessionStorage);
                $pars = array(
                    'status'       => $status,
                    'redirect_url' => self::ROUTE_DASHBOARD,
                );
                return $this->renderJson($pars);
            }
        }
        
        return $this->getLayoutLogin($frmLogin);
    }
    
    public function getReturnAuth($result, $auth, $sessionStorage)
    {
        $status = false;
        if($result->isValid()) {
            $identity = $auth->getIdentity();
            $sessionStorage->write($identity['user'], null);
            $status = true;
        }
        return $status;
    }
    
    public function getRedirectAuth($result, $auth, $sessionStorage, $flashMessenger)
    {
        if($result->isValid()) {
            $identity = $auth->getIdentity();
            $sessionStorage->write($identity['user'], null);
            return $this->redirect()->toRoute(self::ROUTE_DASHBOARD);
        } else {
            $flashMessenger->addMessage(self::MESSAGE_FAIL_LOGIN);
            return $this->redirect()->toRoute(self::ROUTE_LOGIN);
        }
    }
    
    public function getLayoutLogin($form)
    {
        /**
         * Modifica layout padrão
         */
        $this->layout('blank');
        
        $view = new ViewModel();
        $pars = array('form' => $form);
        $view->setVariables($pars);
        $view->setTemplate('login');
        
        return $view;
    }
    
    public function authReloadAction()
    {
        $session = $this->getServiceLocator()->get(self::SERVICE_SESSION);
        $userEmail = $session->offsetGet('email');
        
        if($userEmail == '') {
            return $this->redirect()->toRoute(self::ROUTE_LOGIN); 
        }
        
        $auth = $this->getServiceLocator()->get(self::SERVICE_AUTH);
        $sessionStorage = new SessionStorage(self::SESSION_NAMESPACE);
        $auth->setStorage($sessionStorage);
        
        $authAdapter = $this->getServiceLocator()->get(self::SERVICE_AUTH_EMAIL);
        $authAdapter->setEmail($userEmail);
        
        $result = $auth->authenticate($authAdapter);
        
        if($result->isValid()) {
            $this->flashMessenger()->addSuccessMessage('Seus dados foram alterados com sucesso!');
            
            $identity = $auth->getIdentity();
            $sessionStorage->write($identity['user'], null);
            return $this->redirect()->toRoute('manager', array('controller' => 'users', 
                                                               'action' => 'edit-account'));
        } 
        
        return $this->redirect()->toRoute(self::ROUTE_LOGIN);
    }
    
    
    public function authRegisterAction()
    {
        $session = $this->getServiceLocator()->get(self::SERVICE_SESSION);
        $userEmail = $session->offsetGet('user_email');
        
        if($userEmail == '') {
            return $this->redirect()->toRoute(self::ROUTE_LOGIN); 
        }
        
        $auth = $this->getServiceLocator()->get(self::SERVICE_AUTH);
        $sessionStorage = new SessionStorage(self::SESSION_NAMESPACE);
        $auth->setStorage($sessionStorage);
        
        $authAdapter = $this->getServiceLocator()->get(self::SERVICE_AUTH_EMAIL);
        $authAdapter->setEmail($userEmail);
        
        $result = $auth->authenticate($authAdapter);
        
        if($result->isValid()) {
            $identity = $auth->getIdentity();
            $sessionStorage->write($identity['user'], null);
            return $this->redirect()->toRoute(self::ROUTE_DASHBOARD);
        } 
        
        return $this->redirect()->toRoute(self::ROUTE_LOGIN);
    }
    
    public function recoverPasswordAction()
    {
        $form = new FrmRecoverPassword();
        $request = $this->getRequest();
        $serviceRecoverPass = $this->getServiceLocator()->get(self::SERVICE_RECOVER_PASSWORD);
        $flashMessenger = $this->getServiceLocator()->get(self::SERVICE_FLASHMESSENGER);
        
        if($request->isPost()) {
            $data = $request->getPost()->toArray();
            
            if($serviceRecoverPass->sendMailRecoverPassword($data)) {
                return $this->redirect()->toRoute('general', array('controller' => 'users-login', 
                                                                   'action'     => 'recover-password'));
            }
                
            $msg = self::MESSAGE_FAIL_RECOVER_PASSWORD;
            $flashMessenger->addMessage($msg);
            return $this->redirect()->toRoute(self::ROUTE_LOGIN);
        }
        
        return $this->getLayoutRecoverPassword($form);
    }
    
    public function getLayoutRecoverPassword($form)
    {
        /**
         * Modifica layout padrão
         */
        $this->layout('blank');
        
        $view = new ViewModel();
        $pars = array('form' => $form);
        $view->setVariables($pars);
        $view->setTemplate('recover-password');
        
        return $view;
    }
    
    public function logoffAction() 
    {
        $auth = new AuthenticationService();
        $auth->setStorage(new SessionStorage(self::SESSION_NAMESPACE));
        $auth->clearIdentity();
        
        return $this->redirect()->toRoute('manager');    
    }
}
