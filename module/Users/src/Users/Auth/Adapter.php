<?php

namespace Users\Auth;

use Zend\Authentication\Adapter\AdapterInterface;
use Zend\Authentication\Result;
use Doctrine\ORM\EntityManager;

class Adapter implements AdapterInterface
{
    /**
     * @var EntityManager
     */
    protected $em;
    
    protected $email;
    protected $password;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }
    
    public function authenticate()
    {
        $repository = $this->em->getRepository("Users\Entity\User");
        $entity = $repository->findByEmailAndPassword($this->getEmail(), $this->getPassword());
        
        if($entity) {
            return new Result(Result::SUCCESS, array('user' => $entity), array('OK'));
        } else {
            return new Result(Result::FAILURE_CREDENTIAL_INVALID, null, array());
        }
    }

}
