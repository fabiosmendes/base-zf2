<?php

namespace Users\Auth;

use Zend\Authentication\Adapter\AdapterInterface;
use Zend\Authentication\Result;
use Doctrine\ORM\EntityManager;

class AuthEmail implements AdapterInterface
{
    /**
     * @var EntityManager
     */
    protected $em;
    protected $email;    
    
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }
    
    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    public function authenticate()
    {
        $repository = $this->em->getRepository("Users\Entity\User");
        $entity = $repository->findOneBy(array('email' => $this->getEmail()));
        
        if($entity) {
            return new Result(Result::SUCCESS, array('user' => $entity), array('OK'));
        } else {
            return new Result(Result::FAILURE_CREDENTIAL_INVALID, null, array());
        }
    }
}