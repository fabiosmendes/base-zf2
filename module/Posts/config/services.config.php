<?php

namespace Front;

use Front\Service\Contact as ContactService;

return array(
    'factories' => array(
        'Front\Service\Contact' => function($sm) {
            return new ContactService($sm->get('Doctrine\ORM\EntityManager'), 
                                      $sm->get('Base\Mail\Mail'));
        },  
        'FrontManager\Form\Page\AddPage' => function($sm) {
            $addPageForm = new \FrontManager\Form\Page\AddPage();
            $addPageForm->setServiceLocator($sm);
            return $addPageForm;
        },        
        'FrontManager\Form\Page\EditPage' => function($sm) {
            $editPageForm = new \FrontManager\Form\Page\EditPage();
            $editPageForm->setServiceLocator($sm);
            return $editPageForm;
        },         
        'FrontManager\Service\Page' => function($sm) {
            return new \FrontManager\Service\Page($sm->get('Doctrine\ORM\EntityManager'),  
                                             $sm->get('Base\Http\FileTransfer'));
        },
        'FrontManager\Service\ContentArea' => function($sm) {
            return new \FrontManager\Service\ContentArea($sm->get('Doctrine\ORM\EntityManager'));
        },        
        'FrontManager\Form\Content\Add' => function($sm) {
            $addContentForm = new \FrontManager\Form\Content\Add();
            $contentArea = $sm->get('FrontManager\Service\ContentArea');
            $addContentForm->setCombos(array('area' => $contentArea->getCombo()));
            
            return $addContentForm;
        },        
        'FrontManager\Form\Content\Edit' => function($sm) {
            $editContentForm = new \FrontManager\Form\Content\Edit();
            $contentArea = $sm->get('FrontManager\Service\ContentArea');
            $editContentForm->setCombos(array('area' => $contentArea->getCombo()));
            
            return $editContentForm;
        },         
        'FrontManager\Service\Content' => function($sm) {
            return new \FrontManager\Service\Content($sm->get('Doctrine\ORM\EntityManager'));
        },        
        'FrontManager\Form\Banner\AddBanner' => function($sm) {
            $addBannerForm = new \FrontManager\Form\Banner\AddBanner();
            $addBannerForm->setServiceLocator($sm);
            return $addBannerForm;
        },        
        'FrontManager\Form\Banner\EditBanner' => function($sm) {
            $editBannerForm = new \FrontManager\Form\Banner\EditBanner();
            $editBannerForm->setServiceLocator($sm);
            return $editBannerForm;
        },         
        'FrontManager\Service\Banner' => function($sm) {
            return new \FrontManager\Service\Banner($sm->get('Doctrine\ORM\EntityManager'),  
                                                    $sm->get('Base\Http\FileTransfer'));
        },             
        'FrontManager\Service\PostFile' => function($sm) {
            return new \FrontManager\Service\PostFile($sm->get('Doctrine\ORM\EntityManager'));
        },  
        'FrontManager\Form\Post\AddPost' => function($sm) {
            $addPostForm = new \FrontManager\Form\Post\AddPost();
            $addPostForm->setServiceLocator($sm);
            return $addPostForm;
        },         
        'FrontManager\Form\Post\EditPost' => function($sm) {
            $editPostForm = new \FrontManager\Form\Post\EditPost();
            $editPostForm->setServiceLocator($sm);
            return $editPostForm;
        },         
        'FrontManager\Service\Post' => function($sm) {
            return new \FrontManager\Service\Post($sm->get('Doctrine\ORM\EntityManager'),
                                             $sm->get('FrontManager\Service\PostFile'),   
                                             $sm->get('Base\Http\FileTransfer'));
        },
        'FrontManager\Service\PostCategory' => function($sm) {
            return new \FrontManager\Service\PostCategory($sm->get('Doctrine\ORM\EntityManager'));
        },         
    ),          
                
);