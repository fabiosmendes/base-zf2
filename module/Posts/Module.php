<?php

namespace Front;

class Module
{
    public function onBootstrap($e) 
    {
//        $serviceManager = $e->getApplication()->getServiceManager();
//        
//        echo __FILE__.'<br/>'; 
//        echo __LINE__.'<br/>';
//        echo "<pre>";
//        //var_dump(get_class_methods( get_class($serviceManager) )); exit();
//        echo var_dump( $serviceManager->getRegisteredServices()); exit();
//        //echo "<pre>";var_dump( $serviceManager ); exit();
        
    }
    
    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }
    
    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ . 'Manager' => __DIR__ . '/src/' . __NAMESPACE__ . 'Manager',
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }
    
    public function getServiceConfig()
    {
        return include __DIR__ . '/config/services.config.php';
    }
    
    public function getValidatorConfig()
    {
        return array(
            'factories' => array(
                'FirstName' => function(\Zend\Validator\ValidatorPluginManager $pm) {
                    //$locator = $sm->getServiceLocator(); // $sm is the view helper manager, so we need to fetch the 
                    //main service manager
                    return new \Front\Validator\FirstAndLastName();
                },   
            ),           
        );
    }
    
    public function getViewHelperConfig()
    {
        return array(
            'factories' => array(
                'helloName' => function($sm) {
                    //$locator = $sm->getServiceLocator(); // $sm is the view helper manager, so we need to fetch the 
                    //main service manager
                    return new \Front\View\Helper\HelperMaster();
                },
                'getPostCategories' => function($sm) {
                    $serviceLocator = $sm->getServiceLocator();
                    $postCategory   = $serviceLocator->get('Front\Manager\Service\PostCategory');
                    return new \FrontManager\View\Helper\PostCategory($postCategory);
                },        
            ),
            'invokables' => array(
                'userIdentity' => new View\Helper\UserIdentity()
            )
        );
    }

}
