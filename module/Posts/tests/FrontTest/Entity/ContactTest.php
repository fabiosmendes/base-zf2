<?php

namespace FrontTest\Entity;

use Base\Entity\EntityTestCase;
use Front\Entity\Contact;
use FrontTest\Bootstrap;


class ContactTest extends EntityTestCase
{   
    public function setUp()
    {
        $this->sm = Bootstrap::getServiceManager();
        $this->em = $this->sm->get('Doctrine\ORM\EntityManager');
        
        $this->entity = 'Front\Entity\Contact';
        
        parent::setUp();
    }
    
    public function atributosEntidadeContato() 
    {
        return array(
            array('id'),
            array('name'),
            array('email'),
            array('telefone'),
            array('mensagem'),
            array('dataEnvio'),
        );
    }
    
    /**
     * 
     * @dataProvider atributosEntidadeContato()
     */
    public function testAtributosDaEntidadeContato($attributeName) 
    {
        $entity = new Contact();
        $this->assertObjectHasAttribute($attributeName, $entity);
                
    }
    
    
    public function testCriarNovoRegistroContatoComTodosOsDados()
    {
        $data = array(
            'nome'  => 'José Henrique Silva',
            'email' => 'jose.h@gmail.com',
            'telefone' => '(41) 3677-5000',
            'mensagem' => 'Um texto para a mensagem',
        );
        
        $contato = new Contact($data);
        $this->em->persist($contato);
        $this->em->flush();
        
        $this->assertEquals($data['name'], $contato->getName());
        $this->assertEquals($data['email'], $contato->getEmail());
        $this->assertEquals($data['telephone'], $contato->getTelephone());
        $this->assertEquals($data['message'], $contato->getMessage());
        
        return $contato->getId();
    }
    
    /**
     * @depends testCriarNovoRegistroContatoComNomeEEmailEMensagem
     */
    public function testUpdateClienteComNomEEmailPorEmail($id) 
    {
        $data = array(
            'email' => 'jose.henry@gmail.com',
        );
        
        $contato = $this->em->getRepository($this->entity)->find($id);
        
        $this->assertInstanceOf($this->entity, $contato);
        $this->assertNotEquals($data['email'], $contato->getEmail());
        
        $contato->setEmail($data['email']);
        
        $this->em->persist($contato);
        $this->em->flush();
        
        $this->assertEquals($data['email'], $contato->getEmail());
    }
 
    /**
     * @depends testCriarNovoRegistroContatoComNomeEEmailEMensagem
     */
    public function testRemocaoRegistroContatoComNomeEEmail($id)
    {
        $contato = $this->em->getRepository($this->entity)->find($id);
        $this->assertInstanceOf($this->entity, $contato);
        
        $this->em->remove($contato);
        $this->em->flush();
        
        $contatoRetorno = $this->em->getRepository($this->entity)->find($id);
        $this->assertEquals(false, $contatoRetorno);  
    }
}