<?php

namespace FrontTest\Assets;

class ContatosSql
{
    public static function criarTabelaContato($em)
    {
        //static::dropTable($em);
        $sqlCreateTable = ' CREATE TABLE contacts (
                            id int,
                            name varchar(255),
                            phone varchar(255),
                            email varchar(255),
                            message varchar(255),
                            register_at NUMERIC
                            ) ';
        
       $em->getConnection()->exec($sqlCreateTable);
    }
    
    
    public static function dropTable($em)
    {
        $sqlDropTable = ' DROP TABLE contacts ';
        $em->getConnection()->exec($sqlDropTable);
    }
}
