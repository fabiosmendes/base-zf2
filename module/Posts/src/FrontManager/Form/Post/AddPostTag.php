<?php

namespace FrontManager\Form\Post;

use Zend\Form\Form;

class AddPostTag extends Form
{

    public function __construct()
    {
        parent::__construct('settings');

        $this->setAttribute('method', 'post');

        $this->add(array(
            'name' => 'title',
            'options' => array(
                'type' => 'text',
                'label' => 'Título'
            ),
            'attributes' => array(
                'placeholder' => 'Título da Tag',
                'class' => 'input-block-level',
                'id' => 'PostTagTitle'
            )
        ));

        $this->add(array(
            'name' => 'slug',
            'options' => array(
                'type' => 'text',
                'label' => 'Slug'
            ),
            'attributes' => array(
                'placeholder' => 'slug-da-tag',
                'class' => 'input-block-level',
                'id' => 'PostTagSlug',
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Textarea',
            'name' => 'description',
            'options' => array(
                'type' => 'textarea',
                'label' => 'Descrição'
            ),
            'attributes' => array(
                'placeholder' => 'Descrição da Tag',
                'class' => 'span5',
            )
        ));

        $this->add(array(
            'name' => 'submit',
            'type' => 'Zend\Form\Element\Submit',
            'attributes' => array(
                'value' => 'Salvar',
                'class' => 'btn btn-primary'
            )
        ));
    }

}
