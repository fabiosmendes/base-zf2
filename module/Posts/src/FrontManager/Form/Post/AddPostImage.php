<?php

namespace FrontManager\Form\Post;

use Zend\Form\Form;

class AddPostImage extends Form
{

    public function __construct()
    {
        parent::__construct('settings');

        $this->setAttribute('method', 'post');

        $this->add(array(
            'name' => 'image',
            'options' => array(
                'type' => 'text',
                'label' => 'Título'
            ),
            'attributes' => array(
                'class' => 'input-block-level',
            )
        ));
        
        $this->add(array(
            'name' => 'enviar',
            'type' => 'Zend\Form\Element\Submit',
            'attributes' => array(
                'value' => 'Salvar',
                'class' => 'btn btn-primary'
            )
        ));
    }

}
