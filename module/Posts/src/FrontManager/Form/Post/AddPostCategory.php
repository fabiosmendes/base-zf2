<?php

namespace FrontManager\Form\Post;

use Zend\Form\Form;

class AddPostCategory extends Form
{

    public function __construct()
    {
        parent::__construct('settings');

        $this->setAttribute('method', 'post');

        $this->add(array(
            'name' => 'title',
            'options' => array(
                'type' => 'text',
                'label' => 'Título'
            ),
            'attributes' => array(
                'placeholder' => 'Título da Categoria',
                'class' => 'input-block-level',
                'id' => 'PostCategoryTitle'
            )
        ));

        $this->add(array(
            'name' => 'slug',
            'options' => array(
                'type' => 'text',
                'label' => 'Slug',
                'disable_inarray_validator' => true
            ),
            'attributes' => array(
                'placeholder' => 'slug-da-categoria',
                'class' => 'input-block-level',
                'id' => 'PostCategorySlug',
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'post_category_id',
            'options' => array(
                'type' => 'select',
                'label' => 'Categoria Pai',
                'disable_inarray_validator' => true
            ),
            'attributes' => array(
                'class' => 'uniformselect',
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Textarea',
            'name' => 'description',
            'options' => array(
                'type' => 'textarea',
                'label' => 'Descrição',
                'disable_inarray_validator' => true
            ),
            'attributes' => array(
                'placeholder' => 'Descrição da Categoria',
                'class' => 'span5',
            )
        ));

        $this->add(array(
            'name' => 'submit',
            'type' => 'Zend\Form\Element\Submit',
            'attributes' => array(
                'value' => 'Salvar',
                'class' => 'btn btn-primary'
            )
        ));
    }

}
