<?php

namespace FrontManager\Form\Menu;

use Zend\Form\Form;

class AddMenu extends Form
{

    public function __construct()
    {
        parent::__construct('settings');

        $this->setAttribute('method', 'post');

        $this->add(array(
            'name' => 'title',
            'options' => array(
                'type' => 'text',
                'label' => 'Título'
            ),
            'attributes' => array(
                'placeholder' => 'Título do Menu',
                'class' => 'input-block-level',
            )
        ));

        $this->add(array(
            'name' => 'url-menu',
            'options' => array(
                'type' => 'text',
                'label' => 'URL'
            ),
            'attributes' => array(
                'placeholder' => 'Endereço de URL, exemplo: http://www.example.com',
                'class' => 'input-block-level',
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'menu_id',
            'options' => array(
                'type' => 'select',
                'label' => 'Menu Pai'
            ),
            'attributes' => array(
                'class' => 'chzn-select',
                'style' => 'width:350px'
            )
        ));

        $this->add(array(
            'name' => 'enviar',
            'type' => 'Zend\Form\Element\Submit',
            'attributes' => array(
                'value' => 'Salvar',
                'class' => 'btn btn-primary'
            )
        ));
    }

}
