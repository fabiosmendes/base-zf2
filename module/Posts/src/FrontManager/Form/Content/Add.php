<?php

namespace FrontManager\Form\Content;

use Zend\Form\Form;

class Add extends Form
{
    public function __construct()
    {
        parent::__construct('page');

        $this->setAttribute('method', 'post');
        //$this->setInputFilter(new AddFilter());
        
        $this->add(array(
            'name' => 'title',
            'options' => array(
                'type' => 'text',
                'label' => 'Título'
            ),
            'attributes' => array(
                'placeholder' => 'Título da Página',
                'class' => 'span4',
            )
        ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'area',
            'options' => array(
                'type' => 'text',
                'label' => 'Área',
                'empty_option' => ' -- ',
                'value_options' => array(
                )
            ),
            'attributes' => array(
                'class' => 'uniformselect',
            )
        ));
        
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Textarea',
            'name' => 'text',
            'options' => array(
                'label' => 'Texto',
                'type' => 'textarea',
            ),
            'attributes' => array(
                'class' => 'tinymce',
                'id' => 'elm1',
                'rows' => '15',
                'cols' => '20',
                'style' => 'width: 20%',
            )
        ));
        
        $this->add(array(
            'name' => 'link',
            'options' => array(
                'type'  => 'text',
                'label' => 'Link'
            ),
            'attributes' => array(
                'id'          => 'link',
                'placeholder' => '',
                'class'       => 'span4',
            )
        ));
        
        $this->add(array(
            'name' => 'submit',
            'type' => 'Zend\Form\Element\Submit',
            'attributes' => array(
                'value' => 'Salvar',
                'class' => 'btn btn-primary'
            )
        ));
    }
    
    public function setCombos($lists)
    {
        foreach($lists as $name => $options) {
            if($this->has($name)) {
                $this->get($name)->setOptions(array('value_options' => $options));
            }
        }
    }
}
