<?php

namespace FrontManager\Service;

use Doctrine\ORM\EntityManager;
use Base\Service\AbstractService;

class PostFile extends AbstractService
{
    /**
     * 
     * @param \Doctrine\ORM\EntityManager $em
     * @param type $fileTransfer
     */
    public function __construct(EntityManager $em)
    {
        parent::__construct($em);
        $this->entity = "Front\Entity\PostFile";
    }
    
    public function getPostImage($postId)
    {
        $sqlImage  = ' SELECT name ';
        $sqlImage .= ' FROM  post_files';
        $sqlImage .= ' WHERE posts_id = '.$postId;
        $sqlImage .= ' LIMIT 1';
        
        $stmtImage = $this->em->getConnection()->prepare($sqlImage);
        try {
            $stmtImage->execute();
        }
        catch(\Exception $e) {
          // @todo - avaliar o registro de log de erros
          echo $e->getMessage(); 
          exit();   
        }
        
        $rsImage = $stmtImage->fetch();
        if($rsImage) {
            return $rsImage['name'];
        }
        
        return null;
    }
    
    public function removePostFileByPostId($postId)
    {
        $postFile = $this->getRepository()->findOneBy(array('post' => $postId));
        if($postFile) {
            $pathFile = $this->getPathMedia().'/posts/'.$postId.'/'.$postFile->getName();
            $this->removeFile($pathFile);    
            $this->em->remove($postFile);
            $this->em->flush(); 
        }
    }
    
    public function removePostFile($entity)
    {
        $postId   = $entity->getPost()->getId();
        $pathFile = $this->getPathMedia().'/posts/'.$postId.'/'.$entity->getName();
        $this->removeFile($pathFile);
            
        $this->em->remove($entity);
        $this->em->flush(); 
    }
    
    public function removePostFolder($entity)
    {
        $postId   = $entity->getPost()->getId();
        $pathFile = $this->getPathMedia().'/posts/'.$postId.'/';
        $this->removeDiretory($pathFile);
    }
}