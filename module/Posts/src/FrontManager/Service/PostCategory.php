<?php

namespace FrontManager\Service;

use Doctrine\ORM\EntityManager;
use Base\Service\AbstractService;
use Base\Filter\Inflector\Slug;
use Front\Entity\PostCategory as Category;

class PostCategory extends AbstractService
{

    /**
     * 
     * @param \Doctrine\ORM\EntityManager $em
     * @param type $fileTransfer
     */
    public function __construct(EntityManager $em)
    {
        parent::__construct($em);
    }

    public function getList($pars = array(), $limit = 40)
    {
        $dql  = " SELECT p FROM ".$this->entity." p ";
        $dql .= " WHERE p.id > 0";
            
        if(isset($pars['title']) && $pars['title'] != '') {
            $dql .= " AND p.title LIKE '%".$pars['title']."%'";  
        }
        
        $dql .= ' ORDER BY p.id DESC';
        $query = $this->em->createQuery($dql);
        
        $lista = $query->getResult();
        return $lista;
    }
    
    public function insert(array $data)
    {
        $data['slug'] = Slug::slugToLower($data['title'], '-');
        if (!isset($data['keywords'])) {
            $data['keywords'] = $data['slug'];
        }
        if (!isset($data['description'])) {
            $data['description'] = $data['slug'];
        }
        $post = new Category($data);
        $this->em->persist($post);
        $this->em->flush();
    }

    public function getCategories()
    {
        $query = $this->em->createQuery('SELECT partial c.{id, title} FROM Front\Entity\PostCategory c');
        return $query->getArrayResult();
    }
    
    public function getCombo()
    {
        $lista = $this->getCategories();
        $combo = array();
        foreach($lista as $item) {
            $categoryId = $item['id'];
            $title      = $item['title'];
            $combo[$categoryId] = $title;
        }
        
        return $combo;
    }

}
