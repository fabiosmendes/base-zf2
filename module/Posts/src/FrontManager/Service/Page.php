<?php

namespace FrontManager\Service;

use Doctrine\ORM\EntityManager;
use Base\Service\AbstractService;

class Page extends AbstractService
{
    /**
     * 
     * @param \Doctrine\ORM\EntityManager $em
     * @param type $fileTransfer
     */
    public function __construct(EntityManager $em, $fileTransfer)
    {
        parent::__construct($em);
        $this->fileTransfer = $fileTransfer;
        $this->entity = "Front\Entity\Page";
    }
    
    public function getList($pars = array(), $limit = 10)
    {
        $dql  = " SELECT p FROM ".$this->entity." p ";
        $dql .= " WHERE p.id > 0";
            
        if(isset($pars['title']) && $pars['title'] != '') {
            $dql .= " AND p.title LIKE '%".$pars['title']."%'";  
        }
        
        $dql .= ' ORDER BY p.id DESC';
        $query = $this->em->createQuery($dql);
        
        $lista = $query->getResult();
        return $lista;
    }
    
    public function getPage($id)
    {
        $page = $this->getRepository()->find($id);
        return $page;
    }
    
    public function update(array $data)
    {
        $language = \Front\Data\Languages::getLanguageData($data['language_id']);
        $data['slug']    = \Base\Filter\Inflector\Slug::slugToLower($data['title'], '-');
        if($language && $language[0] != 'pt') {
            $data['slug'] .= '-'.$language[0];
        }
        
        $data['excerpt'] = \Base\Filter\Inflector\Excerpt::getExcerpt($data['text'], 40);
        $data['users_id'] = 1;
        
        $photo = $data['image'];
        unset($data['image']);
        
        $entity = parent::update($data);
        
        $photoImage = $this->uploadFile('./public_html/media/pages/', $photo, 'image_'.$entity->getId().'-'.rand(99,999));
        if($photoImage != '') {
            $entity->setImage($photoImage);
        }
        
        $this->em->persist($entity);
        $this->em->flush();
        return $entity;
    }
    
    public function insert(array $data)
    {
        $language = \Front\Data\Languages::getLanguageData($data['language_id']);
        $data['slug']    = \Base\Filter\Inflector\Slug::slugToLower($data['title'], '-');
        if($language && $language[0] != 'pt') {
            $data['slug'] .= '-'.$language[0];
        }
        
        $data['excerpt'] = \Base\Filter\Inflector\Excerpt::getExcerpt($data['text'], 40);
        $data['users_id'] = 1;
        
        $photo = $data['image'];
        unset($data['image']);
        
        $entity = parent::insert($data);
        
        $photoImage = $this->uploadFile('./public_html/media/pages/', $photo, 'image_'.$entity->getId().'-'.rand(99,999));
        if($photoImage != '') {
            $entity->setImage($photoImage);
        }
        $this->em->persist($entity);
        $this->em->flush();
        
        return $entity;
    }
    
    public function delete($id)
    {
        $entity = $this->getRepository()->find($id);
        if($entity) {
            /*
            $postFiles = $entity->getPostFiles();
            foreach($postFiles as $postFile) {
                $this->postFiles->removePostFile($postFile);
                $this->postFiles->removePostFolder($postFile);
            }
            */
            $this->em->remove($entity);
            $this->em->flush();
            return $id;
        }
        
        return 0;
    }
}
