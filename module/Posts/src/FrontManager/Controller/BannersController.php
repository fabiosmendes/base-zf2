<?php

namespace FrontManager\Controller;

use BaseAdmin\Controller\CrudController;

class BannersController extends CrudController
{
    public function __construct()
    {
        $this->service = 'FrontManager\Service\Banner';
        $this->entity  = 'Front\Entity\Banner';
        $this->form    = 'FrontManager\Form\Page\NewBanner';
        $this->route      = 'general';
        $this->controller = 'manager-banners';
        
        parent::__construct();
    }
    
    public function editAction() 
    {
        $form     = $this->getServiceLocator()->get('FrontManager\Form\Banner\EditBanner');
        $service  = $this->getServiceLocator()->get($this->service);
        $flashMessenger = $this->getServiceLocator()->get('ServiceFlashMessenger');
        $request = $this->getRequest();
        
        if($request->isPost()) {
            $postData = $request->getPost()->toArray();
            $form->setData($postData);
            if($form->isValid()) {
                $postData['image']  = $this->params()->fromFiles('image'); // pegar arquivo no request
                $service->update($postData);
                
                $flashMessenger->addMessage('Banner alterado com sucesso!');
                return $this->redirect()->toRoute($this->route, array('controller' => $this->controller));
            }
        }
        else {
            $repository  = $this->getEm()->getRepository($this->entity);
            $id          = $this->params()->fromRoute('id', 0);
            $entity      = $repository->find($id);

            if(null == $entity) {
                return $this->redirect()->toRoute($this->route, array('controller' => $this->controller));
            }
            else { 
                $form->setData($entity->toArray());
            }  
        }
        
        return $this->renderView(array('form' => $form));
    }
    
    public function newAction() 
    {
        $form = $this->getServiceLocator()->get('FrontManager\Form\Banner\AddBanner');
        $flashMessenger = $this->getServiceLocator()->get('ServiceFlashMessenger');
        $request = $this->getRequest();
        
        if($request->isPost()) {
            $postData = $request->getPost()->toArray();
            $form->setData($postData);
            if($form->isValid()) {
                $postData['image']  = $this->params()->fromFiles('image'); // pegar arquivo no request
                $service = $this->getServiceLocator()->get($this->service);
                $service->insert($postData);
                
                $flashMessenger->addMessage('Banner cadastrado com sucesso!');
                return $this->redirect()->toRoute($this->route, array('controller' => $this->controller));
            }
        }
        
        return $this->renderView(array('form' => $form));
    }
    
    public function deleteAction() 
    {
        $service = $this->getServiceLocator()->get($this->service);
        $flashMessenger = $this->getServiceLocator()->get('ServiceFlashMessenger');
        $id      = $this->params()->fromRoute('id', 0);
        
        if($service->delete($id)) {
            
            $flashMessenger->addMessage('Banner excluído com sucesso!');
        } else {
            $messages = $service->getMessages();
            $flashMessenger->addMessage(isset($messages[0]) ? $messages[0] : "");
        }
        
        return $this->redirect()->toRoute($this->route, array('controller' => $this->controller));
            
    }

}
