<?php

namespace FrontManager\Controller;

use BaseAdmin\Controller\CrudController;

class PostsController extends CrudController
{
    public function __construct()
    {
        $this->service = 'FrontManager\Service\Post';
        $this->entity  = 'Front\Entity\Post';
        $this->form    = 'FrontManager\Form\Post\AddPost';
        $this->route      = 'general';
        $this->controller = 'manager-posts';
        
        parent::__construct();
    }
    
    public function addAction()
    {    
        $frmAddPost = $this->getServiceLocator()->get('FrontManager\Form\Post\AddPost');
        $service  = $this->getServiceLocator()->get($this->service);
        $flashMessenger = $this->getServiceLocator()->get('ServiceFlashMessenger');
        $request = $this->getRequest();
        
        $msg = '';
        $status = false;
        if ($request->isPost()) {
            $postData = $request->getPost()->toArray();
            $frmAddPost->setData($postData);
            if ($frmAddPost->isValid()) {
                $service = $this->getServiceLocator()->get('FrontManager\Service\Post');
                $postData['image'] = $this->params()->fromFiles('post_image');
                $service->setData($postData);
                $service->insert($postData);
                
                $flashMessenger->addMessage('Post salvo com sucesso!');
                $this->redirect()->toRoute('general', array('controller' => $this->controller));
            }
        }
        $pars = array('form' => $frmAddPost, 'msg' => $msg, 'status' => $status);
        return $this->renderView($pars);
    }

    public function editAction()
    {
        $formEditPost = $this->getServiceLocator()->get('FrontManager\Form\Post\EditPost');
        $service  = $this->getServiceLocator()->get($this->service);
        $flashMessenger = $this->getServiceLocator()->get('ServiceFlashMessenger');
        $request = $this->getRequest();
        
        if($request->isPost()) {
            $postData = $request->getPost()->toArray();
            $formEditPost->setData($postData);
            if($formEditPost->isValid()) {
                $postData['image'] = $this->params()->fromFiles('post_image');
                $service->setData($postData);
                if($service->update($postData)) {
                    $flashMessenger->addMessage('Dados do post alterados com sucesso!');
                    return $this->redirect()->toRoute($this->route, array('controller' => $this->controller));
                } else {
                    $this->setMessagesForm($formEditPost, $service->getMessages());
                }
                
            } 
        }
        else {
            $repository  = $this->getEm()->getRepository($this->entity);
            $id          = $this->params()->fromRoute('id', 0);
            $entity      = $repository->find($id);
            
            if(null == $entity) {
                return $this->redirect()->toRoute($this->route, array('controller' => $this->controller));
            }
            else { 
                $dados = $entity->toArray();
                $dados['category_id'] = $service->getCategoryId($entity);
                $dados['post_image']  = $service->getPostFiles()
                                                ->getPostImage($entity->getId());
                
                $formEditPost->setData($dados);
            }  
        }
        
        return $this->renderView(array('form' => $formEditPost));
    }

    public function deleteAction() 
    {
        $service = $this->getServiceLocator()->get($this->service);
        $flashMessenger = $this->getServiceLocator()->get('ServiceFlashMessenger');
        $id      = $this->params()->fromRoute('id', 0);
        $post    = $service->getPost($id);
        
        if($service->delete($id)) {
            $flashMessenger->addMessage('Post "'.$post->getTitle().'" excluído!');
        } else {
            $flashMessenger->addMessage('Post já excluído!');
        }
        
        return $this->redirect()->toRoute($this->route, array('controller' => $this->controller));
            
    }
    
}
