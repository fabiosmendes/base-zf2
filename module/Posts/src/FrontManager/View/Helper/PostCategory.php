<?php

namespace FrontManager\View\Helper;

use Zend\View\Helper\AbstractHelper;

class PostCategory extends AbstractHelper
{
    protected $postCategoryService;
    
    public function __construct($postCategoryService)
    {
        $this->postCategoryService = $postCategoryService;
    }
    public function __invoke()
    {
        return $this->postCategoryService->getCombo();
    }

}
