<?php

namespace Front\Fixture;

use Doctrine\Common\DataFixtures\AbstractFixture,
    Doctrine\Common\Persistence\ObjectManager;

use Front\Entity\Contact;

class LoadContact extends AbstractFixture
{
    public function load(ObjectManager $manager)
    {
        echo "Load Contact\n";
        
        $data = array(
            0 => array(
                'name'  => 'Fabio de Souza Mendes',
                'email' => 'fbiosm@gmail.com',
                'subject'   => 'Contato',
                'phone' => '(41) 3045-0345',
                'message'   => 'Gostaria de receber mais informações sobre o serviço.'
            ),
            1 => array(
                'name' => 'João Roberto Bello Mendes',
                'email' => 'jrbello@gmail.com',
                'subject' => 'Contato',
                'phone' => '(41) 3045-0345',
                'message' => 'Gostaria de receber mais informações sobre o serviço.'
            ),
            2 => array(
                'name'  => 'José Henrique Bello Mendes',
                'email' => 'joseh@gmail.com',
                'subject'   => 'Contato',
                'phone' => '(41) 3045-0345',
                'message'   => 'Gostaria de receber mais informações sobre o serviço.'
            ),
        );
        
        foreach($data as $item) {
            $contact = new Contact($item);
            $manager->persist($contact);
        }
        $manager->flush();
    }
    
    public function getOrder()
    {
        return 1;
    }
}
