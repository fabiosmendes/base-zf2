<?php

namespace Front\Filter;

use Zend\Filter\AbstractFilter;

class NameFilter extends AbstractFilter
{
    public function filter($value)
    {
        return strtoupper($value);
    }
}
