<?php

namespace Front\Controller;

use Base\Controller\BaseController;

class IndexController extends BaseController 
{        
    public function indexAction() 
    {
        $pars = array();        
        return $this->renderView($pars);
    }
}
