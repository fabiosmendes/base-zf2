<?php

namespace Front\Controller;

use Base\Controller\BaseController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use Front\Form\Modelo\CriarProduto as FrmProduto;

class CadastroController extends BaseController
{
    public function indexAction()
    {
        $frmProduto = new FrmProduto();
        $request = $this->getRequest();
        
        if($request->isPost()) {
            $postData = $request->getPost()->toArray();
            
            $frmProduto->setData($postData);
            if($frmProduto->isValid()) {
                $data = $frmProduto->getData();
                
                $produto = new \Front\Entity\Produto($data['produto']);
                var_dump($produto); exit();
                $produto->setCategorias($data['produto']['categorias']);
                $this->getEm()->persist($produto);
                $this->getEm()->flush();
                
                var_dump($produto); exit();
                
                return $this->redirect()->toRoute('front-cadastro');
            } 
        }
        
        $pars = array('form' => $frmProduto);
        return $this->renderView($pars);
    }
    
    public function deleteAction()
    {
        $this->getEm()->delete();
        
        exit();
    }
    
    public function renderFieldsetAction()
    {
        $frmProduto = new FrmProduto();
        $partial = $this->getServiceLocator()->get('viewhelpermanager')->get('partial');
        $html = $partial('front/forms/fieldset.phtml', array("fieldset" => $frmProduto->get('categoria[]'),'campo' => 'nome'));
        
        $pars = array('form' => $html);
        return new JsonModel($pars);
    }
    
    public function renderPartAction()
    {
        $parsView = array(
            'nome' => 'Fabio de Souza', 
            'email' => 'fbiosm@gmail.com'
        );
        
        $htmlViewPart = new ViewModel();
        $htmlViewPart->setTerminal(true)
                     ->setTemplate('front/cadastro/part')
                     ->setVariables($parsView);
        
        $htmlOutput = $this->getServiceLocator()
                           ->get('viewrenderer')
                           ->render($htmlViewPart);
        
        $jsonModel = new JsonModel();
        $jsonModel->setVariables(array(
            'html' => $htmlOutput,
            'jsonVar1' => 'jsonVal2',
            'jsonArray' => array(1,2,3,)
        ));
        
        return $jsonModel;
    }
}
