<?php

namespace Front\Validator;

use Doctrine\ORM\EntityManager;

interface ConfigAwareInterface
{
    public function setEntityManager(EntityManager $entityManager);
}
    
    