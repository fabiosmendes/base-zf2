<?php

namespace Front\Validator;

use Zend\Validator\AbstractValidator;
use Zend\Validator\Exception;

class FirstAndLastName extends AbstractValidator implements ConfigAwareInterface
{

    const INVALID_NAME = 'invalidName';

    protected $messageTemplates = array(
        self::INVALID_NAME => 'Informe seu nome completo',
    );
    
    public function setEntityManager(\Doctrine\ORM\EntityManager $entityManager)
    {
        //echo __LINE__.'<br/>'; echo "<pre>";var_dump($entityManager); exit();
    }

    /**
     * Retorna true se o valor for vazio ou se ao executar função explode com 
     * um espaço como primeiro parâmetro, o resultado for maior que 1. 
     *
     * @param  mixed $value
     * @return bool
     * @throws Exception\RuntimeException If validation of $value is impossible
     */
    public function isValid($value)
    {
        if ($value === "") {
            return true;
        }

        $partsValue = explode(" ", $value);

        if (count($partsValue) === 1) {
            $this->error(self::INVALID_NAME);
            return false;
        }
        return true;
    }

}
