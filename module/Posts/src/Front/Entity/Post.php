<?php

namespace Front\Entity;

use Doctrine\ORM\Mapping as ORM;
use Zend\Stdlib\Hydrator;


/**
 * Post
 *
 * @ORM\Table(name="posts")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 * @ORM\Entity(repositoryClass="Front\Entity\Repository\PostRepository")
 */
class Post
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=false)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=255, nullable=false)
     */
    private $slug;

    /**
     * @var string
     *
     * @ORM\Column(name="excerpt", type="string", length=255, nullable=true)
     */
    private $excerpt;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="keywords", type="text", nullable=true)
     */
    private $keywords;

    /**
     * @var string
     *
     * @ORM\Column(name="text", type="text", nullable=true)
     */
    private $text;
    
    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=45, nullable=true)
     */
    private $status;

    /**
     * @var boolean
     *
     * @ORM\Column(name="activated", type="boolean", nullable=true)
     */
    private $activated;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     */
    private $created;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime", nullable=false)
     */
    private $modified;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="post_files_id", type="integer", nullable=true)
     */
    private $postFilesId;
    
    /**
     * @ORM\OneToMany(targetEntity="Front\Entity\PostFile", mappedBy="post",cascade={"persist", "remove", "merge"})
     */
    private $files;
    
    /**
     * @ORM\OneToMany(targetEntity="PostCategoriesPosts", mappedBy="post",cascade={"persist", "remove", "merge"})
     */
    private $postCategories;
    
    
    public function __construct(array $options = array()) 
    {
        $hydrator = new Hydrator\ClassMethods();
        $hydrator->hydrate($options, $this);
        
        $this->files = new \Doctrine\Common\Collections\ArrayCollection();
        $this->postCategories = new \Doctrine\Common\Collections\ArrayCollection();

        $this->created  = new \DateTime("now");
        $this->modified = new \DateTime("now");
    }
    
    /**
     * 
     * @param \Front\Entity\PostFile $postFile
     * @return $this
     */
    public function addPostFile(PostFile $postFile)
    {
        $postFile->setPost($this);
        $this->files[] = $postFile;
        
        return $this;
    }
    
    /**
     * 
     * @param \Front\Entity\PostFile $postFile
     */
    public function removePostFile(PostFile $postFile)
    {
        $this->files->removeElement($postFile);
    }
   
    /**
     * 
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPostFiles()
    {
        return $this->files;
    }
    
    /**
     * 
     * @param \Front\Entity\PostCategoriesHasPosts $postCategories
     * @return $this
     */
    public function addPostCategories(PostCategoriesHasPosts $postCategories)
    {
        $postCategories->setPost($this);
        $this->postCategories[] = $postCategories;
        
        return $this;
    }
    
    /**
     * 
     * @param \Front\Entity\PostCategoriesHasPosts $postCategories
     */
    public function removePostCategory(PostCategoriesHasPosts $postCategories)
    {
        $this->postCategories->removeElement($postCategories);
    }
    
    /**
     * 
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPostCategories()
    {
        return $this->postCategories;
    }
    
    public function getId()
    {
        return $this->id;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function getSlug()
    {
        return $this->slug;
    }

    public function getExcerpt()
    {
        return $this->excerpt;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function getKeywords()
    {
        return $this->keywords;
    }

    public function getText()
    {
        return $this->text;
    }
    
    public function getStatus()
    {
        return $this->status;
    }

    public function getActivated()
    {
        return $this->activated;
    }

    public function getCreated()
    {
        return $this->created;
    }

    public function getModified()
    {
        return $this->modified;
    }

    public function getPostFilesId()
    {
        return $this->postFilesId;
    }
    
    public function getFiles()
    {
        return $this->files;
    }
    
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    public function setSlug($slug)
    {
        $this->slug = $slug;
        return $this;
    }

    public function setExcerpt($excerpt)
    {
        $this->excerpt = $excerpt;
        return $this;
    }

    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    public function setKeywords($keywords)
    {
        $this->keywords = $keywords;
        return $this;
    }

    public function setText($text)
    {
        $this->text = $text;
        return $this;
    }
    
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    public function setActivated($activated)
    {
        $this->activated = $activated;
        return $this;
    }

    public function setCreated()
    {
        $this->created = new \DateTime("now");
        return $this;
    }

    /**
     * @ORM\PreUpdate
     */
    public function setModified()
    {
        $this->modified = new \DateTime("now");
        return $this;
    }

    public function setPostFilesId($postFilesId)
    {
        $this->postFilesId = $postFilesId;
        return $this;
    }
    
    public function setFiles($files)
    {
        $this->files = $files;
        return $this;
    }
    
    public function toArray() 
    {
        $hydrator = new Hydrator\ClassMethods();
        return $hydrator->extract($this);
    }
    
    public function __toString()
    {
        return $this->title;
    }
}
