<?php

namespace Front\Entity;

use Doctrine\ORM\Mapping as ORM;
use Zend\Stdlib\Hydrator;

/**
 * PostCategoriesHasPosts
 *
 * @ORM\Table(name="post_categories_posts")
 * @ORM\Entity
 */
class PostCategoriesPosts
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;
    
    /**
     * @ORM\ManyToOne(targetEntity="Post", inversedBy="postCategories")
     * @ORM\JoinColumn(name="posts_id", referencedColumnName="id")
     */
    private $post;

    /**
     * @ORM\ManyToOne(targetEntity="PostCategory", inversedBy="postCategories")
     * @ORM\JoinColumn(name="post_categories_id", referencedColumnName="id")
     */
    private $category;
    
    public function __construct(array $options = array()) 
    {
        $hydrator = new Hydrator\ClassMethods();
        $hydrator->hydrate($options, $this);
    }
    
    public function getId()
    {
        return $this->id;
    }
    
    public function getPost()
    {
        return $this->post;
    }

    /**
     * @param \Front\Entity\Post $post
     * @return \Front\Entity\PostCategoriesHasPosts
     */
    public function setPost(Post $post)
    {
        $this->post = $post;
        return $this;
    }
    
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param \Front\Entity\PostCategory $post
     * @return \Front\Entity\PostCategoriesHasPosts
     */
    public function setCategory(PostCategory $category)
    {
        $this->category = $category;
        return $this;
    }
    
    
    public function toArray() 
    {
        $hydrator = new Hydrator\ClassMethods();
        return $hydrator->extract($this);
    }

}
