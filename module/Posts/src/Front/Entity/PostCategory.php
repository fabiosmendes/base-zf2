<?php

namespace Front\Entity;

use Doctrine\ORM\Mapping as ORM;
use Zend\Stdlib\Hydrator;

/**
 * PostCategories
 *
 * @ORM\Table(name="post_categories")
 * @ORM\Entity
 */
class PostCategory
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=false)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=255, nullable=false)
     */
    private $slug;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=false)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="keywords", type="text", nullable=false)
     */
    private $keywords;

    /**
     * @var integer
     *
     * @ORM\Column(name="node", type="integer", nullable=true)
     */
    private $node;

    /**
     * @var integer
     *
     * @ORM\Column(name="node_id", type="integer", nullable=true)
     */
    private $nodeId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime", nullable=false)
     */
    private $modified;

    /**
     * @ORM\OneToMany(targetEntity="PostCategoriesPosts", mappedBy="category")
     */
    private $postCategories;
    
    public function __construct(array $options = array())
    {
        $hydrator = new Hydrator\ClassMethods();
        $hydrator->hydrate($options, $this);

        $this->postCategories = new \Doctrine\Common\Collections\ArrayCollection();
        $this->node   = 0;
        $this->nodeId = 0;
        
        $this->created  = new \DateTime("now");
        $this->modified = new \DateTime("now");
    }

    public function getId()
    {
        return $this->id;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function getSlug()
    {
        return $this->slug;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function getKeywords()
    {
        return $this->keywords;
    }

    public function getNode()
    {
        return $this->node;
    }

    public function getNodeId()
    {
        return $this->nodeId;
    }

    public function getPostCategories()
    {
        return $this->postCategories;
    }
    
    public function setPostCategories($postCategories)
    {
        $this->postCategories = $postCategories;
        return $this;
    }

    public function getCreated()
    {
        return $this->created;
    }

    public function getModified()
    {
        return $this->modified;
    }

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    public function setSlug($slug)
    {
        $this->slug = $slug;
        return $this;
    }

    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    public function setKeywords($keywords)
    {
        $this->keywords = $keywords;
        return $this;
    }
    
    public function setNode($node)
    {
        $this->node = $node;
        return $this;
    }

    public function setNodeId($nodeId)
    {
        $this->nodeId = $nodeId;
        return $this;
    }
    
    public function setCreated()
    {
        $this->created = new \DateTime("now");
        return $this;
    }

    public function setModified()
    {
        $this->modified = new \DateTime("now");
        return $this;
    }

    public function toArray()
    {
        $hydrator = new Hydrator\ClassMethods();
        return $hydrator->extract($this);
    }

}
