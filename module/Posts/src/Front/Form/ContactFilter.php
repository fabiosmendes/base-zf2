<?php

namespace Front\Form;

use Zend\InputFilter\InputFilter;


class ContactFilter extends InputFilter
{
    public function __construct()
    {
        $this->add(array(
            'name' => 'name',
            'required' => true,
            'filters' => array(
                //array('name' => 'StripTags'),
                //array('name' => 'StringTrim'),
                array('name' => '\Front\Filter\NameFilter'),
            ),
            'validators' => array(
                array(
                    'name' => 'NotEmpty',
                    'options' => array(
                        'messages' => array('isEmpty' => 'Nome não pode estar em branco.')
                    )
                ),
                array(
                    'name' => '\Front\Validator\FirstAndLastName', // Custom Validator
                ),
            )
        ));

        $this->add(array(
            'name' => 'email',
            'required' => true,
            'filters' => array(
                array('name' => 'StringTrim'),
            ),
            'validators' => array(
                array(
                    'name' => 'NotEmpty',
                    'options' => array(
                        'messages' => array('isEmpty' => 'E-mail não pode estar vazio.'),
                    ),
                ),
                array(
                    'name' => 'EmailAddress',
                ),
            )
        ));

        $this->add(array(
            'name' => 'phone',
            'required' => true,
            'filters' => array(
                array('name' => 'StringTrim'),
            ),
            'validators' => array(
                array(
                    'name' => 'NotEmpty',
                    'options' => array(
                        'messages' => array('isEmpty' => 'Telefone não pode estar vazio.'),
                    ),
                ),
            )
        ));

        $this->add(array(
            'name' => 'message',
            'required' => true,
            'filters' => array(
                array('name' => 'StringTrim'),
            ),
            'validators' => array(
                array(
                    'name' => 'NotEmpty',
                    'options' => array(
                        'messages' => array('isEmpty' => 'Preencha o campo mensagem.'),
                    ),
                ),
            )
        ));
        
    }

}
