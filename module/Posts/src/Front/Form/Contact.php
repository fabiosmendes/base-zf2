<?php

namespace Front\Form;

use Zend\Form\Form;
use Zend\Captcha;

class Contact extends Form
{
    public function __construct($urlcaptcha = null)
    {
        parent::__construct('contact');

        $this->setAttribute('method', 'post');
        $this->setInputFilter(new ContactFilter());
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'name',
            'options' => array(
                'label' => 'Nome Completo'
            ),
            'attributes' => array(
                'id'          => 'name',
                'placeholder' => 'Entre com o nome',
                'class' => '',
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Email',
            'name' => 'email',
            'options' => array(
                'label' => 'Seu e-mail',
            ),
            'attributes' => array(
                'id' => 'email',
               'value' => '',
                'placeholder' => 'ex:seu@dominio.com',
                'class' => '',
            )
        ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'phone',
            'options' => array(
                'type' => 'text',
                'label' => 'Telefone'
            ),
            'attributes' => array(
                'id' => 'phone',
                'placeholder' => '( )',
                'class' => ''
            )
        ));


        $this->add(array(
            'type' => 'Zend\Form\Element\Textarea',
            'name' => 'message',
            'options' => array(
                'label' => 'Mensagem'
            ),
            'attributes' => array(
                'id' => 'message',
                'class' => ''
            )
        ));
        
        $dirdata = './data';
        $dirDataImg = './public/images/captcha';
        //pass captcha image options
        $captchaImage = new \Zend\Captcha\Image(array(
                'font'   => $dirdata . '/fonts/arial.ttf',
                'width'  => 250,
                'height' => 100,
                'dotNoiseLevel' => 80,
                'lineNoiseLevel' => 3)
        );
        $captchaImage->setImgDir($dirDataImg);
        $captchaImage->setImgUrl($urlcaptcha);
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Captcha',
            'name' => 'captcha',
            'options' => array(
                'label' => 'Informe os caracteres a seguir:',
                'captcha' => $captchaImage,
            ),
        ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Submit',
            'name' => 'enviar',
            'attributes' => array(
                'value' => 'Salvar',
                'class' => ''
            )
        ));
        
    }
    
}
