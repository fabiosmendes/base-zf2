<?php

namespace Front\Service;

use Doctrine\ORM\EntityManager;
use Base\Service\AbstractService;

class Contact extends AbstractService
{
    protected $mail;
    protected $emailTo;

    public function __construct(EntityManager $em, $mail)
    {
        parent::__construct($em);

        $this->entity  = "Front\Entity\Contact";
        $this->mail    = $mail;
        
        $this->emailTo = "fabio@redsuns.com.br";
    }
    
    protected function getCurrentDateFormat()
    {
        $dateTime    = new \DateTime("now");
        return $dateTime->format('d-m-Y H:i:s');
    }
    
    public function sendEmail($pars = array())
    {
        if(count($pars) > 0) {
            $entity = parent::insert($pars);
            if($entity) {
                $subject  = 'Contato - '.$this->getCurrentDateFormat();
                
                $this->mail->setPage('contact/contact-site')
                           ->setSubject($subject)
                           ->setTo($this->emailTo)
                           ->setData($pars)
                           ->prepare()
                           ->send();

                return $entity;
            }
        }
        return false;
    }
}
