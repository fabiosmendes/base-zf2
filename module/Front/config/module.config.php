<?php

namespace Front;

return array(
    'router' => array(
        'routes' => array(
            'home' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/',
                    'defaults' => array(
                        'controller' => 'index',
                        'action' => 'index'
                    ),
                ),
            ),
            'site' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/site[/:controller[/:action]][/:id]',
                    'constraints' => array(
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        'action' => 'index'
                    ),
                ),
                
            ),
           'general' => array(
                'type'    => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route' => '[/:controller[/:action]][/:id]',
                    'constraints' => array(
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                       'action' => 'index'
                    ),
                ),
            ),
            'front-contact' => array(
                'type'    => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/contact[/:action]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller'    => 'contact',
                        'action'        => 'index',
                    ),
                ),
            ),
            // config de uma rota para uma página estática (para sobre nós, quem somos, perguntas e etc)
            'front-about-us' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/about-us', // acesso na url
                    'defaults' => array(
                        'controller' => 'pages',
                        'action'     => 'about-us',
                    ),
                ),
            ),            
        ),
    ),
    'controllers' => array(
        'invokables' => array(
            'index'    => 'Front\Controller\IndexController',
            'contact'  => 'Front\Controller\ContactController',
            'pages'    => 'Front\Controller\PagesController',
            
            'manager-contents'       => 'FrontManager\Controller\ContentsController',
            'manager-posts'          => 'FrontManager\Controller\PostsController',
            'manager-posts-category' => 'FrontManager\Controller\PostsCategoryController',
            'manager-banners' => 'FrontManager\Controller\BannersController',
            'manager-pages'   => 'FrontManager\Controller\PagesController',
            'manager-post'    => 'FrontManager\Controller\PostController',
            'manager-post'    => 'FrontManager\Controller\PostController',
        ),
    ),
    'service_manager' => array(
        'factories' => array(
            'translator' => 'Zend\I18n\Translator\TranslatorServiceFactory',
        ),
    ),
    'translator' => array(
        'locale' => 'en_US',
        'translation_patterns' => array(
            array(
                'type' => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern' => '%s.mo',
            ),
        ),
    ),
    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => array(
            //'layout/layout'      => __DIR__ . '/../view/layout/layout.phtml', // move for the Site module
            //'front/index/index'    => __DIR__ . '/../view/front/index/index.phtml', // move for the Site module
            'error/404'            => __DIR__ . '/../view/error/404.phtml',
            'error/index'          => __DIR__ . '/../view/error/index.phtml',
        ),
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
        'strategies' => array(
            'ViewJsonStrategy',
        )
    ),
    'doctrine' => array(
        'driver' => array(
            __NAMESPACE__ . '_driver' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(__DIR__ . '/../src/' . __NAMESPACE__ . '/Entity')
            ),
            'orm_default' => array(
                'drivers' => array(
                    __NAMESPACE__ . '\Entity' => __NAMESPACE__ . '_driver'
                )
            )
        ),
        'fixture' => array(
            __NAMESPACE__ . '_fixture' => __DIR__ . '/../src/' . __NAMESPACE__ . '/Fixture',
        ),
    ),
);