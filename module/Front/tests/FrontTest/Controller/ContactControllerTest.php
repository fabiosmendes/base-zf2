<?php

namespace FrontTest\Controller;

//use Zend\Http\Request as HttpRequest;

use FrontTest\Bootstrap;
use Base\Controller\TestCaseController;

/**
 * @group controller
 * @group contact
 */
class ContatoControllerTest extends TestCaseController
{
    public function setUp()
    {
        $this->setApplicationConfig(Bootstrap::getConfig());
        parent::setUp();
    }
    
    public function testIfIndexActionCanBeAccessed()
    {
        $this->dispatch('/contact');
        $this->assertResponseStatusCode(200);    
        $this->assertModuleName('Front');
        $this->assertControllerName('Contact');
        $this->assertControllerClass('ContactController');
        $this->assertActionName('index');
        $this->assertMatchedRouteName('rota-modelo');
    }
    
    public function testNovoActionPodeSerAcessada()
    {
        $this->dispatch('/url-acesso');
        $this->assertResponseStatusCode(200);
        
        $this->assertModuleName('Modulo');
        $this->assertControllerName('Modelo');
        $this->assertControllerClass('ModeloController');
        $this->assertActionName('index');
        $this->assertMatchedRouteName('rota-modelo');
    }


    
    public function testSendPostDataInvalidIndexAction()
    {
        $this->assertTrue(false);
    }
    
    public function testSendPostDataValidIndexAction()
    {
        $this->assertTrue(false);
    }
    
    public function testIsRedirectAfterPost()
    {
        $this->assertTrue(false);
    }
    
    public function testSendPostForIndexActionNotOriginInTheSameMethod()
    {
        $this->assertTrue(false);
    }
}