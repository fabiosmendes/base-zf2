<?php

namespace FrontTest\Form;

use Base\Form\FormTestCase;
use Front\Entity\Contact;
use Front\Form\Contact as FrmContato;

use FrontTest\Bootstrap;

class ContactFormTest extends FormTestCase
{
    public function setUp()
    {
        $this->sm = Bootstrap::getServiceManager();
        parent::setUp();
        
        $this->entity = new  Contact();
        $this->form   = new FrmContato();
        //$this->form->setInputFilter($this->entity->getInputFilter());
        
        
    }
    
    public function camposFormularioContato() 
    {
        return array(
            array('nome'),
            array('email'),
            array('telefone'),
            array('mensagem'),
        );
    }
    
    /**
     * 
     * @dataProvider camposFormularioContato()
     */
    public function testCamposFormularioContato($campoNome) 
    {
        $this->assertTrue($this->form->has($campoNome));           
    }
        
    
    public function testCanInsertNewRecord()
    { 
        $data = array(
            'nome' => 'Fabio de Souza',
            'email' => 'fbiosm@gmail.com',
            'telefone' => '(41) 3677-8080',
            'mensagem' => 'Um teste de texto para enviar uma mensagem'
        );
        
        $this->form->setData($data);
        $this->assertTrue($this->form->isValid());
    }
    
    public function testCannotInsertNewRecordWithInvalidData()
    { 
        $data = array(
            'nome' => '',
            'email' => 'fbiosm@gmail.com',
            'telefone' => '(41) 3677-8080',
            'mensagem' => 'Um teste de texto para enviar uma mensagem'
        );
        
        $this->form->setData($data);
        $this->assertFalse($this->form->isValid());
        $this->assertEquals(1, count($this->form->get('nome')->getMessages()));
    }
    
    public function testCanUpdateExistingRecord()
    {
        $contato = $this->em->find('Front\Entity\Contato', 1);
        
        $data = array(
            'nome' => 'Fabio',
            'email' => 'fbiosm@gmail.com',
            'telefone' => '(41) 36775000',
            'mensagem' => 'Mensagem',
        );
        $this->form->setData($data);
        $this->assertTrue($this->form->isValid());
    }

    public function testCannnotUpdateExistingRecordWithInvalidData()
    {
        $album = $this->em->find('Front\Entity\Contato', 1);
        $data = array(
            'nome' => 'Fabio',
            'email' => '',
            'telefone' => '(41) 36775000',
            'mensagem' => 'mensagem enviada',
        );
        
        $this->form->setData($data);
        $this->assertFalse($this->form->isValid());
        $numMessages =  count($this->form->get('email')->getMessages());
        $this->assertTrue(($numMessages > 0));
    }
}