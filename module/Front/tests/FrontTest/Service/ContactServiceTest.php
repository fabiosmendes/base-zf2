<?php

namespace FrontTest\Form;

use Zend\Mail\Transport\Smtp as SmtpTransport;
use Zend\View\Model\ViewModel;

use Base\Service\ServiceTestCase;
use Front\Service\Contato as ServiceContato;
use FrontTest\Bootstrap;
use FrontTest\Assets\ContatcSql;

class ContactServiceTest extends ServiceTestCase
{
    public function setUp()
    {        
        $this->sm = Bootstrap::getServiceManager();
        
        $this->em = $this->sm->get('Doctrine\ORM\EntityManager');
        $transport = new SmtpTransport();
        $view = new ViewModel();
        $this->service = new ServiceContato($this->em, $transport, $view);
        
        parent::setUp();
    }
    
    
    public function testEnviarEmail()
    {
        $dados = array(
            'nome' => 'Fabio de Souza',
            'email' => 'fbiosm@hotmail.com',
            'telefone' => '(41) 3677-5000',
            'mensagem' => 'datax',  
        );
        /*
        $entity = $this->service->enviarEmail($dados);
        $this->assertEquals($dados['email'], $entity->getEmail());
        */
    }
    
}