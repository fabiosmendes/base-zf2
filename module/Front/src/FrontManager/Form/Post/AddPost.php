<?php

namespace FrontManager\Form\Post;

use Zend\Form\Form;
use Zend\ServiceManager\ServiceLocatorInterface;

class AddPost extends Form
{
    public function __construct()
    {
        parent::__construct('post');

        $this->setAttribute('method', 'post');
        $this->setInputFilter(new AddPostFilter());

        $this->add(array(
            'name' => 'title',
            'options' => array(
                'type' => 'text',
                'label' => 'Título'
            ),
            'attributes' => array(
                'placeholder' => 'Título da Página',
                'class' => 'span4',
            )
        ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\File',
            'name' => 'post_image',
            'options' => array(
                'type' => 'file',
                'label' => 'Imagem',
            ),
            'attributes' => array(
                'class' => '',
            )
        ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Textarea',
            'name' => 'text',
            'options' => array(
                'label' => 'Texto',
                'type' => 'textarea',
            ),
            'attributes' => array(
                'class' => 'tinymce',
                'id' => 'elm1',
                'rows' => '15',
                'cols' => '20',
                'style' => 'width: 20%',
            )
        ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Textarea',
            'name' => 'excerpt',
            'options' => array(
                'label' => 'Chamada',
                'type' => 'textarea',
            ),
            'attributes' => array(
                'class' => 'tinymce',
                'id' => 'excerpt',
                'rows' => '5',
                'cols' => '20',
                'style' => 'width: 20%',
            )
        ));
        
        $this->add(array(
            'name' => 'submit',
            'type' => 'Zend\Form\Element\Submit',
            'attributes' => array(
                'value' => 'Salvar',
                'class' => 'btn btn-primary'
            )
        ));
    }
    
    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
    }

    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }  
    
    public function isValid()
    {
        $userService = $this->serviceLocator->get('FrontManager\Service\Post');
        $isValid = parent::isValid();
        
        /*
        $id    = $this->get('id')->getValue();
        $email = $this->get('email')->getValue();
        if(!$userService->checkEmailUser($email, $id)) {
            $messagesCadastro = array(
                'emailCadastrado' => 'E-mail já cadastrado com outro usuário',
            );
            
            $this->get('email')->setMessages($messagesCadastro);
            $isValid = false;
        }
        
        $currentPassword = $this->get('current_password')->getValue();
        $newPassword     = $this->get('new_password')->getValue();
        $confirmNewPassword     = $this->get('confirm_new_password')->getValue();
        if($newPassword != null) {
            $checkPassword = $userService->checkPassword($email, $currentPassword);
            if(!$checkPassword) {
                 $senhaInvalida = array(
                    'senhaInvalida' => 'A senha informada é inválida!',
                );
                $this->get('current_password')->setMessages($senhaInvalida);
                $isValid = false;
            } else if($newPassword !== $confirmNewPassword) { 
                $confirmacaoSenha = array(
                    'confirmacaoSenha' => 'A confirmação da senha não confere com a senha informada!',
                );
                $this->get('confirm_new_password')->setMessages($confirmacaoSenha);
                $isValid = false;
            }
        }
        */
        return $isValid;
    }
}
