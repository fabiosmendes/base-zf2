<?php

namespace FrontManager\Service;

use Doctrine\ORM\EntityManager;
use Base\Service\AbstractService;
use Front\Entity\PostFile;

class Post extends AbstractService
{
    protected $postFiles;
    protected $post;

    /**
     * 
     * @param \Doctrine\ORM\EntityManager $em
     * @param type $fileTransfer
     */
    public function __construct(EntityManager $em, $postFiles, $fileTransfer)
    {
        parent::__construct($em);
        $this->postFiles    = $postFiles;
        $this->fileTransfer = $fileTransfer;
        $this->entity = "Front\Entity\Post";
    }
    
    public function getPostFiles()
    {
        return $this->postFiles;
    }
    
    public function getList($pars = array(), $limit = 10)
    {
        $dql  = " SELECT p FROM ".$this->entity." p ";
        $dql .= " WHERE p.id > 0";
            
        if(isset($pars['title']) && $pars['title'] != '') {
            $dql .= " AND p.title LIKE '%".$pars['title']."%'";  
        }
        
        if(isset($pars['language_id']) && $pars['language_id'] != '') {
            $dql .= " AND p.languageId LIKE '%".$pars['language_id']."%'";  
        }
        
        $dql .= ' ORDER BY p.id DESC';
        $query = $this->em->createQuery($dql);
        
        $lista = $query->getResult();
        return $lista;
    }
    
    public function getPost($id)
    {
        $post = $this->getRepository()->find($id);
        return $post;
    }
    
    public function getCategoryId($entityPost)
    {
        $postCategories = $entityPost->getPostCategories();
        $categoryId = 0;
        foreach($postCategories as $category) {
            $categoryId = $category->getCategory()->getId();
        }
        
        return $categoryId;
    }
    
    /**
     * Save post files
     * @param type $data
     * @param type $image
     */
    public function setPostFiles($data, $image)
    {
        $image = (!is_numeric(array_values($image[0]))) ? array($image) : $image;
        foreach ($image as $img) {
            $img['title'] = $data['title'];
            $img['slug'] = \Base\Filter\Inflector\Slug::slugToLower($data['title'], '-');
            $img['name'] = $img['slug'] . \Base\File\Upload::extensionFile($img['name']);
            $img['nameDir'] = \Base\File\Upload::pathFile("Front\Entity\PostFile", "insert") . $img['name'];
            $postFile = new PostFile;
            $postFile->setPost($img);
            $this->em->persist($postFile);
        }
    }

    /**
     * Save post photo
     * @todo - mover para service PostFile
     * @param type $post
     * @param type $img
     */
    public function setPostPhoto($post, $img)
    {
        if($img['tmp_name'] == '') {
            return false;
        }
        $postId = $post->getId();
        $this->postFiles->removePostFileByPostId($postId);
        $postTitle = $post->getTitle();
        $img['title'] = $postTitle;
        $img['slug']  = \Base\Filter\Inflector\Slug::slugToLower($postTitle, '-');
        $pathPost = $this->getPathMedia().'/posts/'.$postId.'/';
        
        $img['name'] = $this->uploadFile($pathPost, $img, $img['slug'].'_'.rand(99,999));
        if($img['name'] != '') {
            $img['nameDir'] = '';
            $postFile = new PostFile($img);
            $postFile->setPost($post);
            $this->em->persist($postFile);
        }
    }
    
    /**
     * Save post categories
     * @param type $data
     */
    public function setPostCategories($post, $data)
    {    
        foreach ($data['category_id'] as $categoryId) {
            if ($categoryId < 1) {
                continue;
            }
            $category = $this->em->getReference('Front\Entity\PostCategory', $categoryId);
            
            $postCategory = new \Front\Entity\PostCategoriesHasPosts();
            $postCategory->setCategory($category)
                    ->setPost($post);
            
            if(!$post->getPostCategories()->contains($postCategory)) {
                $this->em->persist($postCategory);
            }
            
        }
    }
    
    /**
     * Save post category
     * @param type $data
     */
    public function setPostCategory($post, $categoryId)
    {
        $postCategoryCurrent = $post->getPostCategories()->first();
        if($postCategoryCurrent) {
            $postCategoryEntity = $this->em->getReference('Front\Entity\PostCategoriesHasPosts', $postCategoryCurrent->getId()); 
            
            $this->em->remove($postCategoryEntity);
            $this->em->flush();
        }
        
        if($categoryId > 0) {
            $category = $this->em->getReference('Front\Entity\PostCategory', $categoryId);
            $postCategory = new \Front\Entity\PostCategoriesHasPosts();
            $postCategory->setCategory($category)
                        ->setPost($post);

            $this->em->persist($postCategory);
        }
        
           
    }
    
    /**
     * Save tags post
     * @param type $post
     * @param type $data
     */
    public function setPostTags($post, $data)
    {
        foreach ($data['tag'] as $tagId) {
            $tag = $this->em->getReference('Front\Entity\PostCategory', $tagId);
            $postTag = new \Front\Entity\PostHasTag;
            $postTag->setCategory($tag)
                    ->setPost($post);
            $this->em->persist($postTag);
        }
    }
    
    public function checkSlug($slug, $id = 0)
    {
        $sqlSlug  = ' SELECT id';
        $sqlSlug .= ' FROM posts';
        $sqlSlug .= ' WHERE slug = "'.$slug.'"';
        if($id > 0) {
            $sqlSlug .= ' AND id = '.$id;
        }
        $sqlSlug .= ' ORDER BY  title ASC';
        
        $rs= $this->executeSql($sqlSlug);
        return isset($rs['id']);
    }
    
    public function getSlug($string, $id = 0)
    {
        $slug = \Base\Filter\Inflector\Slug::slugToLower($string, '-');
        $checkSlug = true;
        $i = 1;
        while($checkSlug) {
            if($this->checkSlug($slug, $id)) {
                $slug = $slug.'_'.(2 * rand(2,5)); 
            } else {
                $checkSlug = false;
            }       
        }
        
        return $slug;
    }
    
    public function validateInsert()
    {
        $validateClass = new \FrontManager\Validate\Post($this->em, $this->data);
        $validate = $validateClass->validateTitle();
        $this->data['slug']     = $this->getSlug($this->data['title']);
        $this->data['users_id'] = 1;
        
        return $validate;
    }
    
    /**
     * Save post
     * @param array $data
     * @param type $image
     */
    public function insert(array $data)
    {
        if(!$this->validateInsert()) {
            return false;
        }
        $data = $this->data;
        $post = parent::insert($data);
        
        $this->setPostPhoto($post, $this->data['image']);
        $this->em->flush();
    }
    
    public function validateUpdate()
    {
        $validate = true;
        $this->data['slug'] = $this->getSlug($this->data['title'], $this->data['id']);
        $this->data['users_id'] = 1;
        
        $validateClass = new \FrontManager\Validate\Post($this->em, $this->data);
        if(!$validateClass->validateData()) {
            $this->addMessages($validateClass->getMessages());
            $validate = false;
        }
        return $validate;
        
    }
    
    /**
     * Save post
     * @param array $data
     * @param type $image
     */
    public function update(array $data)
    {
        if(!$this->validateUpdate()) {
            return false;
        }
        $data = $this->data;
        $post = parent::update($data);
        
        $this->setPostPhoto($post, $data['image']);
        
        $this->em->flush();
        
        return $post;
    }
    
    
    
    
    public function delete($id)
    {
        $entity = $this->getRepository()->find($id);
        if($entity) {
            $postFiles = $entity->getPostFiles();
            foreach($postFiles as $postFile) {
                $this->postFiles->removePostFile($postFile);
                $this->postFiles->removePostFolder($postFile);
            }
            $this->em->remove($entity);
            $this->em->flush();
            return $id;
        }
        
        return 0;
    }
}
