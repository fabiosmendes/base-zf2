<?php

namespace FrontManager\Service;

use Doctrine\ORM\EntityManager;
use Base\Service\AbstractService;

class ContentArea extends AbstractService
{
    /**
     * 
     * @param \Doctrine\ORM\EntityManager $em
     * @param type $fileTransfer
     */
    public function __construct(EntityManager $em)
    {
        parent::__construct($em);
        $this->entity = "Front\Entity\ContentArea";
    }
    
    public function getList($pars = array(), $limit = 10)
    {
        $dql  = " SELECT p FROM ".$this->entity." p ";
        $dql .= " WHERE p.id > 0";
            
        if(isset($pars['title']) && $pars['title'] != '') {
            $dql .= " AND p.title LIKE '%".$pars['title']."%'";  
        }
        
        $dql .= ' ORDER BY p.id DESC';
        $query = $this->em->createQuery($dql);
        
        $lista = $query->getResult();
        return $lista;
    }
    
    public function getCombo()
    {
        $list = $this->getRepository()->findBy(array(), array('name' => 'ASC'));
        $pars = array();
        foreach($list as $item) {
            $pars[$item->getSlug()] = $item->getName();
        }
        
        return $pars;
    }
}
