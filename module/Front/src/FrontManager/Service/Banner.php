<?php

namespace FrontManager\Service;

use Doctrine\ORM\EntityManager;
use Base\Service\AbstractService;

class Banner extends AbstractService
{
    /**
     * 
     * @param \Doctrine\ORM\EntityManager $em
     * @param type $fileTransfer
     */
    public function __construct(EntityManager $em, $fileTransfer)
    {
        parent::__construct($em);
        $this->fileTransfer = $fileTransfer;
        $this->entity = "Front\Entity\Banner";
    }
    
    public function getList($pars = array(), $limit = 10)
    {
        $dql  = " SELECT p FROM ".$this->entity." p ";
        $dql .= " WHERE p.id > 0";
            
        if(isset($pars['title']) && $pars['title'] != '') {
            $dql .= " AND p.title LIKE '%".$pars['title']."%'";  
        }
        
        $dql .= ' ORDER BY p.id DESC';
        $query = $this->em->createQuery($dql);
        
        $lista = $query->getResult();
        return $lista;
    }
    
    public function getPage($id)
    {
        $page = $this->getRepository()->find($id);
        return $page;
    }
    
    public function update(array $data)
    {
        $photo = $data['image'];
        unset($data['image']);
        
        $entity = parent::update($data);
        
        $image = $this->uploadFile( $this->getPathMedia() . '/banners/', $photo, 
                                    'banner_'.$entity->getId().'-'.rand(99,999));
        if($image != '') {
            $entity->setImage($image);
        }
        
        $this->em->persist($entity);
        $this->em->flush();
        return $entity;
    }
    
    public function insert(array $data)
    {
        $photo = $data['image'];
        unset($data['image']);
        
        $entity = parent::insert($data);
        
        $image = $this->uploadFile( $this->getPathMedia() . '/banners/', $photo, 
                                    'banner_'.$entity->getId().'-'.rand(99,999));
        if($image != '') {
            $entity->setImage($image);
        }
        $this->em->persist($entity);
        $this->em->flush();
        
        return $entity;
    }
    
    public function delete($id)
    {
        $entity = $this->getRepository()->find($id);
        if($entity) {
            $imagePath = $entity->getImage();
            if($imagePath != '') {
                $pathFile = $this->getPathMedia() . '/banners/' . $imagePath;
                $this->removeFile($pathFile);
            }
            
            $this->em->remove($entity);
            $this->em->flush();
            return $id;
        }
        return 0;
    }
}
