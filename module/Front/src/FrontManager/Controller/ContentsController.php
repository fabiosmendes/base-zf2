<?php

namespace FrontManager\Controller;

use Manager\Controller\ManagerController;

class ContentsController extends ManagerController
{
    public function __construct()
    {
        $this->service = 'FrontManager\Service\Content';
        $this->entity  = 'Front\Entity\Content';
        $this->form    = 'FrontManager\Form\Page\Content';
        $this->route      = 'general';
        $this->controller = 'manager-contents';
        
        $this->msgSuccessInsert = 'Conteúdo cadastrado com sucesso!';
        $this->msgSuccessUpdate = 'Conteúdo alterado com sucesso!';
        $this->msgSuccessDelete = 'Conteúdo excluído com sucesso!';
        
        parent::__construct();
    }
    
    public function editAction() 
    {
        $form     = $this->getServiceLocator()->get('FrontManager\Form\Content\Edit');
        $service  = $this->getServiceLocator()->get($this->service);
        $flashMessenger = $this->getFlashMessager();
        $request  = $this->getRequest();
        
        if($request->isPost()) {
            $postData = $request->getPost()->toArray();
            $form->setData($postData);
            if($form->isValid()) {
                $postData['image']  = $this->params()->fromFiles('image'); // pegar arquivo no request
                $service->update($postData);
                $flashMessenger->addMessage($this->msgSuccessUpdate);
                return $this->redirect()->toRoute($this->route, array('controller' => $this->controller));
            } 
        }
        else {
            $repository  = $this->getEm()->getRepository($this->entity);
            $id          = $this->params()->fromRoute('id', 0);
            $entity      = $repository->find($id);

            if(null == $entity) {
                return $this->redirect()->toRoute($this->route, array('controller' => $this->controller));
            } else { 
                $form->setData($entity->toArray());
            }  
        }
        
        return $this->renderView(array('form' => $form));
    }
    
    public function newAction() 
    {
        $form = $this->getServiceLocator()->get('FrontManager\Form\Content\Add');
        $flashMessenger = $this->getFlashMessager();
        $request = $this->getRequest();
        
        if($request->isPost()) {
            $postData = $request->getPost()->toArray();
            $form->setData($postData);
            if($form->isValid()) {
                $postData['image']  = $this->params()->fromFiles('image');
                $service = $this->getServiceLocator()->get($this->service);
                $service->insert($postData);
                
                $flashMessenger->addMessage($this->msgSuccessInsert);
                return $this->redirect()->toRoute($this->route, array('controller' => $this->controller));
            }
        }
        
        return $this->renderView(array('form' => $form));
    }
    
    public function deleteAction() 
    {
        $service = $this->getServiceLocator()->get($this->service);
        $flashMessenger = $this->getFlashMessager();
        $id      = $this->params()->fromRoute('id', 0);
        
        if($service->delete($id)) {
            $flashMessenger->addMessage($this->msgSuccessDelete);
        } else {
            $messages = $service->getMessages();
            $flashMessenger->addMessage($messages[0]);
        }
        
        return $this->redirect()->toRoute($this->route, array('controller' => $this->controller));
            
    }
    
    public function getMessage($key)
    {
        return isset($this->messages[$key]) ? $this->messages[$key] : self::GENERIC_MESSAGE; 
    }
}
