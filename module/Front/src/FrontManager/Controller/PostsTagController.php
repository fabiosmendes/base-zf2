<?php

namespace FrontManager\Controller;

use BaseAdmin\Controller\CrudController;
use Manager\Form\Post\AddPostTag as FrmAddPostTag;

class PostsTagController extends CrudController
{

    public function indexAction()
    {
        $frmAddPost = new FrmAddPostTag();
        $request = $this->getRequest();
        $msg = '';
        $status = false;

        if ($request->isPost()) {
            $postData = $request->getPost()->toArray();
            $frmAddPost->setData($postData);
            if ($frmAddPost->isValid()) {

                //Falta o doctrine

                $msg .= 'Dados da tag foram salvos com suscesso!';
                $status = true;

                $this->redirect()->toRoute('posts-tag');
            } else {
                $msg .= 'Não foi possível cadastrar a tag. Tente novamente.';
            }
        }

//        $em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
//        $repo = $em->getRepository('Manager\Entity\PostCategory');
//
//        $postsTags = $repo->findAll();

        $pars = array('form' => $frmAddPost, 'msg' => $msg, 'status' => $status, /* 'postsCategories' => $postsTags */);
        return $this->renderView($pars);
    }

    public function addAction()
    {
        $pars = array();
        return $this->renderView($pars);
    }

    public function editAction()
    {
        $pars = array();
        return $this->renderView($pars);
    }

    public function deleteAction()
    {
        $pars = array();
        return $this->renderView($pars);
    }

}
