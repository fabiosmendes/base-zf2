<?php

namespace FrontManager\Controller;

use BaseAdmin\Controller\CrudController;

class PostsCategoryController extends CrudController
{
    public function __construct()
    {
        $this->service = 'FrontManager\Service\PostCategory';
        $this->entity  = 'Front\Entity\PostCategory';
        $this->route      = 'general';
        $this->controller = 'manager-posts-category';
    }
    
    public function indexAction() 
    {   
        $service  = $this->getServiceLocator()->get($this->service);
        $searchPars = $this->params()->fromQuery();
        $list     = $service->getList($searchPars);
        
        $page = $this->params()->fromRoute('pager');    
        $paginator = new Paginator(new ArrayAdapter($list));
        $paginator->setCurrentPageNumber($page);
        $paginator->setDefaultItemCountPerPage(50);
        
        $flashMessenger = $this->getServiceLocator()->get('ServiceFlashMessenger');
        
        $message = '';
        if ($flashMessenger->hasMessages()) {
            $messages = $flashMessenger->getMessages(); // segmentar mensagens (erro, por exemplo e success)
            $message = $messages[0];   
        }
        
        return $this->renderView(array('data' => $paginator, 
                                       'page' => $page, 
                                       'message'   => $message, 
                                       'queryPars' => json_encode($searchPars)));
    }
    
    public function newAction()
    {    
        $frmAddPost = $this->getServiceLocator()->get('FrontManager\Form\Product\AddPostCategory');
        $service  = $this->getServiceLocator()->get($this->service);
        $flashMessenger = $this->getServiceLocator()->get('ServiceFlashMessenger');
        $request = $this->getRequest();
        
        $msg = '';
        $status = false;
        if ($request->isPost()) {
            $postData = $request->getPost()->toArray();
            $frmAddPost->setData($postData);
            if ($frmAddPost->isValid()) {
                $service = $this->getServiceLocator()->get('FrontManager\Service\ProductCategory');
                $postData['image'] = $this->params()->fromFiles('image');
                $service->insert($postData);
                
                $flashMessenger->addMessage('Categoria salva com sucesso!');
                $this->redirect()->toRoute('painel', array('controller' => $this->controller));
            }
        }
        $pars = array('form' => $frmAddPost);
        return $this->renderView($pars);
    }

    public function editAction()
    {
        $formEditPost = $this->getServiceLocator()->get('FrontManager\Form\Product\EditProductCategory');
        $service  = $this->getServiceLocator()->get($this->service);
        $flashMessenger = $this->getServiceLocator()->get('ServiceFlashMessenger');
        $request = $this->getRequest();
        
        if($request->isPost()) {
            $postData = $request->getPost()->toArray();
            $formEditPost->setData($postData);
            if($formEditPost->isValid()) {
                $postData['image'] = $this->params()->fromFiles('image');
                $service->update($postData); // # get form->getData()
                $flashMessenger->addMessage('Categoria alterada com sucesso!');
                return $this->redirect()->toRoute($this->route, array('controller' => $this->controller));
            } 
        }
        else {
            $repository  = $this->getEm()->getRepository($this->entity);
            $id          = $this->params()->fromRoute('id', 0);
            $entity      = $repository->find($id);

            if(null == $entity) {
                return $this->redirect()->toRoute($this->route, array('controller' => $this->controller));
            }
            else { 
                $dados = $entity->toArray();
                $formEditPost->setData($dados);
            }  
        }
        
        return $this->renderView(array('form' => $formEditPost));
    }

    public function deleteAction() 
    {
        $service = $this->getServiceLocator()->get($this->service);
        $flashMessenger = $this->getServiceLocator()->get('ServiceFlashMessenger');
        $id       = $this->params()->fromRoute('id', 0);
        $category = $service->getCategory($id);
        
        if($service->delete($id)) {
            $flashMessenger->addMessage('Categoria "'.$category->getTitle().'" excluída!');
        } else {
            $flashMessenger->addMessage('Categoria já excluída!');
        }
        
        return $this->redirect()->toRoute($this->route, array('controller' => $this->controller));
            
    }

    public function addAjaxAction()
    {

        $frmAddPost = new FrmAddPostCategory();
        $request = $this->getRequest();
        $msg = '';
        $status = false;

        if ($request->isPost()) {
            $postData = $request->getPost()->toArray();
            $frmAddPost->setData($postData);
            if ($frmAddPost->isValid()) {
                $service = $this->getServiceLocator()->get('FrontManager\Service\PostCategory');
                $service->insert($postData);
                $msg .= 'Dados da categoria foram salvos com suscesso!';
                $status = true;
            } else {
                $msg .= 'Não foi possível cadastrar a categoria. Tente novamente.';
            }
        }

        exit();
    }

    public function getAllCategoriesByAjaxAction()
    {
        $service = $this->getServiceLocator()->get('FrontManager\Service\PostCategory');
        return new \Zend\View\Model\JsonModel($service->getCategories());
    }

}
