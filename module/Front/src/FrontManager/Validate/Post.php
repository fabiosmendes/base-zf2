<?php

namespace FrontManager\Validate;

use Doctrine\ORM\EntityManager;

class Post
{
    /**
     *
     * @var EntityManager
     */
    protected $em;
    
    /**
     * @var array 
     */
    protected $data;
    
    /**
     *
     * @var array 
     */
    protected $messages = array();
    
    public function __construct(EntityManager $em, array $data)
    {
        $this->em   = $em;
        $this->data = $data;
    }
    
    public function setData(array $data)
    {
        $this->data = $data;
    }
    
    public function validateTitle()
    {
        $validate = true;
        $title = $this->data['title'];
        if($title == 'post') {
            $this->messages['title']['titleEmpty'] = 'Informe um título diferente o post';
            $validate = false;
        }
        return $validate;
    }
    
    public function validateCategory()
    {
        $validate = true;
        $categoryId = $this->data['category_id'];
        if($categoryId == '') {
            $this->messages['category_id']['titleEmpty'] = 'Informe uma categoria para o post';
            $validate = false;
        }
        return $validate;
    }
    
    public function validateData()
    {
        return ($this->validateTitle()); // &&  $this->validateCategory());
    }
    
    public function getMessages()
    {
        return $this->messages;
    }
}