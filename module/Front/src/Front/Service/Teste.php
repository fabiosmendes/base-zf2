<?php

namespace Front\Service;

use Doctrine\ORM\EntityManager;
use Zend\Mail\Transport\Smtp as SmtpTransport;

use Base\Service\AbstractService;
use Base\Mail\Mail;

class Teste extends AbstractService
{
    public function __construct(EntityManager $em)
    {
        parent::__construct($em);

        $this->entity    = "Front\Entity\Contato";
    }
    
    public function getLista()
    {
        return array(
            1,2,3,4,5,6
        );
    }

}
