<?php

namespace Front\Pages;

use Zend\View\Model\ViewModel;

class ControlContent
{
    protected $serviceManager;
    protected $em;
    protected $itemUri;
    protected $translator;
    protected $view;
    
    protected $pathFileSystem;
    protected $contentData;
    protected $checkStatusPage = false;  // db ou cache
    protected $checkStatusPageFile = false; // static
    
    /**
     *
     * @var array 
     */
    protected $data = array();
    
    public function __construct($serviceManager, $itemUri, $defaultModule = 'Front')
    {
        $this->serviceManager = $serviceManager;
        $this->itemUri = $itemUri;
        
        $this->em = $this->serviceManager->get('Doctrine\ORM\EntityManager');
        $this->translator = $this->serviceManager->get('MvcTranslator');
        $this->view = $this->serviceManager->get('ViewRenderer');
        
        $this->pathFileSystem = './module/'.$defaultModule.'/view/front/pages/%s.phtml';
    }
    
    public function checkPageFileSystem()
    {
        $this->checkStatusPageFile = false;
        if(is_file(sprintf($this->pathFileSystem, $this->itemUri))) {
            $this->checkStatusPageFile = true;
        }
        
        return $this->checkStatusPageFile;
    }
    
    public function checkContentDB()
    {
        $this->itemUri; // @todo - search db content, com itemUri - partes se necessário (categoria - conteudo)
        $this->contentData = false;//array('content' => 'conteudo db');//
        $postService = $this->serviceManager->get('Front\Service\Post');
        
        if(strstr($this->itemUri, '/') != '') {
            $partsUri = explode('/', $this->itemUri);
            $categorySlug = $partsUri[0];
            $postSlug     = $partsUri[1];
            
            $post = $postService->getPostCategory($postSlug, $categorySlug);    
        } else {
            $post = $postService->getPost($this->itemUri);
        }
        
        $this->contentData = $post; // @todo - retorno da busca
        return $this->contentData;
    }
    
    public function setData($data)
    {
        $this->data = $data;
    }
    
    public function getFinalTemplate($model)
    {
        $modelLayout = new ViewModel();
        $modelLayout->setTemplate('layout/layout');
        $modelLayout->setVariable('content', $this->view->render($model));
            
        return $this->view->render($modelLayout);
    }
    
    public function getContent()
    {
        if($this->checkPageFileSystem()) {
            $model = new ViewModel();
            $model->setTemplate("front/pages/{$this->itemUri}");
            $model->setOption('has_parent', true);
            $model->setVariables($this->data);
            
            return $this->getFinalTemplate($model);
        } else if($this->checkContentDB()) {
            $model = new ViewModel();
            $model->setTemplate("front/pages/page.phtml");
            $model->setOption('has_parent',true);
            $modelDefault = new ViewModel();
            $modelDefault->setVariable('postData', array('chave' => 'valor'));
            $modelDefault->setTemplate("front/pages/default.phtml");
            
            $model->setVariable('data', $this->view->render($modelDefault));
            
            return $this->view->render($model);
        }
    }
    
    public function get404()
    {
        $model = new ViewModel();
        $model->setTemplate("error/404.phtml");
        $model->setOption('has_parent',true);
        //$model->setVariable('data', 'conteudo db');
            
        return $this->view->render($model);
    }
}

