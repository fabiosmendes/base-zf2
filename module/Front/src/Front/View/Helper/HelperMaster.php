<?php

namespace Front\View\Helper;

use Zend\View\Helper\AbstractHelper;

class HelperMaster extends AbstractHelper
{
    public function __invoke($name)
    {
        $nameHello = "Hello ".$name;
        
        $template  = 'front/helpers/hello';
        $pars = array('name' => $nameHello);
        
        return $this->getView()->render($template, $pars);
    }

}
