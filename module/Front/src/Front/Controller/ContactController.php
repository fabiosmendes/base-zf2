<?php

namespace Front\Controller;

use Base\Controller\BaseController;
use Front\Form\Contact as FrmContact;

class ContactController extends BaseController
{
    /**
     * Const Name Service Contact  
     */
    const SERVICE_CONTACT = "Front\Service\Contact";
    
    /**
     * Const Route Default
     */
    const ROUTE_INDEX_CONTACT = "front-contact";
    
    /**
     * Messages
     */
    const MSG_SUCCESS = "Contato enviado com sucesso!";
    
    
    /**
     *
     * @var response|null 
     */
    protected $redirect = null;
    
    public function indexAction()
    {
        $pathImg    = $this->getRequest()->getBaseUrl().'/images/captcha/';
        $frmContact = new FrmContact($pathImg);
        $request    = $this->getRequest();
        
        if($request->isPost()) {
            $this->executeActionsPost($frmContact, $request);
            
            if(null !== $this->redirect) {
                return $this->redirect;
            }
        }
        
        $pars = array('form'   => $frmContact);
        return $this->renderView($pars);
    }    
    
    public function captchaAction()
    {
        $response = $this->getResponse();
        $response->getHeaders()->addHeaderLine('Content-Type', "image/png");
 
        $id = $this->params('id', false);
 
        if($id) {
            $image = './data/captcha/' . $id;
 
            if (file_exists($image) !== false) {
                $imagegetcontent = file_get_contents($image);
 
                $response->setStatusCode(200);
                $response->setContent($imagegetcontent);
                unlink($image);
            }
        }
 
        return $response;
    }
    
    /**
     * 
     * @param type $frmContact
     * @param type $request
     */
    private function executeActionsPost($frmContact, $request)
    {
        $postData = $request->getPost()->toArray();
        $frmContact->setData($postData);
        
        if($frmContact->isValid()) {
            $this->sendEmail($postData);
            $this->flashMessenger()->addSuccessMessage(self::MSG_SUCCESS);
            $this->redirect = $this->redirect()->toRoute(self::ROUTE_INDEX_CONTACT);
        }
        
        //echo __LINE__.'<br/>'; echo "<pre>";var_dump($frmContact->getMessages()); exit();
    }
    
    /**
     * 
     * @param type $postData
     */
    private function sendEmail($postData)
    {
        $contactService = $this->getServiceLocator()->get(self::SERVICE_CONTACT);
        $contactService->sendEmail($postData);
    }
}
