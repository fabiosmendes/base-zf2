<?php

namespace Front\Entity;

use Doctrine\ORM\Mapping as ORM;
use Zend\Stdlib\Hydrator;

/**
 * Content
 *
 * @ORM\Table(name="contents")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 * @ORM\Entity(repositoryClass="Front\Entity\Repository\ContentRepository")
 */
class Content
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;
    
    /**
     * @var string
     *
     * @ORM\Column(name="area", type="string", length=45, nullable=true)
     */
    private $area;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="language_id", type="integer", nullable=true)
     */
    private $languageId;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=true)
     */
    private $title;
 
    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=255, nullable=true)
     */
    private $slug;
    
    /**
     * @var string
     *
     * @ORM\Column(name="text", type="text", nullable=true)
     */
    private $text;

    /**
     * @var string
     *
     * @ORM\Column(name="link", type="string", length=255, nullable=true, options={"comment" = "Incluir se o conteúdo levar para algum link"})
     */
    private $link;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime", nullable=false)
     */
    private $modified;

    public function __construct(array $options = array())
    {
        $hydrator = new Hydrator\ClassMethods();
        $hydrator->hydrate($options, $this);

        $this->created  = new \DateTime("now");
        $this->modified = new \DateTime("now");
    }

    public function getId()
    {
        return $this->id;
    }

    public function getArea()
    {
        return $this->area;
    }

    public function getLanguageId()
    {
        return $this->languageId;
    }   

    public function getTitle()
    {
        return $this->title;
    }
    
    public function getText()
    {
        return $this->text;
    }

    public function getLink()
    {
        return $this->link;
    }

    public function getCreated()
    {
        return $this->created;
    }

    public function getModified()
    {
        return $this->modified;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function setArea($area)
    {
        $this->area = $area;
    }

    public function setLanguageId($languageId)
    {
        $this->languageId = $languageId;
    }
    
    public function setTitle($title)
    {
        $this->title = $title;
    }

    public function setText($text)
    {
        $this->text = $text;
    }

    public function setLink($link)
    {
        $this->link = $link;
    }

    public function setCreated()
    {
        $this->created = new \DateTime("now");
        return $this;
    }
    
    /**
     * @ORM\PreUpdate
     */
    public function setModified()
    {
        $this->modified = new \DateTime("now");
        return $this;
    }
    
    public function toArray()
    {
        $hydrator = new Hydrator\ClassMethods();
        return $hydrator->extract($this);
    }
    
    public function __toString()
    {
        return $this->title;
    }
}
