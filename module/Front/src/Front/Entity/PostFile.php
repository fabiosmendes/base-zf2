<?php

namespace Front\Entity;

use Doctrine\ORM\Mapping as ORM;
use Zend\Stdlib\Hydrator;

/**
 * PostFiles
 *
 * @ORM\Table(name="post_files")
 * @ORM\Entity
 */
class PostFile
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=false)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=255, nullable=false)
     */
    private $slug;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="name_dir", type="string", length=255, nullable=false)
     */
    private $nameDir;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=50, nullable=false)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="size", type="string", length=45, nullable=false)
     */
    private $size;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime", nullable=false)
     */
    private $modified;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     */
    private $created;
    
    /**
     * @ORM\ManyToOne(targetEntity="Front\Entity\Post", inversedBy="files")
     * @ORM\JoinColumn(name="posts_id", referencedColumnName="id")
     */
    private $post;
    
    public function __construct(array $options = array()) 
    {
        $hydrator = new Hydrator\ClassMethods();
        $hydrator->hydrate($options, $this);
        
        $this->created  = new \DateTime("now");      
        $this->modified = new \DateTime("now"); 
    }
    
    public function getPost()
    {
        return $this->post;
    }
    
    public function setPost(Post $post)
    {
        $this->post = $post;
        return $this;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function getSlug()
    {
        return $this->slug;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getNameDir()
    {
        return $this->nameDir;
    }

    public function getType()
    {
        return $this->type;
    }

    public function getSize()
    {
        return $this->size;
    }

    public function getModified()
    {
        return $this->modified;
    }

    public function getCreated()
    {
        return $this->created;
    }
    
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    public function setSlug($slug)
    {
        $this->slug = $slug;
        return $this;
    }

    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    public function setNameDir($nameDir)
    {
        $this->nameDir = $nameDir;
        return $this;
    }

    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    public function setSize($size)
    {
        $this->size = $size;
        return $this;
    }

    public function setCreated()
    {
        $this->created = new \DateTime("now");
        return $this;
    }

    public function setModified()
    {
        $this->modified = new \DateTime("now");
        return $this;
    }

    public function toArray() 
    {
        $hydrator = new Hydrator\ClassMethods();
        return $hydrator->extract($this);
    }
}
