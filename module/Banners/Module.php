<?php

namespace Banners;

class Module
{
    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }
    
    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ . 'Manager' => __DIR__ . '/src/' . __NAMESPACE__ . 'Manager',
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }
    
    public function getServiceConfig()
    {
        return include __DIR__ . '/config/services.config.php';
    }
    
    public function getValidatorConfig()
    {
        return array(
            'factories' => array( 
            ),           
        );
    }
    
    public function getViewHelperConfig()
    {
        return array(
            'factories' => array(
            ),
            'invokables' => array(
                //'userIdentity' => new View\Helper\UserIdentity()
            )
        );
    }

}
