<?php

namespace Banners\Controller;

use Base\Controller\BaseController;

class BannersController extends BaseController
{
    protected $isValidData;
    
    public function indexAction()
    {
        $request = $this->getRequest();
        if($request->isPost()) {
            $postData = $request->getPost()->toArray();
            if( $this->processPost($postData) ) {
                return $this->redirect()->toRoute('general', array('controller' => 'budget'));
            }
        }
        
        $pars = array();
        return $this->renderView($pars);
    }

    public function processPost(array $postData)
    {
        if($this->validate($postData)) {
            return $this->sendToServiceLayer($postData);
        }
        return false;
    }
    
    public function validate(array $postData)
    {
        $this->isValidData = true;
        if(empty($postData['name'])) {
            $this->isValidData = false;
        } 
        if(empty($postData['email'])) {
            $this->isValidData = false;
        }
        
        return $this->isValidData;
    }
    
    public function sendToServiceLayer(array $postData) 
    {
        if($this->isValidData) {
            /**
             * @todo - execute service call and get status operation
             */
            return $this->isValidData;
        }
        return false;
    }
}