<?php

namespace BannersManager\Service;

use Doctrine\ORM\EntityManager;
use Base\Service\AbstractService;

class Banner extends AbstractService
{
    /**
     * 
     * @param \Doctrine\ORM\EntityManager $em
     * @param type $fileTransfer
     */
    public function __construct(EntityManager $em, $fileTransfer)
    {
        parent::__construct($em);
        $this->entity = "Banners\Entity\Banner";
        $this->fileTransfer = $fileTransfer;
    }
    
    
}
