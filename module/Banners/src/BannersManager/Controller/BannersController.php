<?php

namespace BannersManager\Controller;

use Manager\Controller\ManagerController;

class BannersController extends ManagerController 
{
    public function __construct()
    {
        $this->entity  = "Banners\Entity\Banner";
        $this->service = "BannersManager\Service\Banner";
        $this->form    = "BannersManager\Form\Banner";
        $this->controller = "manager-banners";
        $this->route      = "general";
        
        parent::__construct();
    }
}
