<?php

namespace BannerManager\Form;

use Zend\Form\Form;

class Banner extends Form
{
    public function __construct($name = null)
    {
        parent::__construct('banner');
        
        $this->setAttribute('method', 'post');
        $this->setInputFilter(new BannerFilter());
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Hidden',
            'name' => 'id',
            'attributes' => array(
                'id' => 'id',
            )
        ));
        
        $this->add(array(
            'name' => 'title',
            'options' => array(
                'label' => 'Título'
            ),
            'attributes' => array(
                'id' => 'name',
                'placeholder' => 'Digite um título para o banner',
                'class' => 'cx-form1',
                'title' => '',
            )
        ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Textarea',
            'name' => 'description',
            'options' => array(
                'label' => 'Descrição',
            ),
            'attributes' => array(
                'id' => 'description',
                'placeholder' => 'Digite uma descrição (opcional)',
                'class' => 'cx-form2',
                'title' => '',
            )
        ));
    }
}
