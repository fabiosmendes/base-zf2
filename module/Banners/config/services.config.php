<?php

namespace Banners;

return array(
    'factories' => array(
        'Banners\Service\Banner' => function($sm) {
            return new \Banners\Service\Banner($sm->get('Doctrine\ORM\EntityManager'));
        },        
        'BannersManager\Service\Banner' => function($sm) {
            return new \BannersManager\Service\Banner($sm->get('Doctrine\ORM\EntityManager'),
                                                      $sm->get('Base\Http\FileTransfer'));
        },        
    ),          
                
);