<?php
 
namespace Front\Form\Modelo;

use Front\Entity\Categoria;
use Zend\Form\Fieldset;

class CategoriaFieldset extends Fieldset
{
    public function __construct()
    {
        parent::__construct('categoria');
        
        $this->setLabel('Categoria');
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'id',
            'options' => array(
                'label' => 'Categoria',
                'empty_option' => ' -- ',
                'value_options' => array(
                    '1' => 'Categoria 1',
                    '2' => 'Categoria 2',
                    '3' => 'Categoria 3',
                    '4' => 'Categoria 4',
                ),
            ),
            'attributes' => array(
                'value' => '1' //set selected to '1'
            )
        ));
    }
}
