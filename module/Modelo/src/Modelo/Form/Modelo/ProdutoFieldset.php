<?php
 
namespace Front\Form\Modelo;

use Zend\Form\Fieldset;

class ProdutoFieldset extends Fieldset
{
    public function __construct()
    {
        parent::__construct('produto');
        
        $this->setLabel('Produto');
        
        $this->add(array(
            'name' => 'id',
            'type' => 'Zend\Form\Element\Hidden',
        ));
        
        $this->add(array(
            'name' => 'nome',
            'options' => array(
                'type' => 'text',
                'label' => 'Nome do Produto'
            ),
            'attributes' => array(
                'id' => 'nome',
                'placeholder' => 'Entre com o nome',
                'class' => '',
            )
        ));
        
        $this->add(array(
            'name' => 'codigo',
            'options' => array(
                'type' => 'text',
                'label' => 'Código do Produto'
            ),
            'attributes' => array(
                'id' => 'nome',
                'placeholder' => 'Entre com o código',
                'class' => '',
            )
        ));
        
        $this->add(array(
             'type' => 'Zend\Form\Element\Collection',
             'name' => 'categorias',
             'options' => array(
                 'label' => 'Please choose categories for this product',
                 'count' => 2,
                 'should_create_template' => true,
                 'allow_add' => true,
                 'fieldset' => true,
                 'target_element' => array(
                     'type' => 'Front\Form\Modelo\CategoriaFieldset',
                 ),
             ),
         ));
    }

}
