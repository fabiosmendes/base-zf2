<?php

namespace Front\Form;

use Zend\InputFilter\InputFilter;

class ContatoFilter extends InputFilter {

    public function __construct() {
        $this->add(array(
            'name' => 'nome',
            'required' => true,
            'filters' => array(
                array('name' => 'StripTags'),
                array('name' => 'StringTrim'),
             ),
            'validators' => array(
                array(
                    'name' => 'NotEmpty',
                    'options' => array(
                        'messages' => array('isEmpty' => 'Nome não pode estar em branco.')
                    )
                )
            )
        ));
        
        $this->add(
            array(
                'name' => 'email',
                'required' => true,
                'filters' => array(
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'NotEmpty',
                        'options' => array(
                            'messages' => array('isEmpty' => 'E-mail não pode estar vazio.'),
                        ),
                    ),
                    array(
                        'name' => 'EmailAddress',
                    ),
                )
            )   
        );
        
        $this->add(
            array(
                'name' => 'telefone',
                'required' => true,
                'filters' => array(
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'NotEmpty',
                        'options' => array(
                            'messages' => array('isEmpty' => 'Telefone não pode estar vazio.'),
                        ),
                    ),
                )
            )   
        );
        
        $this->add(
            array(
                'name' => 'mensagem',
                'required' => true,
                'filters' => array(
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'NotEmpty',
                        'options' => array(
                            'messages' => array('isEmpty' => 'Preencha o campo mensagem.'),
                        ),
                    ),
                )
            )   
        );
    }
    
}
