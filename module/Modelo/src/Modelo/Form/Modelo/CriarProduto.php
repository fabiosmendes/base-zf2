<?php

namespace Front\Form\Modelo;

use Zend\Form\Form;
use Zend\Captcha;
use Zend\Stdlib\Hydrator\ClassMethods as ClassMethodsHydrator;

class CriarProduto extends Form
{
    public function __construct()
    {
        parent::__construct('contato');
        
        $this->setAttribute('method', 'post');
        
        $this->add(array(
             'type' => 'Front\Form\Modelo\ProdutoFieldset',
             'name' => 'produto',
             'options' => array(
                 'use_as_base_fieldset' => true,
             ),
        ));
        
        $this->add(array(
            'name' => 'submit',
            'type' => 'Zend\Form\Element\Submit',
            'attributes' => array(
                'value' => 'Salvar',
                'class' => ''
            )
        ));
    }

}
