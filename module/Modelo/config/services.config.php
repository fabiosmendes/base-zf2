<?php

namespace Modelo;

use Modelo\Auth\Adapter as AuthAdapter;

return array(
    'factories' => array(
        'Modelo\Auth\Adapter' => function($sm) {
            return new AuthAdapter($sm->get('Doctrine\ORM\EntityManager'));
        },       
    ),
);