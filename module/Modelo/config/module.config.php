<?php

namespace Modelo;

return array(
    'router' => array(
        'model-contact' => array(
            'type'    => 'Zend\Mvc\Router\Http\Segment',
            'options' => array(
                'route'    => '/contact[/:action]',
                'constraints' => array(
                    'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                ),
                'defaults' => array(
                    'controller'    => 'contact',
                    'action'        => 'index',
                ),
            ),
        ),
        // config de uma rota para uma página estática (para sobre nós, quem somos, perguntas e etc)
        'front-about-us' => array(
            'type' => 'Zend\Mvc\Router\Http\Literal',
            'options' => array(
                'route' => '/about-us', // acesso na url
                'defaults' => array(
                    'controller' => 'pages',
                    'action'     => 'about-us',
                ),
            ),
        ),
    ),
    'controllers' => array(
        'invokables' => array(
            'distributor-auth' => __NAMESPACE__. '\Controller\AuthController',
            'distributor-area' => __NAMESPACE__. '\Controller\AreaController',       
        ),
    ),
    'view_manager' => array(
        'template_map' => array(
            'modelo/index/index' => __DIR__ . '/../view/modelo/index/index.phtml',
        ),
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
        'strategies' => array(
            'ViewJsonStrategy',
        )
    ),
    'doctrine' => array(
        'driver' => array(
            __NAMESPACE__ . '_driver' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(__DIR__ . '/../src/' . __NAMESPACE__ . '/Entity')
            ),
            'orm_default' => array(
                'drivers' => array(
                    __NAMESPACE__ . '\Entity' => __NAMESPACE__ . '_driver'
                )
            )
        ),
    ),
    'data-fixture' => array(
        'Modelo_fixture' => __DIR__ . '/../src/Modelo/Fixture',
    ),
);
