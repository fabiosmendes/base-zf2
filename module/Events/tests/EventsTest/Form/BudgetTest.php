<?php

namespace EventsTest\Form;

use Base\Form\FormTestCase;
use EventsTest\Assets\BudgetData;

/**
 * @group Forms
 */
class BudgetTest extends FormTestCase
{
    const CLASS_FORM = 'Events\Form\Budget';
    const EXTEND_CLASS = 'Zend\Form\Form';
            
    public function setUp()
    {
        parent::setUp();
    }
    
    public function testExistsClass()
    {
        $class = class_exists(self::CLASS_FORM);
        $this->assertTrue($class);
    }
    
    /**
     * @depends testExistsClass
     */
    public function testIfClassFormExtendsForm()
    {
        $object  = $this->getClassForm();
        $this->assertTrue(is_subclass_of($object, self::EXTEND_CLASS));
    }
    
    public function formFields() 
    {
        return array(
            array('name'),
            array('email'),
            array('nickname'),
        );
    }
    
    /** 
     * @depends testIfClassFormExtendsForm
     * @dataProvider formFields()
     */
    public function testIfFormHasDefinedFields($fieldName) 
    {
        $form  = $this->getClassForm();
        $this->assertTrue($form->has($fieldName), 'Form field "'.$fieldName.'" not found.');           
    }
    
    /**
     * Test if the attributes that exist in form and are set in testing. 
     * @depends testIfClassFormExtendsForm
     */
    public function testIfAttributesAreMirrored()
    {
        $dataProviderTest = $this->formFields();
        $attributesDefined = array();
        foreach($dataProviderTest as $item) {
            $attributesDefined[] = $item[0];
        }
        
        $form = $this->getClassForm();
        $attributesFormClass = $form->getElements();
        $attributesForm = array();
        foreach($attributesFormClass as $key => $value) {
            $attributesForm[] = $key;
            $this->assertContains($key, $attributesDefined, 'Attribute "'.$key.'" not found in test class.');
        }
        
        $this->assertTrue(($attributesDefined === $attributesForm), 'There are no attributes sets.');        
    }
    
    public function testIfCompleteDataAreValid()
    { 
        $data = (new BudgetData)->getValidPost();
        
        $form  = $this->getClassForm();
        $form->setData($data);
        $this->assertTrue($form->isValid());
    }
    
    public function testIfEmptyFieldNameIsInvalid()
    { 
        $fieldName = 'name';
        $data = (new BudgetData)->getCompleteData();
        $data[$fieldName] = '';
        
        $form  = $this->getClassForm();
        $form->setData($data);
        $this->assertFalse($form->isValid());
        $this->assertTrue(count($form->get($fieldName)->getMessages()) > 0);
    }
    
    public function testIfEmptyFieldEmailIsEmpty()
    { 
        $fieldName = 'email';
        $data = (new BudgetData)->getCompleteData();
        $data[$fieldName] = '';
        
        $form  = $this->getClassForm();
        $form->setData($data);
        $this->assertFalse($form->isValid());
        $this->assertTrue(count($form->get($fieldName)->getMessages()) > 0);
    }
    
    public function testIfEmptyFieldEmailIsInvalid()
    { 
        $fieldName = 'email';
        $data = (new BudgetData)->getCompleteData();
        $data[$fieldName] = 'fbiosm@gmail.';
        
        $form  = $this->getClassForm();
        $form->setData($data);
        $this->assertFalse($form->isValid());
        $this->assertTrue(count($form->get($fieldName)->getMessages()) > 0);
    }
    
    protected function getClassForm()
    {
        $classForm = self::CLASS_FORM;
        return new $classForm;
    }
}