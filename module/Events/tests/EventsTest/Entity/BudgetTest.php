<?php

namespace EventsTest\Entity;

use Base\Entity\EntityTestCase;
use Events\Entity\Budget;

class BudgetTest extends EntityTestCase
{
    public function setUp()
    {
        $this->entity = '\\Events\\Entity\\Budget';
        parent::setUp();
    }
    
    public function dataProviderAttributes()
    {
        return array(
            array('id', 1),
            array('code', '25354512'),
            array('name', 'John Lennon'),
            array('email', 'john.beatles@gmail.com'),
            array('phone', '(41)96430788'),
            array('subject', 'Guitar Strings'),
            array('message', 'I like request special guitar strings.'),
            array('ip', '200.125.25.26'),
            array('recurrent', '1'),
            array('occurrences', '5'),
            array('createdAt', new \DateTime()),
            array('updatedAt', new \DateTime())
        );
    }
    
    public function testInitialStateEntity()
    {
        $entity =  new Budget();
        $this->assertNull($entity->getId());
    }
    
    /**
     * Tests whether there are attributes in the entity or are configured in tests
     */
    public function testIfAttributesAreInTheEntityClass()
    {
        $entity = new $this->entity();
        $obj = new \ReflectionObject($entity);
        $properties = $obj->getProperties();
        $dataProviderEntity = $this->dataProviderAttributes();
        $definedAttributes = array();
        foreach($dataProviderEntity as $item) {
            $definedAttributes[] = $item[0];
        }

        $propertiesArray = array();
        foreach($properties as $attribute) {
            $propertiesArray[] = $attribute->name;
        }
        $diff = array_diff($definedAttributes, $propertiesArray);
        $messageFalse = "There are no attributes configured in the Entity : ".serialize($diff)."\n";
        if(0 === count($diff)) {
            $diff = array_diff($propertiesArray, $definedAttributes);
            $messageFalse = "There are no attributes configured in the TestEntity : ".serialize($diff)."\n";
        }
        
        $this->assertTrue(($definedAttributes === $propertiesArray), $messageFalse);
    }
    
    /**
     * @depends testIfAttributesAreInTheEntityClass
     * @dataProvider dataProviderAttributes
     */
    public function testChecksIfClassHasGetAndSetsOfAttributes($attribute, $value)
    {
        $get = 'get' . ucfirst($attribute);
        $set = 'set' . ucfirst($attribute);

        $class = new $this->entity();
        $class->$set($value); 

        $attributesNotApplicateForTest = array(
            'createdAt', 'updatedAt',
        );
        
        if(!in_array($attribute, $attributesNotApplicateForTest)) {
            $this->assertEquals($value, $class->$get(), 'Fail Method for attribute -' . $attribute . ' | ');
        }
    }
    
    /**
     * @depends testChecksIfClassHasGetAndSetsOfAttributes
     */
    public function testIfEntitySetDataWithArrayViaConstructor()
    {
        $data = $post = (new \EventsTest\Assets\BudgetData())->getCompleteData();
        $entity =  new $this->entity($data);
        
        $this->assertSame($data['name'], $entity->getName(), 'Campo "name" diferente do parametro informado.');
        $this->assertSame($data['email'], $entity->getEmail(), 'Campo "email" diferente do parametro informado.');
        $this->assertSame($data['phone'], $entity->getPhone(), 'Campo "phone" diferente do parametro informado.');
        $this->assertSame($data['subject'], $entity->getSubject(), 'Campo "subject" diferente do parametro informado.');
        $this->assertSame($data['message'], $entity->getMessage(), 'Campo "message" diferente do parametro informado.');
        $this->assertSame($data['ip'], $entity->getIp(), 'Campo "ip" diferente do parametro informado.');
        $this->assertSame($data['recurrent'], $entity->getRecurrent(), 'Campo "recurrent" diferente do parametro informado.');
        $this->assertSame($data['occurrences'], $entity->getOccurrences(), 'Campo "occurrences" diferente do parametro informado.');
    }
    
    /**
     * @depends testChecksIfClassHasGetAndSetsOfAttributes
     */
    public function testChecarMetodoToArray()
    {
        $data = $post = (new \EventsTest\Assets\BudgetData())->getCompleteData();
        $entity =  new $this->entity($data);
        $arrayData = $entity->toArray();

        $this->assertSame($data['name'], $arrayData['name'], 'Campo "name" diferente do parametro informado.');
        $this->assertSame($data['email'], $arrayData['email'], 'Campo "email" diferente do parametro informado.');
        $this->assertSame($data['phone'], $arrayData['phone'], 'Campo "phone" diferente do parametro informado.');
        $this->assertSame($data['subject'], $arrayData['subject'], 'Campo "subject" diferente do parametro informado.');
        $this->assertSame($data['message'], $arrayData['message'], 'Campo "message" diferente do parametro informado.');
        $this->assertSame($data['ip'], $arrayData['ip'], 'Campo "ip" diferente do parametro informado.');
        $this->assertSame($data['recurrent'], $arrayData['recurrent'], 'Campo "recurrent" diferente do parametro informado.');
        $this->assertSame($data['occurrences'], $arrayData['occurrences'], 'Campo "occurrences" diferente do parametro informado.');
    }

    public function testCheckIfCreatedAtIsSet()
    {
        $entity =  new $this->entity();
        $entity->setCreatedAt();

        $this->assertInstanceOf('DateTime', $entity->getCreatedAt());
    }

    public function testCheckIfUpdatedAtAtIsSet()
    {
        $entity =  new $this->entity();
        $entity->setUpdatedAt();
        
        $this->assertInstanceOf('DateTime', $entity->getUpdatedAt());
    }
}   