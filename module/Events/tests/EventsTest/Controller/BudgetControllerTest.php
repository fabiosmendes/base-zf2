<?php

namespace EventsTest\Controller;

use Zend\Http\Request as HttpRequest;

use EventsTest\Bootstrap;
use Base\Controller\TestCaseController;

/**
 * @group Controllers
 */
class BudgetControllerTest extends TestCaseController
{
    const CONTROLLER_CLASS = 'Events\Controller\BudgetController';
    const EXTEND_CLASS     = 'Base\Controller\BaseController';
    
    /**
     * Main Service Controller
     */
    const MAIN_SERVICE     = 'Events\Service\Event';
    
    public function setUp()
    {
        $this->setApplicationConfig(Bootstrap::getConfig());
        parent::setUp();
        
        $this->controllerClass = self::CONTROLLER_CLASS;
        $this->extendClass     = self::EXTEND_CLASS;
    }
    
    public function testIndexActionIfAccess()
    {
        $this->dispatch('/budget');
        $this->assertResponseStatusCode(200);
        
        $this->assertModuleName('Events');
        $this->assertControllerName('Budget');
        $this->assertControllerClass('BudgetController');
        $this->assertActionName('index');
        $this->assertMatchedRouteName('general');
    }
    
    public function testIndexActionSendPostWithValidData()
    {
        $post = (new \EventsTest\Assets\BudgetData())->getValidPost();
        
        $this->dispatch('/budget', HttpRequest::METHOD_POST, $post);
        $this->assertResponseStatusCode(302);
        
        $this->assertModuleName('Events');
        $this->assertControllerName('Budget');
        $this->assertControllerClass('BudgetController');
        $this->assertActionName('index');
        $this->assertMatchedRouteName('general');
        
        $this->assertRedirectTo('/budget');
    }
    
    public function testIndexActionSendPostWithInvalidDataName()
    {
        $post = (new \EventsTest\Assets\BudgetData())->getValidPost();
        $post['name'] = '';
        
        $this->dispatch('/budget', HttpRequest::METHOD_POST, $post);
        $this->assertResponseStatusCode(200);
        
        $this->assertModuleName('Events');
        $this->assertControllerName('Budget');
        $this->assertControllerClass('BudgetController');
        $this->assertActionName('index');
        $this->assertMatchedRouteName('general');
    }
    
    public function testIndexActionSendPostWithInvalidDataEmail()
    {
        $post = (new \EventsTest\Assets\BudgetData())->getValidPost();
        $post['email'] = '';
        
        $this->dispatch('/budget', HttpRequest::METHOD_POST, $post);
        $this->assertResponseStatusCode(200);
        
        $this->assertModuleName('Events');
        $this->assertControllerName('Budget');
        $this->assertControllerClass('BudgetController');
        $this->assertActionName('index');
        $this->assertMatchedRouteName('general');
    }
    
    public function setServiceMockEventSuccess()
    {
        $srvUserMock = \Mockery::mock(self::MAIN_SERVICE);
        $srvUserMock->shouldReceive('checkMessage')->andReturn(true);
        
        $serviceManager = $this->getApplicationServiceLocator();
        $serviceManager->setAllowOverride(true);
        $serviceManager->setService(self::MAIN_SERVICE, $srvUserMock);   
    }
    
    public function testMethodSendForServiceLayerSucess()
    {    
        $this->setServiceMockEventSuccess();
        
        $controller = new \Events\Controller\BudgetController();
        $controller->setServiceLocator( $this->getApplicationServiceLocator() );
        
        $postData = (new \EventsTest\Assets\BudgetData())->getValidPost();
        $controller->processPost($postData);
        $result = $controller->sendToServiceLayer($postData);
        
        $this->assertTrue($result);  
    }
    
    public function setServiceMockEventFail()
    {
        $srvUserMock = \Mockery::mock(self::MAIN_SERVICE);
        $srvUserMock->shouldReceive('checkMessage')->andReturn(false);
        
        $serviceManager = $this->getApplicationServiceLocator();
        $serviceManager->setAllowOverride(true);
        $serviceManager->setService(self::MAIN_SERVICE, $srvUserMock);   
    }
    
    public function testMethodSendForServiceLayerFail()
    {    
        $this->setServiceMockEventFail();
        
        $controller = new \Events\Controller\BudgetController();
        $controller->setServiceLocator( $this->getApplicationServiceLocator() );
        
        $postData = (new \EventsTest\Assets\BudgetData())->getValidPost();
        $postData['name'] = '';
        $controller->processPost($postData);
        $result = $controller->sendToServiceLayer($postData);
        
        $this->assertFalse($result);  
    }
    
    /*
    public function testChecksIfTemplateFormContaisFormFieldsRequired()
    {
        $this->dispatch('/budget');
        $this->assertResponseStatusCode(200);
        
        $this->assertQuery('body form input[name="name"]');
        $this->assertQuery('body form input[name="email"]');
        $this->assertQuery('body form input[name="subject"]');
        $this->assertQuery('body form input[name="message"]');
    }*/
}