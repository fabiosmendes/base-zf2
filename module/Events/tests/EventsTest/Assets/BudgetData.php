<?php

namespace EventsTest\Assets;

class BudgetData
{
   public function getValidPost()
   {
        return array(
            'name'  => 'John Lennon',
            'email' => 'john@beatles.com',
            'nickname' => 'j.beatles',
        );
   }
   
   public function getCompleteData()
   {
       return array(
           'id'   => 1,
           'code' => '25354512',
           'name' => 'John Lennon',
           'email' => 'john.beatles@gmail.com',
           'phone' => '(41)96430788',
           'subject' => 'Guitar Strings',
           'message' => 'I like request special guitar strings.',
           'ip' => '200.125.25.26',
           'recurrent' => 1,
           'occurrences' => 5,
           'createdAt' => new \DateTime(),
           'updatedAt' => new \DateTime(),
       );
   }
}
