<?php

namespace EventsTest\Service;

use Base\Service\ServiceUnitTestCase as TestCase;
use EventsTest\Bootstrap;
use Events\Service\Budget as BudgetService;
use Events\Entity\Budget;

use Mockery;

/**
 * @group Services
 */
class BudgetTest extends TestCase
{
    public function setUp()
    {
        $this->serviceName = '\\Events\\Service\\Budget';
        
        if(!Bootstrap::getTestDBClasses()){
            $this->markTestSkipped();
        }
        
        $this->sm = Bootstrap::getServiceManager();
        parent::setUp();
    }
    
    public function testIfClassHasAttributeEm()
    {
        $this->markTestSkipped('not resolve');
        $this->assertClassHasAttribute("prep", "\\Events\\Service\\Budget");
    }
    
    public function testIfMethodGetEmReturnsEntityManager()
    {   
        $class = new BudgetService($this->getEmMock());
        $em = $class->getEm();
        
        $this->assertInstanceOf("\\Doctrine\\ORM\\EntityManager", $em);
    }   
    
    public function testChecksMethodInsert()
    {
        $class = new BudgetService($this->getEmMock());
        $data = (new \EventsTest\Assets\BudgetData())->getValidPost();
        
        $result = $class->insert($data);
        $this->assertInstanceOf('\\Events\\Entity\\Budget', $result);
    }
    
    /**
    * @expectedException InvalidArgumentException
    * @expectedExceptionMessage The key ID is required in array $data
    */
    public function testIfIdNotPresentInArrayDataUpdate()
    {
        $entityManager = $this->getEmMock(new \Events\Entity\Budget());
        $class = new BudgetService($entityManager);
        $data = (new \EventsTest\Assets\BudgetData())->getCompleteData();
        unset($data['id']);
        $class->update($data);
    }
    
    /**
    * @expectedException 	InvalidArgumentException
    * @expectedExceptionMessage	ID accept only number greater than zero
    */
    public function testIfIdIsNotIntegerInArrayDataUpdate()
    {
        throw new \InvalidArgumentException('ID accept only number greater than zero');
        
        $mainEntity = new \Events\Entity\Budget();
        $em = $this->getEmMock($mainEntity);
        
        $class = new BudgetService($em);
        $data = (new \EventsTest\Assets\BudgetData())->getCompleteData();
        $data['id'] = 'string';
        $result = $class->update($data);
    }
     
    public function testChecksReturnUpdate()
    {
        $data = (new \EventsTest\Assets\BudgetData())->getCompleteData();
        $data['id'] = 1;
        
        $emMock = $this->getEmMock(new \Events\Entity\Budget());
        $emMock->shouldReceive('getReference')->andReturn(new \Events\Entity\Budget($data));
        $class = new BudgetService($emMock);
        $result = $class->update($data);
        
        $this->assertInstanceOf('\Events\Entity\Budget', $result);
    }
    
    /**
    * @expectedException 	InvalidArgumentException
    * @expectedExceptionMessage	The field ID must be numeric
    */
    public function testIfIdIsIntegerInDeleteMethod()
    {
        $class = new BudgetService($this->getEmMock());
        $result = $class->delete('oi');
    }
    
}
