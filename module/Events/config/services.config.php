<?php

namespace Events;

use Front\Service\Contact as ContactService;

return array(
    'factories' => array(
        'Front\Service\Contact' => function($sm) {
            return new ContactService($sm->get('Doctrine\ORM\EntityManager'), 
                                      $sm->get('Base\Mail\Mail'));
        },        
    ),          
                
);