<?php

namespace Events\Form;

use Zend\Form\Form;
use Events\Form\BudgetFilter;

class Budget extends Form
{

    public function __construct($name = null, $options = array())
    {
        parent::__construct('budget');
        $this->setAttribute('method', 'post');
        $this->setInputFilter(new BudgetFilter());
        
        $this->add(array(
            'name' => 'name',
            'type' => 'Zend\Form\Element\Text',
            'options' => array(
                'label' => 'Name'
            ),
            'attributes' => array(
                'id' => 'name',
                'placeholder' => 'your name',
                'class' => '',
            )
        ));
        
        $this->add(array(
            'name' => 'email',
            'type' => 'Zend\Form\Element\Email',
            'options' => array(
                'label' => 'E-mail'
            ),
            'attributes' => array(
                'id' => 'email',
                'placeholder' => 'your e-mail',
                'class' => '',
            )
        ));
        
        $this->add(array(
            'name' => 'nickname',
            'type' => 'Zend\Form\Element\Text',
            'options' => array(
                'label' => 'Nickname'
            ),
            'attributes' => array(
                'id' => 'name',
                'placeholder' => 'your nickname',
                'class' => '',
            )
        ));
    }

}
