<?php

namespace Events\Service;

use Doctrine\ORM\EntityManager;
use Base\Service\AbstractService;

class Budget extends AbstractService
{
    public $prep;
    
    public function __construct(EntityManager $em) 
    {        
        $this->entity = 'Events\Entity\Budget';
        parent::__construct($em);
    }
}
