<?php

/**
 * @author Fabio de Souza <fbiosm@gmail.com>
 */
class Client
{
    /**
     * @access private
     * @var string 
     */
    private $name;
    
    /**
     * @access private
     * @var string 
     */
    private $email;

    /**
     * 
     * @param string $name
     * @param string $email
     */
    public function __construct($name, $email)
    {
        $this->name  = $name;
        $this->email = $email;
    }
    
    /**
     * @return string
     */
    public function getName()
    {
        return $this->name; 
    }
    
    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }
}


$reflect = new ReflectionClass('Client');


$methods = $reflect->getMethods();
foreach($methods as $method) {
    
    echo $method->getDocComment(); exit();
    
    echo __CLASS__.' | '.__METHOD__;
var_dump( get_class_methods( get_class($method)) ); exit();
}

echo "<pre>";
var_dump($reflect->getDocComment()); exit();

echo __CLASS__.' | '.__METHOD__;
var_dump( get_class_methods( get_class($reflect)) ); exit();